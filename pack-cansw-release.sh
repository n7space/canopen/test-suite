#!/usr/bin/env bash
if test "$#" -lt 1; then
    echo "Usage: $0 <TAG> [<PREFIX> [<REPO_PATH> [<GIT_URL>]]"
    echo ""
    echo "For example:"
    echo "       $0 v3.3.0 CAN-N7S-CANDP n7space/canopen/lely-core gitlab.com"
    exit -2
fi

set -e
set -x

PREFIX="${2:-CAN-N7S-CANDP}"
REPO_PATH="${3:-n7space/canopen/lely-core}"
TAG="$1"

SUFFIX=${TAG//./_}

GIT_URL="${5:-gitlab.com}"

DIST_DIR=build/dist

SRC_DIR="${PREFIX}-CANSW-${SUFFIX}"

rm -rf "${DIST_DIR}"
mkdir -p "${DIST_DIR}"

cd "${DIST_DIR}"

echo "Cloning repository"
git clone "git@${GIT_URL}:${REPO_PATH}.git" "${SRC_DIR}"
cd "${SRC_DIR}"
git checkout "${TAG}"
git submodule update --recursive --init
cd -

echo "Packing GIT repo"
tar -cjf "${SRC_DIR}-git.tar.bz2" "${SRC_DIR}"

echo "Removing git files"
find "${SRC_DIR}" -name .git | xargs rm -rf
find "${SRC_DIR}" -name .gitmodules | xargs rm -f

echo "Creating VERSION file"
echo "${TAG}" > "${SRC_DIR}/VERSION"

echo "Source checksums"
hashdeep    -c sha256 -r -l "${SRC_DIR}" > "${PREFIX}-SCF-A.csv"
hashdeep -d -c sha256 -r -l "${SRC_DIR}" > "${PREFIX}-SCF-A.xml"

echo "Packing SRC"
tar -cjf "${SRC_DIR}.tar.bz2" "${SRC_DIR}"

echo "Resolving symlinks"
cp -rL "${SRC_DIR}" "${SRC_DIR}-nolinks"
rm -r "${SRC_DIR}"
mv "${SRC_DIR}-nolinks" "${SRC_DIR}"

echo "zipping source"
7z a "${SRC_DIR}.zip" "${SRC_DIR}"

rm -r "${SRC_DIR}"

set +x
ls -alh

echo "Items available in:"
echo "${DIST_DIR}"
