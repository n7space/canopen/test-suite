# This file is part of the Test Suite build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Suite was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

tests = [
    "tests/dcf2dev",
    "tests/emcy",
    "tests/ecss-time",
    "tests/nmt",
    "tests/pdo",
    "tests/sdo",
    "tests/sync",
]

env = CreateEnvironment(
    root_dir="environment/",
    test_suite_lcov_modules=[
        "../tests",
    ],
)

Help("\n\nCTSSW - CANopen SW Library Test Suite - v3.3.0\n", append=True)
Help(
    "Licensed under European Space Agency Public License (ESA-PL) Permissive – v2.3\n",
    append=True,
)
Help("Copyright N7 Space sp. z o.o. 2020-2021\n\n", append=True)

Export("env")

env.AllEnvs.BuildModule("environment/resources/n7-core")
env.AllEnvs.BuildModule("environment/resources/bsp")
env.AllEnvs.BuildModule("environment/resources/LibCANopen")
env.AllEnvs.BuildModule("environment/resources/TestFramework")

env = env.CreateEnvForCanOpenTests()

tests = env.BuildModules(tests)
env.Alias("tests", tests)
env.Default(tests)

traces = env.DumpTraces(env["buildDir"] + "/integration-tests-specs.json")

env.Alias("traces", traces)
