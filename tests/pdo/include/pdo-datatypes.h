/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_PDO_DATATYPES_H__
#define CTSSW_PDO_DATATYPES_H__

#include <string.h>

#include <lely/compat/uchar.h>

#include "pdo.h"

/* basic types */
static const co_boolean_t TEST_VAL_B = 0x1u;
static const co_integer8_t TEST_VAL_I8 = 0x1A;
static const co_integer16_t TEST_VAL_I16 = 0x1234;
static const co_integer32_t TEST_VAL_I32 = 0x12345678;
static const co_unsigned8_t TEST_VAL_U8 = 0xABu;
static const co_unsigned16_t TEST_VAL_U16 = 0xABCDu;
static const co_unsigned32_t TEST_VAL_U32 = 0xABCDEF12u;
static const co_real32_t TEST_VAL_R32 = 1234.5678f;
static const co_integer24_t TEST_VAL_I24 = 0x123456;
static const co_real64_t TEST_VAL_R64 = 1234.5678f;
static const co_integer40_t TEST_VAL_I40 = 0x1234567890;
static const co_integer48_t TEST_VAL_I48 = 0x1234567890AB;
static const co_integer56_t TEST_VAL_I56 = 0x1234567890ABCD;
static const co_integer64_t TEST_VAL_I64 = 0x1234567890ABCDEF;
static const co_unsigned24_t TEST_VAL_U24 = 0xABCDEFu;
static const co_unsigned40_t TEST_VAL_U40 = 0xABCDEF1234u;
static const co_unsigned48_t TEST_VAL_U48 = 0xABCDEF123456u;
static const co_unsigned56_t TEST_VAL_U56 = 0xABCDEF12345678u;
static const co_unsigned64_t TEST_VAL_U64 = 0xABCDEF1234567890u;

/* time types */
static const co_time_of_day_t TEST_VAL_T = { 0x02345678u, 0x1234u };
static const co_time_diff_t TEST_VAL_TD = { 0x02345678u, 0x1234u };

static const co_time_scet_t TEST_VAL_SCET = { 0x12345678u, 0x123456u };
static const co_time_sutc_t TEST_VAL_SUTC = { 0x1234u, 0x12345678u, 0x1234u };

/* array types */
#define ARRAY_VAL_LEN 8

static char STRING_VAL[ARRAY_VAL_LEN] = "abcdefgh";
static char16_t USTRING_VAL[ARRAY_VAL_LEN] = u"abcd";
static uint_least8_t BYTE_VAL[ARRAY_VAL_LEN] = { 0x12, 0x34, 0x56, 0x78, 0x90,
	0xAB, 0xCD, 0xEF };

static const co_visible_string_t TEST_VAL_VS = STRING_VAL;
static const co_unicode_string_t TEST_VAL_US = USTRING_VAL;
static const co_octet_string_t TEST_VAL_OS = BYTE_VAL;
static const co_domain_t TEST_VAL_DOM = BYTE_VAL;

#endif // CTSSW_PDO_DATATYPES_H__
