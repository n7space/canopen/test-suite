/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_PDO_SYNC_H__
#define CTSSW_PDO_SYNC_H__

#include <assert.h>

#include "pdo.h"

#include <lely/co/sync.h>

static co_sync_t *sync = NULL;

static const co_unsigned32_t TEST_VAL_U32 = 0xC0DE0000u;
static const co_integer8_t TEST_VAL_I8 = 0x1A;

// PDO acyclic
static const uint32_t PDO_ACYCLIC_SYNC_NUMBER = 11u;
static const uint32_t PDO_ACYCLIC_ON_SYNC_SIZE = 5u;
static const uint32_t PDO_ACYCLIC_ON_SYNC[] = { 1u, 3u, 6u, 7u, 10u };

// PDO cyclic 1
static const uint32_t PDO_CYCLIC_1_NUMBER = 11u;
static const uint32_t PDO_CYCLIC_1_SYNC_NUMBER = PDO_CYCLIC_1_NUMBER;

// PDO cyclic N
static const uint32_t PDO_CYCLIC_N_EVERY_NTH_SYNC = 4u;
static const uint32_t PDO_CYCLIC_N_NUMBER = 7u;
static const uint32_t PDO_CYCLIC_N_SYNC_NUMBER =
		(PDO_CYCLIC_N_EVERY_NTH_SYNC * PDO_CYCLIC_N_NUMBER) + 1u;

// PDO start value
static const uint32_t PDO_START_VALUE = 11u;
static const uint32_t PDO_START_VALUE_EVERY_NTH_SYNC = 4u;
static const uint32_t PDO_START_VALUE_NUMBER = 4u;
static const uint32_t PDO_START_VALUE_SYNC_NUMBER = PDO_START_VALUE
		+ (PDO_START_VALUE_EVERY_NTH_SYNC * PDO_START_VALUE_NUMBER);

// SYNC default callbacks
static uint32_t syncCounter = 0;

static void
sync_ind_func(co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync == sync_);

	(void)cnt;
	(void)data;

	++syncCounter;
}

static void
sync_err_func(co_sync_t *sync_, co_unsigned16_t eec, co_unsigned8_t er,
		void *data)
{
	assert(sync == sync_);

	(void)data;
	LOG_UINT32("eec", eec);
	LOG_UINT32("er", er);
	FAIL_TEST("sync_err_func called");
}

#endif // CTSSW_PDO_SYNC_H__
