/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_PDO_H__
#define CTSSW_PDO_H__

#include <lely/co/dev.h>
#include <lely/co/type.h>

#include <TestFramework/TestHarness.h>

static co_dev_t *dev = NULL;

static const co_unsigned16_t PDO_NUM = 0x0001u;

static const co_unsigned16_t IDX = 0x2000u;
static const co_unsigned8_t SUBIDX = 0x00u;

static const co_unsigned16_t IDX_U32 = 0x2000u;
static const co_unsigned16_t IDX_I8 = 0x2001u;

static inline void
FailTestInvalidObjVal(const char errTag[], const co_unsigned16_t idx,
		const uint64_t val, const uint64_t expected)
{
	LOG_STR("invalid object value", errTag);
	LOG_UINT32("OD object", idx);
	LOG_UINT64_EXPECTED("value", expected, val);
	FAIL_TEST("Bad object value");
}

#endif // CTSSW_PDO_H__
