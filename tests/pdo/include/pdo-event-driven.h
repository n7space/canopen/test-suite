/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_PDO_EVENT_DRIVEN_H__
#define CTSSW_PDO_EVENT_DRIVEN_H__

#include "pdo.h"

static const co_unsigned32_t TEST_VAL_U32 = 0xDEAF9000u;
static const co_integer8_t TEST_VAL_I8 = 0x1A;

// PDO no timers
static const uint32_t PDO_NOTIMERS_NUMBER = 10u;

// PDO event-timer
static const uint32_t PDO_TIMER_NUMBER = 10u;
static const uint32_t PDO_TIMER_VALUE_MS = 1000u;

// PDO inhibit time
static const uint32_t PDO_INHIBIT_NUMBER = 10u;
static const uint32_t PDO_INHIBIT_VALUE_MS = 1000u;

#endif // CTSSW_PDO_EVENT_DRIVEN_H__
