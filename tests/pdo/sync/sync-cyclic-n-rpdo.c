/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "dcf/sync-cyclic-n-rpdo.h"
#include "pdo-rpdo.h"
#include "pdo-sync.h"

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static co_unsigned8_t lastSyncCnt = 0;

static co_unsigned32_t pdoDataU32 = TEST_VAL_U32;
static co_integer8_t pdoDataI8 = TEST_VAL_I8;

// RPDO callbacks
static void
rpdo_ind_func_cyclic_n_rpdo(co_rpdo_t *pdo, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	rpdo_ind_func(pdo, ac, ptr, n, data);

	// PDO is actuated with the reception of the next SYNC
	const uint32_t prevSyncCnt = (lastSyncCnt > 0) ? (lastSyncCnt - 1u) : 0;
	if ((prevSyncCnt % PDO_CYCLIC_N_EVERY_NTH_SYNC) != 0) {
		LOG_UINT32("counter", prevSyncCnt);
		LOG_UINT32("n-th sync", PDO_CYCLIC_N_EVERY_NTH_SYNC);
		FAIL_TEST("rpdo: invalid counter, PDO should be sent every n-th SYNC");
	}

	const co_unsigned32_t readValueU32 =
			co_dev_get_val_u32(dev, IDX_U32, SUBIDX);
	const co_integer8_t readValueI8 =
			co_dev_get_val_i8(dev, IDX_I8, SUBIDX);

	if (readValueU32 != pdoDataU32) {
		FailTestInvalidObjVal(
				"rpdo", IDX_U32, readValueU32, pdoDataU32);
	} else if (readValueI8 != pdoDataI8) {
		FailTestInvalidObjVal("rpdo", IDX_I8, (uint8_t)readValueI8,
				(uint8_t)pdoDataI8);
	}

	if (syncCounter == PDO_CYCLIC_N_SYNC_NUMBER) {
		if (rpdoCounter != PDO_CYCLIC_N_NUMBER) {
			LOG_UINT32_EXPECTED("received PDO messages",
					PDO_CYCLIC_N_NUMBER, rpdoCounter);
			FAIL_TEST("rpdo: incorrect received PDO messages count");
		} else if (sentMsgCounter != PDO_CYCLIC_N_SYNC_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					PDO_CYCLIC_N_SYNC_NUMBER,
					sentMsgCounter);
			FAIL_TEST("tpdo: incorrect sent messages count");
		} else if (msgCounter != rpdoCounter) {
			LOG_UINT32_EXPECTED("received PDO vs all messages",
					rpdoCounter, msgCounter);
			FAIL_TEST("rpdo: received non-PDO messages");
		} else {
			FINISH_TEST();
		}
	}
}

// SYNC callbacks
static void
sync_ind_func_cyclic_n_rpdo(co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync == sync_);

	sync_ind_func(sync, cnt, data);
	lastSyncCnt = cnt;

	REQUIRE(co_rpdo_sync(rpdo, cnt));

	++pdoDataU32;
	--pdoDataI8;
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sync_cyclic_n_rpdo_init();

	rpdo = co_rpdo_create(net, dev, PDO_NUM);
	co_rpdo_set_ind(rpdo, rpdo_ind_func_cyclic_n_rpdo, NULL);
	co_rpdo_set_err(rpdo, rpdo_err_func, NULL);

	sync = co_sync_create(net, dev);
	co_sync_set_ind(sync, sync_ind_func_cyclic_n_rpdo, NULL);
	co_sync_set_err(sync, sync_err_func, NULL);

	co_rpdo_start(rpdo);
	co_sync_start(sync);

	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, 0),
			sizeof(co_unsigned32_t));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, 0),
			sizeof(co_integer8_t));
}

void
TestTeardown(void)
{
	co_rpdo_stop(rpdo);
	co_sync_stop(sync);
	co_rpdo_destroy(rpdo);
	co_sync_destroy(sync);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter > PDO_CYCLIC_N_NUMBER)
		FAIL_TEST("rpdo: too many messages received");
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
