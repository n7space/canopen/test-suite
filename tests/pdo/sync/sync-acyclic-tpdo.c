/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>

#include "dcf/sync-acyclic-tpdo.h"

#include "pdo-sync.h"
#include "pdo-tpdo.h"

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static co_unsigned32_t pdoDataU32 = TEST_VAL_U32;
static co_integer8_t pdoDataI8 = TEST_VAL_I8;

// SYNC callbacks
static void
sync_ind_func_acyclic_tpdo(co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync == sync_);

	sync_ind_func(sync, cnt, data);

	++pdoDataU32;
	--pdoDataI8;

	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, pdoDataU32),
			sizeof(pdoDataU32));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, pdoDataI8),
			sizeof(pdoDataI8));

	for (size_t i = 0; i < PDO_ACYCLIC_ON_SYNC_SIZE; ++i) {
		if (syncCounter == PDO_ACYCLIC_ON_SYNC[i]) {
			REQUIRE(co_tpdo_event(tpdo));
			break;
		}
	}

	REQUIRE(co_tpdo_sync(tpdo, cnt));
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sync_acyclic_tpdo_init();
	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func, NULL);

	sync = co_sync_create(net, dev);
	co_sync_set_ind(sync, sync_ind_func_acyclic_tpdo, NULL);
	co_sync_set_err(sync, sync_err_func, NULL);

	co_tpdo_start(tpdo);
	co_sync_start(sync);

	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, pdoDataU32),
			sizeof(pdoDataU32));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, pdoDataI8),
			sizeof(pdoDataI8));
}

void
TestTeardown(void)
{
	co_tpdo_stop(tpdo);
	co_sync_stop(sync);
	co_tpdo_destroy(tpdo);
	co_sync_destroy(sync);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (syncCounter == PDO_ACYCLIC_SYNC_NUMBER) {
		if (tpdoCounter != PDO_ACYCLIC_ON_SYNC_SIZE) {
			LOG_UINT32_EXPECTED("sent TPDO messages",
					PDO_ACYCLIC_ON_SYNC_SIZE, tpdoCounter);
			FAIL_TEST("tpdo: incorrect sent PDO messages count");
		} else if (sentMsgCounter != PDO_ACYCLIC_ON_SYNC_SIZE) {
			LOG_UINT32_EXPECTED("sent messages",
					PDO_ACYCLIC_ON_SYNC_SIZE,
					sentMsgCounter);
			FAIL_TEST("tpdo: incorrect sent messages count");
		} else if (syncCounter != msgCounter) {
			LOG_UINT32_EXPECTED("SYNC vs messages count",
					syncCounter, msgCounter);
			FAIL_TEST("tpdo: received non-SYNC messages");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
