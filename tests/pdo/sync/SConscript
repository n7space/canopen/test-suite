# This file is part of the Test Suite build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Suite was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Import("env")

tests = []

pdoSyncTraces = [
    "SRS-CANSW-FUN-PDO-001",
    "SRS-CANSW-FUN-PDO-002",
]

acyclicTpdo = env.Dcf2Dev("sync-acyclic-tpdo.dcf")
acyclicRpdo = env.Dcf2Dev("sync-acyclic-rpdo.dcf")
tests += env.MakeSymmetricalTestCase(
    "pdo-sync-acyclic-test-case",
    ["sync-acyclic-tpdo.c"] + acyclicTpdo,
    ["sync-acyclic-rpdo.c"] + acyclicRpdo,
    trace={
        "title": "PDO synchronous acyclic transmission",
        "traces": pdoSyncTraces,
        "doc": {
            "given": "Client (RPDO) sends SYNC message every second.",
            "when": "Server (TPDO) receives SYNC message and there is new data available (internal condition).",
            "then": "Server (TPDO) sends PDO message.",
            "notes": [
                "Test validates that PDO service is implemented as specified in [SRS-CANSW-FUN-PDO-001].",
                "The service is configured with Synchronously Triggered PDO transmission ([SRS-CANSW-FUN-PDO-002]).",
            ],
        },
    },
)

cyclic_1_Tpdo = env.Dcf2Dev("sync-cyclic-1-tpdo.dcf")
cyclic_1_Rpdo = env.Dcf2Dev("sync-cyclic-1-rpdo.dcf")
tests += env.MakeSymmetricalTestCase(
    "pdo-sync-cyclic-1-test-case",
    ["sync-cyclic-1-tpdo.c"] + cyclic_1_Tpdo,
    ["sync-cyclic-1-rpdo.c"] + cyclic_1_Rpdo,
    trace={
        "title": "PDO synchronous cyclic transmission (every SYNC)",
        "traces": pdoSyncTraces,
        "doc": {
            "given": "Client (RPDO) sends SYNC message every second.",
            "when": "Server (TPDO) receives SYNC message.",
            "then": "Server (TPDO) sends PDO message.",
            "notes": [
                "Test validates that PDO service is implemented as specified in [SRS-CANSW-FUN-PDO-001].",
                "The service is configured with Synchronously Triggered PDO transmission ([SRS-CANSW-FUN-PDO-002]).",
            ],
        },
    },
)

cyclic_n_Tpdo = env.Dcf2Dev("sync-cyclic-n-tpdo.dcf")
cyclic_n_Rpdo = env.Dcf2Dev("sync-cyclic-n-rpdo.dcf")
tests += env.MakeSymmetricalTestCase(
    "pdo-sync-cyclic-n-test-case",
    ["sync-cyclic-n-tpdo.c"] + cyclic_n_Tpdo,
    ["sync-cyclic-n-rpdo.c"] + cyclic_n_Rpdo,
    trace={
        "title": "PDO synchronous cyclic transmission (every n-th SYNC)",
        "traces": pdoSyncTraces,
        "doc": {
            "given": "Client (RPDO) sends SYNC message every second.",
            "when": "Server (TPDO) receives SYNC message.",
            "then": "Server (TPDO) sends PDO message entry every 4th SYNC.",
            "notes": [
                "Test validates that PDO service is implemented as specified in [SRS-CANSW-FUN-PDO-001].",
                "The service is configured with Synchronously Triggered PDO transmission ([SRS-CANSW-FUN-PDO-002]).",
            ],
        },
    },
)

cyclicStartValueTpdo = env.Dcf2Dev("sync-cyclic-start-value-tpdo.dcf")
cyclicStartValueRpdo = env.Dcf2Dev("sync-cyclic-start-value-rpdo.dcf")
tests += env.MakeSymmetricalTestCase(
    "pdo-sync-cyclic-start-value-test-case",
    ["sync-cyclic-start-value-tpdo.c"] + cyclicStartValueTpdo,
    ["sync-cyclic-start-value-rpdo.c"] + cyclicStartValueRpdo,
    trace={
        "title": "PDO synchronous cyclic transmission (every n-th SYNC) with the start value",
        "traces": pdoSyncTraces,
        "doc": {
            "given": "Client (RPDO) sends SYNC message every second.",
            "when": "Server (TPDO) receives SYNC message.",
            "then": "Server (TPDO) sends PDO message entry 4th SYNC starting with 11th message.",
            "notes": [
                "Test validates that PDO service is implemented as specified in [SRS-CANSW-FUN-PDO-001].",
                "The service is configured with Synchronously Triggered PDO transmission ([SRS-CANSW-FUN-PDO-002]).",
            ],
        },
    },
)


env.Alias("pdo-sync-tests", tests)

Return("tests")
