/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include "dcf/sync-cyclic-start-value-tpdo.h"
#include "pdo-sync.h"
#include "pdo-tpdo.h"

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static bool pdoStarted = false;

static co_unsigned32_t pdoDataU32 = TEST_VAL_U32;
static co_integer8_t pdoDataI8 = TEST_VAL_I8;

// TPDO callbacks
static void
tpdo_ind_func_cyclic_sv_tpdo(co_tpdo_t *pdo, co_unsigned32_t ac,
		const void *ptr, size_t n, void *data)
{
	tpdo_ind_func(pdo, ac, ptr, n, data);

	if ((!pdoStarted) && (syncCounter < PDO_START_VALUE))
		FAIL_TEST("tpdo: transmitting PDO before SYNC start value");
}

// SYNC callbacks
static void
sync_ind_func_cyclic_sv_tpdo(co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync == sync_);

	sync_ind_func(sync, cnt, data);

	if (cnt == PDO_START_VALUE)
		pdoStarted = true;

	if (syncCounter > PDO_START_VALUE_SYNC_NUMBER)
		FAIL_TEST("tpdo: received too many SYNC messages");

	++pdoDataU32;
	--pdoDataI8;

	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, pdoDataU32),
			sizeof(pdoDataU32));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, pdoDataI8),
			sizeof(pdoDataI8));

	REQUIRE(co_tpdo_sync(tpdo, cnt));
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sync_cyclic_start_value_tpdo_init();
	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func_cyclic_sv_tpdo, NULL);

	sync = co_sync_create(net, dev);
	co_sync_set_ind(sync, sync_ind_func_cyclic_sv_tpdo, NULL);
	co_sync_set_err(sync, sync_err_func, NULL);

	co_tpdo_start(tpdo);
	co_sync_start(sync);

	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, pdoDataU32),
			sizeof(pdoDataU32));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, pdoDataI8),
			sizeof(pdoDataI8));
}

void
TestTeardown(void)
{
	co_tpdo_stop(tpdo);
	co_sync_stop(sync);
	co_tpdo_destroy(tpdo);
	co_sync_destroy(sync);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (syncCounter == PDO_START_VALUE_SYNC_NUMBER) {
		if (tpdoCounter != PDO_START_VALUE_NUMBER) {
			LOG_UINT32("received SYNC", syncCounter);
			LOG_UINT32("n-th SYNC", PDO_START_VALUE_EVERY_NTH_SYNC);
			LOG_UINT32("start m-th SYNC", PDO_START_VALUE);
			LOG_UINT32("TPDO sent", tpdoCounter);
			FAIL_TEST("tpro: received %d SYNC messages and PDO should be transmitted"
				  " every n-th starting from m-th SYNC, but too few PDO was sent");
		} else if (sentMsgCounter != PDO_START_VALUE_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					PDO_START_VALUE_NUMBER, sentMsgCounter);
			FAIL_TEST("tpdo: incorrect sent messages count");
		} else if (syncCounter != msgCounter) {
			LOG_UINT32_EXPECTED("received SYNC vs all messages",
					syncCounter, msgCounter);
			FAIL_TEST("tpdo: received other messages than SYNC - SYNC");
		} else
			FINISH_TEST();
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
