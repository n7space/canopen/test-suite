/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "dcf/event-driven-notimers-tpdo.h"
#include "pdo-event-driven.h"
#include "pdo-tpdo.h"

static uint32_t sentMsgCounter = 0;

static co_unsigned32_t pdoDataU32 = TEST_VAL_U32;
static co_integer8_t pdoDataI8 = TEST_VAL_I8;

static void
tpdo_ind_func_notimers_tpdo(co_tpdo_t *pdo, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	tpdo_ind_func(pdo, ac, ptr, n, data);

	if (tpdoCounter == PDO_NOTIMERS_NUMBER) {
		if (sentMsgCounter != PDO_NOTIMERS_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					PDO_NOTIMERS_NUMBER, tpdoCounter);
			FAIL_TEST("tpdo: incorrect PDO messages count");
		} else {
			FINISH_TEST();
		}
	} else {
		++pdoDataU32;
		++pdoDataI8;
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_event_driven_notimers_tpdo_init();
	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func_notimers_tpdo, NULL);

	co_tpdo_start(tpdo);

	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, 0),
			sizeof(co_unsigned32_t));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, 0),
			sizeof(co_integer8_t));
}

void
TestTeardown(void)
{
	co_tpdo_stop(tpdo);
	co_tpdo_destroy(tpdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("tpdo: received unexpected message");
}

void
TestStep(void)
{
	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, pdoDataU32),
			sizeof(pdoDataU32));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, pdoDataI8),
			sizeof(pdoDataI8));
	REQUIRE(co_tpdo_event(tpdo));
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
