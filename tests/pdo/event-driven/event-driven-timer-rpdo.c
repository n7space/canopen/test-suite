/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/event-driven-timer-rpdo.h"
#include "pdo-event-driven.h"
#include "pdo-rpdo.h"

static uint32_t msgCounter = 0;
static struct timespec lastTime = { 0, 0 };

static co_unsigned32_t pdoDataU32 = TEST_VAL_U32;
static co_integer8_t pdoDataI8 = TEST_VAL_I8;

void
TestSetup(can_net_t *const net)
{
	dev = dcf_event_driven_timer_rpdo_init();

	rpdo = co_rpdo_create(net, dev, PDO_NUM);
	co_rpdo_set_ind(rpdo, rpdo_ind_func, NULL);
	co_rpdo_set_err(rpdo, rpdo_err_func, NULL);

	co_rpdo_start(rpdo);

	REQUIRE_SIZE(co_dev_set_val_u32(dev, IDX_U32, SUBIDX, 0),
			sizeof(co_unsigned32_t));
	REQUIRE_SIZE(co_dev_set_val_i8(dev, IDX_I8, SUBIDX, 0),
			sizeof(co_integer8_t));
}

void
TestTeardown(void)
{
	co_rpdo_stop(rpdo);
	co_rpdo_destroy(rpdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	const co_unsigned32_t readValueU32 =
			co_dev_get_val_u32(dev, IDX_U32, SUBIDX);
	const co_integer8_t readValueI8 =
			co_dev_get_val_i8(dev, IDX_I8, SUBIDX);

	if (readValueU32 != pdoDataU32) {
		FailTestInvalidObjVal(
				"rpdo", IDX_U32, readValueU32, pdoDataU32);
	} else if (readValueI8 != pdoDataI8) {
		FailTestInvalidObjVal("rpdo", IDX_I8, (uint8_t)readValueI8,
				(uint8_t)pdoDataI8);
	}

	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	const int32_t delta =
			(int32_t)timespec_diff_msec(&currentTime, &lastTime);

	if (!CheckTimeIntervalWithAccuracy(
			    delta, PDO_TIMER_VALUE_MS, HOST_TIME_ACCURACY)) {
		LOG_INT32_EXPECTED("messages delay", PDO_TIMER_VALUE_MS, delta);
		FAIL_TEST("rpdo: incorrect delay between messages");
	}

	if (msgCounter == PDO_TIMER_NUMBER) {
		if (rpdoCounter != PDO_TIMER_NUMBER) {
			LOG_UINT32_EXPECTED("received messages",
					PDO_TIMER_NUMBER, rpdoCounter);
			FAIL_TEST("rpdo: incorrect received PDO messages count");
		} else {
			FINISH_TEST();
		}
	} else {
		lastTime = currentTime;
		++pdoDataU32;
		++pdoDataI8;
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("rpdo: sent unexpected message");
}
