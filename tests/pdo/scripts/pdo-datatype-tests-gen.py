#!/usr/bin/env python3

# This file is part of the Test Suite test cases.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Suite was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple
import subprocess

CoDatatype = namedtuple("CoDatatype", "name tag idx len type")

DataTypesList = [
    # basic
    CoDatatype("boolean", "b", "01", "01", "basic"),
    CoDatatype("integer8", "i8", "02", "08", "basic"),
    CoDatatype("integer16", "i16", "03", "10", "basic"),
    CoDatatype("integer32", "i32", "04", "20", "basic"),
    CoDatatype("unsigned8", "u8", "05", "08", "basic"),
    CoDatatype("unsigned16", "u16", "06", "10", "basic"),
    CoDatatype("unsigned32", "u32", "07", "20", "basic"),
    CoDatatype("real32", "r32", "08", "20", "basic"),
    CoDatatype("integer24", "i24", "10", "18", "basic"),
    CoDatatype("real64", "r64", "11", "40", "basic"),
    CoDatatype("integer40", "i40", "12", "28", "basic"),
    CoDatatype("integer48", "i48", "13", "30", "basic"),
    CoDatatype("integer56", "i56", "14", "38", "basic"),
    CoDatatype("integer64", "i64", "15", "40", "basic"),
    CoDatatype("unsigned24", "u24", "16", "18", "basic"),
    CoDatatype("unsigned40", "u40", "18", "28", "basic"),
    CoDatatype("unsigned48", "u48", "19", "30", "basic"),
    CoDatatype("unsigned56", "u56", "1A", "38", "basic"),
    CoDatatype("unsigned64", "u64", "1B", "40", "basic"),
    # extended
    CoDatatype("time_of_day", "t", "0C", "30", "time"),
    CoDatatype("time_diff", "td", "0D", "30", "time"),
    CoDatatype("visible_string", "vs",  "09", "40", "array"),
    CoDatatype("octet_string",   "os",  "0A", "40", "array"),
    CoDatatype("unicode_string", "us",  "0B", "40", "array"),
    CoDatatype("domain",         "dom", "0F", "40", "array"),
]

for dt in DataTypesList:
    for pdo in ["rpdo", "tpdo"]:
        fname = dt.name.replace("_", "-")

        cmd1 = (
            f'sed -e "s/DATATYPE/{dt.name.capitalize()}/"'
            f' -e "s/IDX/{dt.idx}/"'
            f' -e "s/LEN/{dt.len}/"'
            f" datatype-test-{pdo}.dcf"
            f" > ../datatypes/0x{dt.idx}-{fname}-{pdo}.dcf"
        )

        cmd2 = (
            f'sed -e "s/DCF-DATATYPE-FILE/0x{dt.idx}-{fname}/"'
            f' -e "s/DCF_DATATYPE_FUNC/0x{dt.idx}_{dt.name}/"'
            f' -e "s/DATATYPE_NAME/{dt.name}/g"'
            f' -e "s/DATATYPE_UNAME/{dt.name.upper()}/g"'
            f' -e "s/DATATYPE_TAG/{dt.tag}/g"'
            f' -e "s/DATATYPE_UTAG/{dt.tag.upper()}/g"'
            f" 0x00-{dt.type}-datatype-{pdo}.c"
            f" > ../datatypes/0x{dt.idx}-{fname}-{pdo}.c"
        )

        cmd3 = f"clang-format -i ../datatypes/0x{dt.idx}-{fname}-{pdo}.c"

        subprocess.call(cmd1, shell=True)
        subprocess.call(cmd2, shell=True)
        subprocess.call(cmd3, shell=True)
