/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "dcf/0x18-unsigned40-tpdo.h"
#include "pdo-datatypes.h"
#include "pdo-tpdo.h"

void
TestSetup(can_net_t *const net)
{
	dev = dcf_0x18_unsigned40_tpdo_init();
	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func, NULL);

	co_tpdo_start(tpdo);

	REQUIRE_SIZE(co_dev_set_val_u40(dev, IDX, SUBIDX, 0),
			sizeof(co_unsigned40_t));
}

void
TestTeardown(void)
{
	co_tpdo_stop(tpdo);
	co_tpdo_destroy(tpdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("tpdo: received unexpected message");
}

void
TestStep(void)
{
	REQUIRE_SIZE(co_dev_set_val_u40(dev, IDX, SUBIDX, TEST_VAL_U40),
			sizeof(co_unsigned40_t));
	REQUIRE(co_tpdo_event(tpdo));

	if (tpdoCounter != 1u) {
		LOG_UINT32_EXPECTED("sent messges", 1, tpdoCounter);
		FAIL_TEST("tpdo: should have sent one PDO message");
	} else {
		FINISH_TEST();
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
}
