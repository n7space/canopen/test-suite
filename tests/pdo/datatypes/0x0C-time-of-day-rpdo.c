/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/val.h>

#include "dcf/0x0C-time-of-day-rpdo.h"
#include "pdo-datatypes.h"
#include "pdo-rpdo.h"

void
TestSetup(can_net_t *const net)
{
	dev = dcf_0x0C_time_of_day_rpdo_init();

	rpdo = co_rpdo_create(net, dev, PDO_NUM);
	co_rpdo_set_ind(rpdo, rpdo_ind_func, NULL);
	co_rpdo_set_err(rpdo, rpdo_err_func, NULL);

	co_rpdo_start(rpdo);

	const co_time_of_day_t t = CO_TIME_OF_DAY_INIT;
	REQUIRE_SIZE(co_dev_set_val(dev, IDX, SUBIDX, &t, sizeof(t)),
			sizeof(t));
}

void
TestTeardown(void)
{
	co_rpdo_stop(rpdo);
	co_rpdo_destroy(rpdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	const co_time_of_day_t *tReadValue = co_dev_get_val(dev, IDX, SUBIDX);

	if ((tReadValue->days != TEST_VAL_T.days)
			|| (tReadValue->ms != TEST_VAL_T.ms)) {
		LOG_UINT32("OD object", IDX);
		LOG_UINT64_EXPECTED("days", TEST_VAL_T.days, tReadValue->days);
		LOG_UINT64_EXPECTED("ms", TEST_VAL_T.ms, tReadValue->ms);

		FAIL_TEST("rpdo: OD object does not contain expected");
	} else if (rpdoCounter != 1u) {
		FAIL_TEST("rpdo: received more than one PDO frame");
	} else {
		FINISH_TEST();
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
}
