# This file is part of the Test Suite build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Suite was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple

CoDatatype = namedtuple("CoDatatype", "name idx traces notes")

datatype_list = [
    # basic
    CoDatatype("boolean", "01", ["SRS-CANSW-FUN-PDO-910"], []),
    CoDatatype("integer8", "02", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("integer16", "03", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("integer24", "10", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("integer32", "04", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("integer40", "12", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("integer48", "13", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("integer56", "14", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("integer64", "15", ["SRS-CANSW-FUN-PDO-920"], []),
    CoDatatype("unsigned8", "05", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("unsigned16", "06", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("unsigned32", "07", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("unsigned24", "16", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("unsigned40", "18", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("unsigned48", "19", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("unsigned56", "1A", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("unsigned64", "1B", ["SRS-CANSW-FUN-PDO-930"], []),
    CoDatatype("real32", "08", ["SRS-CANSW-FUN-PDO-940"], []),
    CoDatatype("real64", "11", ["SRS-CANSW-FUN-PDO-940"], []),
    # extended
    CoDatatype("time_of_day", "0C", ["SRS-CANSW-FUN-PDO-990"], []),
    CoDatatype("time_diff", "0D", ["SRS-CANSW-FUN-PDO-995"], []),
    CoDatatype("visible_string", "09", ["SRS-CANSW-FUN-PDO-955"], []),
    CoDatatype("octet_string", "0A", ["SRS-CANSW-FUN-PDO-950"], []),
    CoDatatype("unicode_string", "0B", ["SRS-CANSW-FUN-PDO-960"], []),
    CoDatatype("domain", "0F", ["SRS-CANSW-FUN-PDO-970"], []),
    # ecss time
    CoDatatype(
        "time_scet",
        "60",
        [
            "SRS-CANSW-FUN-PDO-1000",
            "SRS-CANSW-FUN-TIME-OBJ-110",
            "SRS-CANSW-FUN-TIME-OBJ-250",
            "SRS-CANSW-FUN-TIME-OBJ-260",
        ],
        [
            "The SCET data type has format compliant with [SRS-CANSW-FUN-TIME-OBJ-110].",
            "[SRS-CANSW-FUN-TIME-OBJ-250] and [SRS-CANSW-FUN-TIME-OBJ-260] are requirements for the user of the Library, "
            + "the test proves that the Library provides features necessary for achieving that compliance to the user "
            + "- Local SCET Set/Get objects are of SCET data type ([SRS-CANSW-FUN-TIME-OBJ-250]) and "
            + "can be mapped into a CAN frame according to [SRS-CANSW-FUN-TIME-OBJ-260].",
        ],
    ),
    CoDatatype(
        "time_sutc",
        "61",
        [
            "SRS-CANSW-FUN-PDO-1005",
            "SRS-CANSW-FUN-TIME-OBJ-120",
            "SRS-CANSW-FUN-TIME-OBJ-130",
            "SRS-CANSW-FUN-TIME-OBJ-340",
            "SRS-CANSW-FUN-TIME-OBJ-350",
        ],
        [
            "The UTC data type has format compliant with [SRS-CANSW-FUN-TIME-OBJ-120] and [SRS-CANSW-FUN-TIME-OBJ-130].",
            "[SRS-CANSW-FUN-TIME-OBJ-340] and [SRS-CANSW-FUN-TIME-OBJ-350] are requirements for the user of the Library, "
            + "the test proves that the Library provides features necessary for achieving that compliance to the user "
            + "- Local UTC Set/Get objects are of UTC data type ([SRS-CANSW-FUN-TIME-OBJ-340]) and "
            + "can be mapped into a CAN frame according to [SRS-CANSW-FUN-TIME-OBJ-350].",
        ],
    ),
]

Import("env")

tests = []

pdoTraces = [
    "SRS-CANSW-FUN-PDO-001",
]

env.Append(DCF2DEV_FLAGS=["--deftype-time-scet", "96"])
env.Append(DCF2DEV_FLAGS=["--deftype-time-sutc", "97"])

for dt in datatype_list:
    fileNameInfix = dt.name.replace("_", "-")

    dtTpdo = env.Dcf2Dev(f"0x{dt.idx}-{fileNameInfix}-tpdo.dcf")
    dtRpdo = env.Dcf2Dev(f"0x{dt.idx}-{fileNameInfix}-rpdo.dcf")

    tests += env.MakeSymmetricalTestCase(
        f"pdo-0x{dt.idx}-{fileNameInfix}-test-case",
        [f"0x{dt.idx}-{fileNameInfix}-tpdo.c"] + dtTpdo,
        [f"0x{dt.idx}-{fileNameInfix}-rpdo.c"] + dtRpdo,
        trace={
            "title": f"PDO transmission of {dt.name.upper()} value",
            "traces": pdoTraces + dt.traces,
            "doc": {
                "given": "Internal event occurs in server (TPDO).",
                "when": f"Server (TPDO) sends PDO message with {dt.name.upper()} value.",
                "then": "Client (RPDO) receives PDO message with the correct value.",
                "notes": [
                    "Test validates that PDO service is implemented as specified in [SRS-CANSW-FUN-PDO-001] "
                    + f"and supports {dt.name.upper()} data type ([{dt.traces[0]}]).",
                ]
                + dt.notes,
            },
        },
    )

env.Alias("pdo-datatypes-tests", tests)

Return("tests")
