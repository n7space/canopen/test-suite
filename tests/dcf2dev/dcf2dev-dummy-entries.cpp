/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <array>

#include <CppUTest/TestHarness.h>

#include <lely/co/dev.h>
#include <lely/co/obj.h>
#include <stdbool.h>

#include "test-base.hpp"

#include "dcf-correct/dcf/dummy-entries.h"

TEST_GROUP_BASE(Dcf2Dev_Dummies, Dcf2Dev_Base) {
  co_dev_t* dev = nullptr;

  TEST_SETUP() { dev = dcf_dummy_entries_init(); }
};

TEST(Dcf2Dev_Dummies, NetId) { CHECK_EQUAL(0x00u, co_dev_get_netid(dev)); }

TEST(Dcf2Dev_Dummies, Baud) {
  const uint16_t BAUD = 0u | CO_BAUD_10 | CO_BAUD_20 | CO_BAUD_50 |
                        CO_BAUD_125 | CO_BAUD_250 | CO_BAUD_250 | CO_BAUD_500 |
                        CO_BAUD_800 | CO_BAUD_1000;
  const auto baud = co_dev_get_baud(dev);

  CHECK_EQUAL(BAUD, baud);
}

TEST(Dcf2Dev_Dummies, LssSupported) { CHECK_EQUAL(1, co_dev_get_lss(dev)); }

TEST(Dcf2Dev_Dummies, Dummies) {
  // clang-format off
  const co_unsigned32_t DUMMY = 1u << 2u
                              | 1u << 3u
                              | 1u << 5u
                              | 1u << 6u;
  // clang-format on

  const co_unsigned32_t dummy = co_dev_get_dummy(dev);

  CHECK_EQUAL(DUMMY, dummy);
}

TEST(Dcf2Dev_Dummies, Id) { CHECK_EQUAL(0x20u, co_dev_get_id(dev)); }

TEST(Dcf2Dev_Dummies, Indexes) {
  const co_unsigned8_t INDEXES_NUM = 10u;
  const std::array<co_unsigned16_t, INDEXES_NUM> INDEXES{
      0x1000u, 0x1001u, 0x1018u, 0x1400u, 0x1600u,
      0x1F80u, 0x2000u, 0x2001u, 0x2020u, 0x2021u};
  std::array<co_unsigned16_t, INDEXES_NUM> indexes{};

  const auto indexes_num = co_dev_get_idx(dev, INDEXES_NUM, indexes.data());

  CHECK_EQUAL(INDEXES_NUM, indexes_num);
  for (co_unsigned8_t i = 0u; i < INDEXES_NUM; i++)
    CHECK_EQUAL(INDEXES[i], indexes[i]);
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x1000) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1000u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0x00u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, sub00, CO_ACCESS_RO,
                              0x00000000u);
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x1001) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1001u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0x00u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, sub00, CO_ACCESS_RO, 0x00u);
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x1018) {
  const co_unsigned8_t SUBS_NUM = 5u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1018u);

  const auto subindexes_num = co_obj_get_subidx(obj, 5u, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, subindexes[i]);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_CONST,
                              0x04u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RO,
                              0x004E3753u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[2], CO_ACCESS_RO,
                              0x00000000u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[3], CO_ACCESS_RO,
                              0x00000000u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[4], CO_ACCESS_RO,
                              0x00000000u);
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x1400) {
  const co_unsigned8_t SUBS_NUM = 3u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1400u);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, i);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_CONST,
                              0x02u);
  const auto nodeid = co_dev_get_id(dev);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RW,
                              0x00000180u + nodeid);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[2], CO_ACCESS_RW,
                              0xFEu);
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x1600) {
  const co_unsigned8_t SUBS_NUM = 4u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1600u);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, i);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_RO,
                              0x03u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RW,
                              0x20000020u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[2], CO_ACCESS_RW,
                              0x20010008u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[3], CO_ACCESS_RW,
                              0x00020008u);
}

TEST(Dcf2Dev_Dummies, NmtStartupObj) {
  const co_obj_t* const obj1f80 = co_dev_find_obj(dev, 0x1F80u);
  const co_sub_t* const sub00 = co_obj_find_sub(obj1f80, 0x00u);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, sub00, CO_ACCESS_RW,
                              0x00000001u);
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x2000) {
  co_unsigned8_t subindex = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2000u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1, &subindex);

  CHECK_EQUAL(1, subindexes_num);
  CHECK_EQUAL(0x00u, subindex);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, 0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, sub00, CO_ACCESS_RW,
                              0x0000000Du);
  CHECK_EQUAL(1, co_sub_get_pdo_mapping(sub00));
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x2001) {
  co_unsigned8_t subindex = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2001u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1, &subindex);

  CHECK_EQUAL(1, subindexes_num);
  CHECK_EQUAL(0x00u, subindex);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, 0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER8, i8, sub00, CO_ACCESS_RW, 0x00u);
  CHECK_EQUAL(1, co_sub_get_pdo_mapping(sub00));
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x2020) {
  const co_unsigned8_t SUBS_NUM = 6u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2020u);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, i);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_RO,
                              0x05u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[1], CO_ACCESS_RW,
                              0xAAu);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[2], CO_ACCESS_RW, 0u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[3], CO_ACCESS_RW, 1u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[4], CO_ACCESS_RW, 56u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[5], CO_ACCESS_RW,
                              0xFFu);
}

TEST(Dcf2Dev_Dummies, SubobjectsInObj0x2021) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2021u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0x00u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER48, i48, sub00, CO_ACCESS_RW, 17u);
}
