# This file is part of the Test Suite test cases.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

[DeviceInfo]
VendorName=N7 Space Sp. z o.o.
VendorNumber=0x004E3753
BaudRate_10=1
BaudRate_20=1
BaudRate_50=1
BaudRate_250=1
BaudRate_800=0
BaudRate_1000=1
# Example of compact TPDO
CompactPDO=0x3F
NrOfTxPDO=2

[DeviceComissioning]
NodeID=0x10

[MandatoryObjects]
SupportedObjects=3
1=0x1000
2=0x1001
3=0x1018

[OptionalObjects]
SupportedObjects=3
1=0x1800
2=0x1A00
3=0x1A01

[ManufacturerObjects]
SupportedObjects=1
1=0x2020

[1000]
ParameterName=Device type
DataType=0x0007
AccessType=ro

[1001]
ParameterName=Error register
DataType=0x0005
AccessType=ro

[1018]
SubNumber=5
ParameterName=Identity object
ObjectType=0x09

[1018sub0]
ParameterName=Highest sub-index supported
DataType=0x0005
AccessType=const
DefaultValue=0x04

[1018sub1]
ParameterName=Vendor-ID
DataType=0x0007
AccessType=ro
DefaultValue=0x004E3753

[1018sub2]
ParameterName=Product code
DataType=0x0007
AccessType=ro
DefaultValue=0xabababab

[1018sub3]
ParameterName=Revision number
DataType=0x0007
AccessType=ro
DefaultValue=0xabcdef12

[1018sub4]
ParameterName=Serial number
DataType=0x0007
AccessType=ro
DefaultValue=0x12344321

[1800]
SubNumber=3
ParameterName=TPDO communication parameter
ObjectType=0x09

[1800sub0]
ParameterName=Highest sub-index supported
DataType=0x0005
AccessType=const
DefaultValue=0x02

[1800sub1]
ParameterName=COB-ID used by TPDO
DataType=0x0007
AccessType=rw
DefaultValue=$NODEID+0x180

[1800sub2]
ParameterName=Transmission type
DataType=0x0005
AccessType=rw
DefaultValue=0xfe

[1A00]
ParameterName=TPDO mapping parameter
ObjectType=0x09
DataType=0x0007
AccessType=rw
CompactSubObj=1

[1A00Value]
NrOfEntries=1
1=0x20200008

[1A01]
ParameterName=Compact TPDO mapping
ObjectType=0x09
DataType=0x0007
AccessType=rw
CompactSubObj=1

[1A01Value]
NrOfEntries=1
1=0x20200008

[2020]
ParameterName=Test
DataType=0x0005
AccessType=rw
PDOMapping=1
