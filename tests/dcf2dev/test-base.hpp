/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <CppUTest/TestHarness.h>

#include <lely/co/dev.h>
#include <lely/co/obj.h>
#include <stdbool.h>

#define CHECK_SUB_TYPE_ACCESS_VALUE(type, type_short, sub, access, value) \
  do { \
    CHECK_EQUAL(CO_DEFTYPE_##type, co_sub_get_type(sub)); \
    CHECK_EQUAL(access, co_sub_get_access(sub)); \
    CHECK_EQUAL(value, co_sub_get_val_##type_short(sub)); \
  } while (0)

TEST_BASE(Dcf2Dev_Base){};
