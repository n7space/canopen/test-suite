/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <array>

#include <CppUTest/TestHarness.h>

#include <lely/co/dev.h>
#include <lely/co/obj.h>
#include <stdbool.h>

#include "test-base.hpp"

#include "dcf-correct/dcf/all-sub-types.h"

TEST_GROUP_BASE(Dcf2Dev_AllTypes, Dcf2Dev_Base) {
  co_dev_t* dev = nullptr;

  TEST_SETUP() { dev = dcf_all_sub_types_init(); }
};

TEST(Dcf2Dev_AllTypes, LssSupported) { CHECK_EQUAL(0, co_dev_get_lss(dev)); }

TEST(Dcf2Dev_AllTypes, Dummies) {
  CHECK_EQUAL(0x00000000u, co_dev_get_dummy(dev));
}

TEST(Dcf2Dev_AllTypes, Id) { CHECK_EQUAL(0x10u, co_dev_get_id(dev)); }

TEST(Dcf2Dev_AllTypes, Indexes) {
  const co_unsigned8_t INDEXES_NUM = 35u;
  const std::array<co_unsigned16_t, INDEXES_NUM> INDEXES{
      0x1000u, 0x1001u, 0x1018u, 0x3001u, 0x3002u, 0x3003u, 0x3004u,
      0x3005u, 0x3006u, 0x3007u, 0x3008u, 0x3009u, 0x300Au, 0x300Bu,
      0x300Cu, 0x300Du, 0x300Fu, 0x3010u, 0x3011u, 0x3012u, 0x3013u,
      0x3014u, 0x3015u, 0x3016u, 0x3018u, 0x3019u, 0x301Au, 0x301Bu,
      0x301Cu, 0x301Du, 0x301Eu, 0x3060u, 0x3061u, 0x3160u, 0x3161u};
  std::array<co_unsigned16_t, INDEXES_NUM> indexes{};

  const auto indexes_num = co_dev_get_idx(dev, INDEXES_NUM, indexes.data());

  CHECK_EQUAL(INDEXES_NUM, indexes_num);
  for (co_unsigned8_t i = 0u; i < INDEXES_NUM; i++)
    CHECK_EQUAL(INDEXES[i], indexes[i]);
}

TEST(Dcf2Dev_AllTypes, SubobjectsInObj0x1000) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1000u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0x00u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, sub00, CO_ACCESS_RO,
                              0x00000000u);
}

TEST(Dcf2Dev_AllTypes, SubobjectsInObj0x1001) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1001u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, sub00, CO_ACCESS_RO, 0x00u);
}

TEST(Dcf2Dev_AllTypes, SubobjectsInObj0x1018) {
  const co_unsigned8_t SUBS_NUM = 5u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1018u);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, subindexes[i]);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_CONST,
                              0x04u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RO,
                              0x004E3753u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[2], CO_ACCESS_RO,
                              0xABABABABu);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[3], CO_ACCESS_RO,
                              0xABCDEF12u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[4], CO_ACCESS_RO,
                              0x12344321u);
}

TEST(Dcf2Dev_AllTypes, Boolean) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3001u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(BOOLEAN, b, sub00, CO_ACCESS_RO, 0x00u);
}

TEST(Dcf2Dev_AllTypes, Integer8) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3002u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER8, i8, sub00, CO_ACCESS_RO, 0x33);
}

TEST(Dcf2Dev_AllTypes, Integer16) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3003u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER16, i16, sub00, CO_ACCESS_RO, 0x4041);
}

TEST(Dcf2Dev_AllTypes, Integer32) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3004u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER32, i32, sub00, CO_ACCESS_RO, 0x7BC66202);
}

TEST(Dcf2Dev_AllTypes, Unsigned8) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3005u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, sub00, CO_ACCESS_RW, 0xFDu);
}

TEST(Dcf2Dev_AllTypes, Unsigned16) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3006u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED16, u16, sub00, CO_ACCESS_RW, 0xD0DEu);
}

TEST(Dcf2Dev_AllTypes, Unsigned32) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3007u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, sub00, CO_ACCESS_RW,
                              0x3210ACDEu);
}

TEST(Dcf2Dev_AllTypes, Real32) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3008u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(REAL32, r32, sub00, CO_ACCESS_RW, 2.5);
}

TEST(Dcf2Dev_AllTypes, VisibleString) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3009u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  const char* const expected = "test visible string";
  const co_visible_string_t sub_val =
      *static_cast<const co_visible_string_t*>(co_sub_get_val(sub00));
  STRCMP_EQUAL(expected, sub_val);
}

TEST(Dcf2Dev_AllTypes, OctetString) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x300Au);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  const co_octet_string_t val =
      *static_cast<const co_octet_string_t*>(co_sub_get_val(sub00));
  const uint_least8_t expected[] = {0x01u, 0x23u, 0x45u, 0x67u};
  CHECK(memcmp(expected, val, 4u) == 0);
}

TEST(Dcf2Dev_AllTypes, UnicodeString) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x300Bu);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  const char16_t* const expected = u"Test unicode string";
  const co_unicode_string_t val =
      *static_cast<const co_unicode_string_t*>(co_sub_get_val(sub00));
  CHECK(memcmp(expected, val, 20u) == 0);
}

TEST(Dcf2Dev_AllTypes, TimeOfDay) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x300Cu);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_OF_DAY, co_sub_get_type(sub00));
  const co_time_of_day_t tod =
      *static_cast<const co_time_of_day_t*>(co_sub_get_val(sub00));
  const co_unsigned16_t days = tod.days;
  const co_unsigned32_t miliseconds = tod.ms;
  CHECK_EQUAL(32u, days);
  CHECK_EQUAL(213u, miliseconds);
}

TEST(Dcf2Dev_AllTypes, TimeDiff) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x300Du);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_DIFF, co_sub_get_type(sub00));
  const co_time_of_day_t td =
      *static_cast<const co_time_of_day_t*>(co_sub_get_val(sub00));
  const co_unsigned16_t days = td.days;
  const co_unsigned32_t miliseconds = td.ms;
  CHECK_EQUAL(321u, days);
  CHECK_EQUAL(213123u, miliseconds);
}

TEST(Dcf2Dev_AllTypes, TimeOfDay_Default) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x301Cu);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_OF_DAY, co_sub_get_type(sub00));
  const co_time_of_day_t tod =
      *static_cast<const co_time_of_day_t*>(co_sub_get_val(sub00));
  const co_unsigned16_t days = tod.days;
  const co_unsigned32_t miliseconds = tod.ms;
  CHECK_EQUAL(0u, days);
  CHECK_EQUAL(0u, miliseconds);
}

TEST(Dcf2Dev_AllTypes, TimeDiff_Default) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x301Cu);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_OF_DAY, co_sub_get_type(sub00));
  const co_time_of_day_t td =
      *static_cast<const co_time_of_day_t*>(co_sub_get_val(sub00));
  const co_unsigned16_t days = td.days;
  const co_unsigned32_t miliseconds = td.ms;
  CHECK_EQUAL(0u, days);
  CHECK_EQUAL(0u, miliseconds);
}

TEST(Dcf2Dev_AllTypes, TimeSCET) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3060u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_SCET, co_sub_get_type(sub00));
  const co_time_scet_t scet =
      *static_cast<const co_time_scet_t*>(co_sub_get_val(sub00));
  const co_unsigned32_t sec = scet.seconds;
  const co_unsigned24_t subsec = scet.subseconds;
  CHECK_EQUAL(32u, sec);
  CHECK_EQUAL(213u, subsec);
}

TEST(Dcf2Dev_AllTypes, TimeSUTC) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3061u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_SUTC, co_sub_get_type(sub00));
  const co_time_sutc_t sutc =
      *static_cast<const co_time_sutc_t*>(co_sub_get_val(sub00));
  const co_unsigned16_t days = sutc.days;
  const co_unsigned32_t ms = sutc.ms;
  const co_unsigned32_t usec = sutc.usec;
  CHECK_EQUAL(321u, days);
  CHECK_EQUAL(213123u, ms);
  CHECK_EQUAL(123u, usec);
}

TEST(Dcf2Dev_AllTypes, TimeSCET_Default) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3160u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_SCET, co_sub_get_type(sub00));
  const co_time_scet_t scet =
      *static_cast<const co_time_scet_t*>(co_sub_get_val(sub00));
  const co_unsigned32_t sec = scet.seconds;
  const co_unsigned24_t subsec = scet.subseconds;
  CHECK_EQUAL(0, sec);
  CHECK_EQUAL(0, subsec);
}

TEST(Dcf2Dev_AllTypes, TimeSUTC_Default) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3161u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_TIME_SUTC, co_sub_get_type(sub00));
  const co_time_sutc_t sutc =
      *static_cast<const co_time_sutc_t*>(co_sub_get_val(sub00));
  const co_unsigned16_t days = sutc.days;
  const co_unsigned32_t ms = sutc.ms;
  const co_unsigned32_t usec = sutc.usec;
  CHECK_EQUAL(0, days);
  CHECK_EQUAL(0, ms);
  CHECK_EQUAL(0, usec);
}

TEST(Dcf2Dev_AllTypes, Domain) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x300Fu);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);

  CHECK_EQUAL(CO_ACCESS_RW, co_sub_get_access(sub00));
  CHECK_EQUAL(CO_DEFTYPE_DOMAIN, co_sub_get_type(sub00));
}

TEST(Dcf2Dev_AllTypes, Integer24) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3010u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER24, i24, sub00, CO_ACCESS_RW, -0x7FFFFF);
}

TEST(Dcf2Dev_AllTypes, Real64) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3011u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(REAL64, r64, sub00, CO_ACCESS_RW, 14567.46377);
}

TEST(Dcf2Dev_AllTypes, Integer40) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3012u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER40, i40, sub00, CO_ACCESS_RW,
                              -0x7FFFFFFFFF);
}

TEST(Dcf2Dev_AllTypes, Integer48) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3013u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER48, i48, sub00, CO_ACCESS_RW,
                              -0x7FFFFFFFFFFF);
}

TEST(Dcf2Dev_AllTypes, Integer56) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3014u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER56, i56, sub00, CO_ACCESS_RW,
                              -0x7FFFFFFFFFFFFF);
}

TEST(Dcf2Dev_AllTypes, Integer64) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3015u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(INTEGER64, i64, sub00, CO_ACCESS_RW,
                              -0x7FFFFFFFFFFFFFFF);
}

TEST(Dcf2Dev_AllTypes, Unsigned24) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3016u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED24, u24, sub00, CO_ACCESS_RW, 0x7FFFFFu);
}

TEST(Dcf2Dev_AllTypes, UNSIGNED40) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3018u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED40, u40, sub00, CO_ACCESS_RW,
                              0x7FFFFFFFFFu);
}

TEST(Dcf2Dev_AllTypes, Unsigned48) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x3019u);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED48, u48, sub00, CO_ACCESS_RW,
                              0x7FFFFFFFFFFFu);
}

TEST(Dcf2Dev_AllTypes, Unsigned56) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x301Au);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED56, u56, sub00, CO_ACCESS_RW,
                              0x7FFFFFFFFFFFFFu);
}

TEST(Dcf2Dev_AllTypes, Unsigned64) {
  co_unsigned8_t subidx = 0xFFu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x301Bu);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED64, u64, sub00, CO_ACCESS_RW,
                              0x7FFFFFFFFFFDFDFFu);
}

TEST(Dcf2Dev_AllTypes, ValuesOfSub0x301E) {
  const co_unsigned8_t SUBS_NUM = 2u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x301Eu);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, subindexes[i]);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_RO,
                              0x01u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RW,
                              0xFFFFFFFFu);
}
