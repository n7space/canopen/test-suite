/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <array>

#include <CppUTest/TestHarness.h>

#include <lely/co/dev.h>
#include <lely/co/obj.h>
#include <stdbool.h>

#include "test-base.hpp"

#include "dcf-correct/dcf/compact-tpdo.h"

TEST_GROUP_BASE(Dcf2Dev_CompactTpdo, Dcf2Dev_Base) {
  co_dev_t* dev = nullptr;

  TEST_SETUP() { dev = dcf_compact_tpdo_init(); }
};

TEST(Dcf2Dev_CompactTpdo, SubobjectsInObj1801) {
  const co_unsigned8_t SUBS_NUM = 7u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1801u);
  CHECK(obj != nullptr);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, subindexes[i]);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_CONST,
                              0x06u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RW,
                              0x00000290u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[2], CO_ACCESS_RW,
                              0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED16, u16, subobjects[3], CO_ACCESS_RW,
                              0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[4], CO_ACCESS_RW, 0x00);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED16, u16, subobjects[5], CO_ACCESS_RW,
                              0x0000u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[6], CO_ACCESS_RW,
                              0x00u);
}
