/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <array>

#include <CppUTest/TestHarness.h>

#include <lely/co/dev.h>
#include <lely/co/obj.h>
#include <stdbool.h>

#include "test-base.hpp"

#include "dcf-correct/dcf/discrepancies-warnings.h"

TEST_GROUP_BASE(Dcf2Dev_Warnings, Dcf2Dev_Base) {
  co_dev_t* dev = nullptr;

  TEST_SETUP() { dev = dcf_discrepancies_warnings_init(); }
};

TEST(Dcf2Dev_Warnings, Baud) {
  const uint16_t BAUD =
      0u | CO_BAUD_10 | CO_BAUD_20 | CO_BAUD_50 | CO_BAUD_250 | CO_BAUD_1000;
  const auto baud = co_dev_get_baud(dev);

  CHECK_EQUAL(BAUD, baud);
}

TEST(Dcf2Dev_Warnings, LssSupported) { CHECK_EQUAL(1, co_dev_get_lss(dev)); }

TEST(Dcf2Dev_Warnings, NetId) { CHECK_EQUAL(0x00u, co_dev_get_netid(dev)); }

TEST(Dcf2Dev_Warnings, Id) { CHECK_EQUAL(0x00u, co_dev_get_id(dev)); }

TEST(Dcf2Dev_Warnings, Indexes) {
  const co_unsigned8_t INDEXES_NUM = 14u;
  const std::array<co_unsigned16_t, INDEXES_NUM> INDEXES{
      0x1000u, 0x1001u, 0x1005u, 0x1007u, 0x1018u, 0x1029u, 0x1800u,
      0x1A00u, 0x2000u, 0x2001u, 0x2002u, 0x2003u, 0x2004u, 0x2005u};
  std::array<co_unsigned16_t, INDEXES_NUM> indexes{};

  const auto indexes_num = co_dev_get_idx(dev, INDEXES_NUM, indexes.data());

  CHECK_EQUAL(INDEXES_NUM, indexes_num);
  for (co_unsigned8_t i = 0u; i < INDEXES_NUM; i++)
    CHECK_EQUAL(INDEXES[i], indexes[i]);
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x1000) {
  co_unsigned8_t subidx = 0xffu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1000u);
  CHECK(obj != nullptr);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0x00u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, sub00, CO_ACCESS_RO,
                              0x00000000u);
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x1001) {
  co_unsigned8_t subidx = 0xffu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1001u);
  CHECK(obj != nullptr);

  const auto subindexes_num = co_obj_get_subidx(obj, 1u, &subidx);

  CHECK_EQUAL(1u, subindexes_num);
  CHECK_EQUAL(0x00u, subidx);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, subidx);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, sub00, CO_ACCESS_RO, 0x00u);
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x1018) {
  const co_unsigned8_t SUBS_NUM = 5u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1018u);
  CHECK(obj != nullptr);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, subindexes[i]);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_CONST,
                              0x04u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RO,
                              0xBB4E3753u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[2], CO_ACCESS_RO,
                              0xABABABABu);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[3], CO_ACCESS_RO,
                              0xABCDEF12u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[4], CO_ACCESS_RO,
                              0x12344321u);
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x1800) {
  const co_unsigned8_t SUBS_NUM = 3u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1800u);
  CHECK(obj != nullptr);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, i);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_CONST,
                              0x02u);
  const auto nodeid = co_dev_get_id(dev);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RW,
                              0x00000180u + nodeid);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[2], CO_ACCESS_RW,
                              0x01u);
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x1A00) {
  const co_unsigned8_t SUBS_NUM = 2u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x1A00u);
  CHECK(obj != nullptr);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, i);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_RO,
                              0x01u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RW,
                              0x20000020u);
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x2000) {
  co_unsigned8_t subindex = 0xffu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2000u);
  CHECK(obj != nullptr);

  const auto subindexes_num = co_obj_get_subidx(obj, 1, &subindex);

  CHECK_EQUAL(1, subindexes_num);
  CHECK_EQUAL(0x00u, subindex);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, 0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, sub00, CO_ACCESS_RW,
                              0x0000000Du);
  CHECK_EQUAL(1, co_sub_get_pdo_mapping(sub00));
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x2001) {
  co_unsigned8_t subindex = 0xffu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2001u);
  CHECK(obj != nullptr);

  const auto subindexes_num = co_obj_get_subidx(obj, 1, &subindex);

  CHECK_EQUAL(1, subindexes_num);
  CHECK_EQUAL(0x00u, subindex);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, 0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, sub00, CO_ACCESS_WO, 0x00u);
  CHECK_EQUAL(0, co_sub_get_pdo_mapping(sub00));
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x2002) {
  co_unsigned8_t subindex = 0xffu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2002u);
  CHECK(obj != nullptr);

  const auto subindexes_num = co_obj_get_subidx(obj, 1, &subindex);

  CHECK_EQUAL(1, subindexes_num);
  CHECK_EQUAL(0x00u, subindex);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, 0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, sub00, CO_ACCESS_RWR, 0x00u);
  CHECK_EQUAL(0, co_sub_get_pdo_mapping(sub00));
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x2003) {
  co_unsigned8_t subindex = 0xffu;
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2003u);
  CHECK(obj != nullptr);

  const auto subindexes_num = co_obj_get_subidx(obj, 1, &subindex);

  CHECK_EQUAL(1, subindexes_num);
  CHECK_EQUAL(0x00u, subindex);

  const co_sub_t* const sub00 = co_obj_find_sub(obj, 0x00u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, sub00, CO_ACCESS_RWW, 0x32u);
  CHECK_EQUAL(0, co_sub_get_pdo_mapping(sub00));
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x2004) {
  const co_unsigned8_t SUBS_NUM = 2u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2004u);
  CHECK(obj != nullptr);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, i);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_CONST,
                              0x01u);
}

TEST(Dcf2Dev_Warnings, SubobjectsInObj0x2005) {
  const co_unsigned8_t SUBS_NUM = 4u;
  std::array<co_unsigned8_t, SUBS_NUM> subindexes{};
  const co_obj_t* const obj = co_dev_find_obj(dev, 0x2005u);
  CHECK(obj != nullptr);

  const auto subindexes_num =
      co_obj_get_subidx(obj, SUBS_NUM, subindexes.data());

  CHECK_EQUAL(SUBS_NUM, subindexes_num);
  for (co_unsigned8_t i = 0; i < SUBS_NUM; i++) CHECK_EQUAL(i, subindexes[i]);

  std::array<co_sub_t*, SUBS_NUM> subobjects{};
  for (co_unsigned8_t i = 0u; i < SUBS_NUM; i++)
    subobjects[i] = co_obj_find_sub(obj, i);

  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED8, u8, subobjects[0], CO_ACCESS_RO,
                              0x03u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[1], CO_ACCESS_RW,
                              0x12341234u);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[2], CO_ACCESS_RW,
                              0xABCDABCDu);
  CHECK_SUB_TYPE_ACCESS_VALUE(UNSIGNED32, u32, subobjects[3], CO_ACCESS_RW,
                              0x00000000u);
}
