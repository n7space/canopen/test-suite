/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "dcf/emcy-producer.h"
#include "emcy.h"

enum emcy_state {
	EMCY_START,
	EMCY_SEND_ERRORS,
	EMCY_CLEAR,
	EMCY_DONE,
};

static enum emcy_state state = EMCY_START;

static uint32_t sentMsgCounter = 0;

void
TestSetup(can_net_t *const net)
{
	dev = dcf_emcy_producer_init();

	emcy = co_emcy_create(net, dev);

	co_emcy_start(emcy);

	state = EMCY_SEND_ERRORS;
}

void
TestTeardown(void)
{
	co_emcy_stop(emcy);
	co_emcy_destroy(emcy);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("producer: received unexpected message");
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	switch (state) {
	case EMCY_SEND_ERRORS:;
		co_unsigned8_t error_reg = 0x01;

		for (size_t i = 0; i < EMCY_MULTI_SEQ_SIZE; ++i) {
			const ssize_t idx = ABS(EMCY_MULTI_SEQ[i].idx);
			const co_unsigned16_t eec = ERROR_TABLE[idx].eec;
			const co_unsigned8_t er = ERROR_TABLE[idx].er;

			if (EMCY_MULTI_SEQ[i].idx > 0) {
				REQUIRE(co_emcy_push(emcy, eec, er, NULL));
				error_reg |= er;

				CheckErrorReg(error_reg);
				CheckLastErrorMsg(eec, error_reg);
			} else {
				co_unsigned16_t pop_eec = 0;
				co_unsigned8_t pop_er = 0;
				REQUIRE(co_emcy_pop(emcy, &pop_eec, &pop_er));

				if (eec != pop_eec) {
					LOG_UINT32_EXPECTED("popped error code",
							eec, pop_eec);
					FAIL_TEST("producer: popped incorrect error code");
				} else if (error_reg != pop_er) {
					LOG_UINT32_EXPECTED(
							"popped error register",
							pop_er, error_reg);
					FAIL_TEST("producer: popped incorrect error register");
				}

				if (EMCY_MULTI_SEQ[i].clear_er) {
					error_reg &= (co_unsigned8_t)~er;
				}
				CheckErrorReg(error_reg);
			}
		}
		state = EMCY_CLEAR;

		break;
	case EMCY_CLEAR:
		REQUIRE(co_emcy_clear(emcy));

		CheckErrorReg(0);
		CheckLastErrorMsg(0, 0);

		if (sentMsgCounter != (EMCY_MULTI_SEQ_SIZE + 1)) {
			LOG_UINT32_EXPECTED("sent messages",
					(EMCY_MULTI_SEQ_SIZE + 1),
					sentMsgCounter);
			FAIL_TEST("producer: incorrect sent messages count");
		} else {
			FINISH_TEST();
		}

		state = EMCY_DONE;

		break;
	default: FAIL_TEST("producer: invalid test state");
	}
}
