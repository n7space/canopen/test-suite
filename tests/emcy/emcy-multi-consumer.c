/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "dcf/emcy-consumer.h"
#include "emcy.h"

enum emcy_state {
	EMCY_START,
	EMCY_RECV_ERROR,
	EMCY_CLEAR,
	EMCY_DONE,
};

static enum emcy_state state = EMCY_START;

static uint32_t msgCounter = 0;
static uint32_t errorCounter = 0;
static co_unsigned8_t error_reg = 0x01;

static void
UpdateErrorReg(void)
{
	const ssize_t idx = ABS(EMCY_MULTI_SEQ[errorCounter].idx);

	if (EMCY_MULTI_SEQ[errorCounter].idx > 0) {
		error_reg |= ERROR_TABLE[idx].er;
	} else {
		if (EMCY_MULTI_SEQ[errorCounter].clear_er)
			error_reg &= ((co_unsigned8_t) ~(ERROR_TABLE[idx].er));
	}
}

static void
CheckEmcyMessage(co_unsigned16_t eec, co_unsigned8_t er)
{
	const ssize_t idx = ABS(EMCY_MULTI_SEQ[errorCounter].idx);
	const co_unsigned16_t expected_eec =
			EMCY_MULTI_SEQ[errorCounter].idx > 0
			? ERROR_TABLE[idx].eec
			: 0;

	if (eec != expected_eec)
		FailErrorCode(expected_eec, eec);
	else if (er != error_reg)
		FailErrorReg(error_reg, er);
}

static inline void
emcy_ind_consumer(co_emcy_t *emcy_, co_unsigned8_t id, co_unsigned16_t eec,
		co_unsigned8_t er, co_unsigned8_t msef[5], void *data)
{
	assert(emcy_ == emcy);

	emcy_ind_func(emcy, id, eec, er, msef, data);

	switch (state) {
	case EMCY_RECV_ERROR:
		UpdateErrorReg();
		CheckEmcyMessage(eec, er);
		++errorCounter;

		if (errorCounter == EMCY_MULTI_SEQ_SIZE)
			state = EMCY_CLEAR;

		break;
	case EMCY_CLEAR:
		if (eec != 0)
			FailErrorCode(0, eec);
		else if (er != 0)
			FailErrorReg(0, er);
		else
			state = EMCY_DONE;

		break;
	case EMCY_DONE:
		FAIL_TEST("consumer: received EMCY indication after test have finished");
		break;
	default: FAIL_TEST("consumer: invalid test state");
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_emcy_consumer_init();

	emcy = co_emcy_create(net, dev);
	co_emcy_set_ind(emcy, emcy_ind_consumer, NULL);

	co_emcy_start(emcy);

	state = EMCY_RECV_ERROR;
}

void
TestTeardown(void)
{
	co_emcy_stop(emcy);
	co_emcy_destroy(emcy);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (state == EMCY_DONE) {
		if (msgCounter != (EMCY_MULTI_SEQ_SIZE + 1)) {
			LOG_UINT32_EXPECTED("messages count",
					(EMCY_MULTI_SEQ_SIZE + 1), msgCounter);
			FAIL_TEST("consumer: incorrect messages count");
		} else if (emcyCounter != (EMCY_MULTI_SEQ_SIZE + 1)) {
			LOG_UINT32_EXPECTED("EMCY indications",
					(EMCY_MULTI_SEQ_SIZE + 1), emcyCounter);
			FAIL_TEST("consumer: incorrect EMCY indications count");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("consumer: unexpected message sent");
}

void
TestStep(void)
{
	// noop
}
