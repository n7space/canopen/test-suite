/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "dcf/emcy-producer.h"
#include "emcy.h"

enum emcy_state {
	EMCY_START,
	EMCY_SEND_ERROR,
	EMCY_CLEAR,
	EMCY_DONE,
};

static enum emcy_state state = EMCY_START;

static uint32_t sentMsgCounter = 0;
static uint32_t errorCounter = 0;

void
TestSetup(can_net_t *const net)
{
	dev = dcf_emcy_producer_init();

	emcy = co_emcy_create(net, dev);

	co_emcy_start(emcy);

	state = EMCY_SEND_ERROR;
}

void
TestTeardown(void)
{
	co_emcy_stop(emcy);
	co_emcy_destroy(emcy);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("producer: received unexpected message");
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	switch (state) {
	case EMCY_SEND_ERROR:;
		const co_unsigned16_t eec = ERROR_TABLE[errorCounter].eec;
		const co_unsigned8_t er = ERROR_TABLE[errorCounter].er;

		REQUIRE(co_emcy_push(emcy, eec, er, NULL));

		CheckErrorReg(0x01u | er);
		CheckLastErrorMsg(eec, 0x01u | er);

		++errorCounter;
		state = EMCY_CLEAR;

		break;
	case EMCY_CLEAR:
		REQUIRE(co_emcy_clear(emcy));

		CheckErrorReg(0);
		CheckLastErrorMsg(0, 0);

		if (errorCounter < ERROR_TABLE_SIZE) {
			state = EMCY_SEND_ERROR;
		} else {
			state = EMCY_DONE;

			if (sentMsgCounter != EMCY_MSG_NUMBER) {
				LOG_UINT32_EXPECTED("sent messages count",
						EMCY_MSG_NUMBER,
						sentMsgCounter);
				FAIL_TEST("producer: incorrect sent message count");
			} else {
				FINISH_TEST();
			}
		}

		break;
	default: FAIL_TEST("producer: invalid test state");
	}
}
