/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/emcy-inhibit-time-consumer.h"
#include "emcy.h"

static uint32_t msgCounter = 0;
static struct timespec lastTime = { 0, 0 };

static void
emcy_ind_inhibit_consumer(co_emcy_t *emcy_, co_unsigned8_t id,
		co_unsigned16_t eec, co_unsigned8_t er, co_unsigned8_t msef[5],
		void *data)
{
	assert(emcy_ == emcy);

	emcy_ind_func(emcy, id, eec, er, msef, data);

	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	if (emcyCounter > 1u) {
		const int32_t delta = (int32_t)timespec_diff_msec(
				&currentTime, &lastTime);

		if (!CheckTimeIntervalWithAccuracy(delta, EMCY_INHIBIT_TIME_MS,
				    HOST_TIME_ACCURACY)) {
			LOG_INT32_EXPECTED("delay between messages",
					EMCY_INHIBIT_TIME_MS, delta);
			FAIL_TEST("consumer: incorrect delay between messages");
		}
	}

	lastTime = currentTime;
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_emcy_inhibit_time_consumer_init();

	emcy = co_emcy_create(net, dev);
	co_emcy_set_ind(emcy, emcy_ind_inhibit_consumer, NULL);

	co_emcy_start(emcy);
}

void
TestTeardown(void)
{
	co_emcy_stop(emcy);
	co_emcy_destroy(emcy);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (emcyCounter == EMCY_INHIBIT_NUMBER) {
		if (msgCounter != EMCY_INHIBIT_NUMBER) {
			LOG_UINT32_EXPECTED("received messages",
					EMCY_INHIBIT_NUMBER, msgCounter);
			FAIL_TEST("consumer: incorrect received messages count");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("consumer: unexpected message sent");
}

void
TestStep(void)
{
	// noop
}
