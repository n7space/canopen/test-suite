/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_EMCY_H__
#define CTSSW_EMCY_H__

#include <assert.h>
#include <stdbool.h>

#include <lely/co/dev.h>
#include <lely/co/emcy.h>
#include <lely/co/type.h>

#include <TestFramework/TestHarness.h>

static co_dev_t *dev = NULL;
static co_emcy_t *emcy = NULL;

static const co_unsigned8_t EMCY_PROD_ID = 0x01u;

// EMCY error codes and register
struct emcy_error {
	co_unsigned16_t eec;
	co_unsigned8_t er;
};

#define ERROR_TABLE_SIZE 11u
static const struct emcy_error ERROR_TABLE[ERROR_TABLE_SIZE] = {
	{ 0x1000u, 0x01u }, // generic
	{ 0x2000u, 0x01u << 1u }, // current
	{ 0x3000u, 0x01u << 2u }, // voltage
	{ 0x4000u, 0x01u << 3u }, // temperature
	{ 0x5000u, 0x01u << 4u }, // device hardware
	{ 0x6000u, 0x01u << 5u }, // device software
	{ 0x7000u, 0x01u << 7u }, // additional modules
	{ 0x8000u, 0x01u << 5u }, // monitoring
	{ 0x9000u, 0x01u << 7u }, // external error
	{ 0xF000u, 0x01u << 5u }, // additional functions
	{ 0xFF00u, 0x01u << 5u }, // device specific
};
static const uint32_t EMCY_MSG_NUMBER = 2 * ERROR_TABLE_SIZE;

// EMCY multi errors
struct emcy_multi_seq {
	ssize_t idx;
	bool clear_er;
};
#define EMCY_MULTI_SEQ_SIZE 10u
static const struct emcy_multi_seq EMCY_MULTI_SEQ[EMCY_MULTI_SEQ_SIZE] = {
	{ 2, false }, { 3, false }, { 5, false }, { 7, false }, { 3, false },
	{ -3, false }, { 1, false }, { -1, true }, { -7, false }, { -5, true }
};

// EMCY inhibit time
static const uint32_t EMCY_INHIBIT_NUMBER = 8u;
static const uint32_t EMCY_INHIBIT_TIME_MS = 1000u;

// EMCY default callbacks
static uint32_t emcyCounter = 0;

static inline void
emcy_ind_func(co_emcy_t *emcy_, co_unsigned8_t id, co_unsigned16_t eec,
		co_unsigned8_t er, co_unsigned8_t msef[5], void *data)
{
	(void)id;
	(void)eec;
	(void)er;
	(void)msef;
	(void)data;

	assert(emcy_ == emcy);

	++emcyCounter;

	if (id != EMCY_PROD_ID)
		FailTest("consumer: invalid EMCY producer Node-ID: %u", id);
}

static inline void
FailErrorCode(co_unsigned16_t expected, co_unsigned16_t recv)
{
	LOG_UINT32_EXPECTED("error code", expected, recv);
	FAIL_TEST("consumer: received invalid error code");
}

static inline void
FailErrorReg(co_unsigned8_t expected, co_unsigned8_t recv)
{
	LOG_UINT32_EXPECTED("error register", expected, recv);
	FAIL_TEST("consumer: received invalid error register");
}

static inline void
CheckErrorReg(co_unsigned8_t er)
{
	co_unsigned8_t reg = co_dev_get_val_u8(dev, 0x1001, 0x00);

	if (er != reg) {
		LOG_UINT32_EXPECTED("error register", er, reg);
		FAIL_TEST("producer: error register differs");
	}
}

static inline void
CheckLastErrorMsg(co_unsigned16_t eec, co_unsigned8_t er)
{
	co_unsigned16_t code = 0;
	co_unsigned8_t reg = 0;

	co_emcy_peek(emcy, &code, &reg);

	if (eec != code) {
		LOG_UINT32_EXPECTED("error code", eec, code);
		FAIL_TEST("producer: last message error code differs");
	} else if (er != reg) {
		LOG_UINT32_EXPECTED("error register", er, reg);
		FAIL_TEST("producer: last message error register differs");
	}
}

#endif // CTSSW_EMCY_H__
