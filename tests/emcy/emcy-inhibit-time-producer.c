/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/emcy-inhibit-time-producer.h"
#include "emcy.h"

enum emcy_inhibit_state {
	EMCY_INHIBIT_START,
	EMCY_INHIBIT_QUEUE_ERRORS,
	EMCY_INHIBIT_SENDING,
	EMCY_INHIBIT_CLEAR,
	EMCY_INHIBIT_DONE,
};

enum emcy_inhibit_state state = EMCY_INHIBIT_START;

static uint32_t sentMsgCounter = 0;
static struct timespec lastTime = { 0, 0 };

void
TestSetup(can_net_t *const net)
{
	dev = dcf_emcy_inhibit_time_producer_init();

	emcy = co_emcy_create(net, dev);

	co_emcy_start(emcy);

	state = EMCY_INHIBIT_QUEUE_ERRORS;
}

void
TestTeardown(void)
{
	co_emcy_stop(emcy);
	co_emcy_destroy(emcy);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("producer: received unexpected message");
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;

	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	if (sentMsgCounter > 1u) {
		const int32_t delta = (int32_t)timespec_diff_msec(
				&currentTime, &lastTime);

		if (!CheckTimeInterval(delta, EMCY_INHIBIT_TIME_MS)) {
			LOG_INT32_EXPECTED("event-time interval",
					EMCY_INHIBIT_TIME_MS, delta);
			FAIL_TEST("producer: incorrect event-timer interval");
		}
	}

	if ((state == EMCY_INHIBIT_SENDING)
			&& (sentMsgCounter == (EMCY_INHIBIT_NUMBER - 1u))) {
		state = EMCY_INHIBIT_CLEAR;
	} else if (sentMsgCounter == EMCY_INHIBIT_NUMBER) {
		FINISH_TEST();
	}

	lastTime = currentTime;
}

void
TestStep(void)
{
	switch (state) {
	case EMCY_INHIBIT_QUEUE_ERRORS:
		// see CiA 301 7.5.2.2 Object 1001h: Error register
		// and Table 25: Emergency error code classes
		REQUIRE(co_emcy_push(emcy, 0x1000u, 0x01u, NULL));
		REQUIRE(co_emcy_push(emcy, 0x2000u, 1u << 1u, NULL));
		REQUIRE(co_emcy_push(emcy, 0x3000u, 1u << 2u, NULL));
		REQUIRE(co_emcy_push(emcy, 0x4000u, 1u << 3u, NULL));
		REQUIRE(co_emcy_push(emcy, 0x8100u, 1u << 4u, NULL));
		REQUIRE(co_emcy_push(emcy, 0xff00u, 1u << 5u, NULL));
		REQUIRE(co_emcy_push(emcy, 0x9000u, 1u << 7u, NULL));
		state = EMCY_INHIBIT_SENDING;
		break;
	case EMCY_INHIBIT_CLEAR:
		REQUIRE(co_emcy_clear(emcy));
		state = EMCY_INHIBIT_DONE;
		break;
	default:;
	}
}
