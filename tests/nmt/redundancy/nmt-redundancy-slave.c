/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-redundancy-slave.h"
#include "nmt-redundancy.h"

static enum nmt_slave_redundancy state = NMT_SLAVE_START;

static uint32_t sentMsgCounter = 0;

static struct timespec testStartTime = { 0, 0 };

static void TestMessageReceivedB(const struct can_msg *const msg);
static void TestMessageSentB(const struct can_msg *const msg);
static void TestIgnoredMessageB(const struct can_msg *const msg);

// NMT callbacks
static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_SLAVE_ID) {
		LOG_INT32_EXPECTED("slave id", NMT_SLAVE_ID,
				co_dev_get_id(device));
		FAIL_TEST("slave: invalid Node-Id in cs_ind()");
	} else {
		LOG_UINT32("slave: received command", cs);
	}
}

static void
hb_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_nmt_ec_state_t hb_state,
		co_nmt_ec_reason_t reason, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	co_nmt_on_hb(nmt, id, hb_state, reason);

	LOG("slave: heartbeat event occurred");
	LOG_UINT32("id", id);
	LOG_INT32("state", hb_state);
	LOG_INT32("reason", reason);

	switch (state) {
	case NMT_SLAVE_BUS_SELECTION:
		if ((reason == CO_NMT_EC_STATE)
				&& (hb_state == CO_NMT_EC_OCCURRED)) {
			LOG("slave: finished bus selection, going operational");
			REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_START));
			state = NMT_SLAVE_TEST;
		} else {
			LOG_INT32_EXPECTED("hb_state", CO_NMT_EC_OCCURRED,
					hb_state);
			LOG_INT32_EXPECTED("hb_state", CO_NMT_EC_STATE, reason);
			FAIL_TEST("slave: incorrect state/reason in hb_ind() while"
				  " in BUS_SELECTION state");
		}
		break;
	case NMT_SLAVE_TEST:
		if ((reason == CO_NMT_EC_TIMEOUT)
				&& (hb_state == CO_NMT_EC_OCCURRED)
				&& (co_nmt_get_st(nmt) == CO_NMT_ST_PREOP)) {
			state = NMT_SLAVE_HB_TIMEOUT;
		} else {
			LOG_UINT32_EXPECTED("st", CO_NMT_ST_PREOP,
					co_nmt_get_st(nmt));
			LOG_INT32_EXPECTED("hb_state", CO_NMT_EC_OCCURRED,
					hb_state);
			LOG_INT32_EXPECTED("reason", CO_NMT_EC_TIMEOUT, reason);
			FAIL_TEST("slave: incorrect state/reason in hb_ind() while"
				  " in TEST state");
		}
		break;
	case NMT_SLAVE_BUS_SWITCH:
		if ((reason == CO_NMT_EC_TIMEOUT)
				&& (hb_state == CO_NMT_EC_RESOLVED)) {
			state = NMT_SLAVE_DONE;
		} else {
			LOG_INT32_EXPECTED("hb_state", CO_NMT_EC_OCCURRED,
					hb_state);
			LOG_INT32_EXPECTED("reason", CO_NMT_EC_TIMEOUT, reason);
			FAIL_TEST("slave: incorrect state/reason in hb_ind() while"
				  " in BUS_SWITCH state");
		}
		break;
	default: FAIL_TEST("slave: invalid test state in hb_ind()");
	}
}

static void
rdn_ind(co_nmt_t *nmt_, co_unsigned8_t bus_id, co_nmt_ecss_rdn_reason_t reason,
		void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("slave: redundancy event occurred");
	LOG_UINT32("bus_id", bus_id);
	LOG_INT32("reason", reason);

	switch (state) {
	case NMT_SLAVE_HB_TIMEOUT:
		if (reason == CO_NMT_ECSS_RDN_BUS_SWITCH) {
			state = NMT_SLAVE_BUS_SWITCH;
		} else {
			LOG_INT32_EXPECTED("reason", CO_NMT_ECSS_RDN_BUS_SWITCH,
					reason);
			FAIL_TEST("slave: incorrect reason in rdn_ind() "
				  "while in HB_TIMEOUT state");
		}
		break;
	default: FAIL_TEST("slave: invalid test state in rdn_ind()");
	}
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id == NMT_MASTER_ID) {
		LOG_UINT32("slave: master state changed to", st);
	} else {
		LOG_UINT32("slave: state changed to", st);

		if ((state == NMT_SLAVE_TEST) && (st != CO_NMT_ST_PREOP)) {
			LOG_UINT32_EXPECTED("st", CO_NMT_ST_PREOP, st);
			FAIL_TEST("slave: incorrect state change in TEST state");
		}
	}
}

void
TestSetup(can_net_t *const net)
{
	SetCanCallbacksBusB(TestMessageSentB, TestMessageReceivedB);
	SetIgnoredMessageCallbackBusB(TestIgnoredMessageB);

	dev = dcf_nmt_redundancy_slave_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("slave: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_ecss_rdn_ind(nmt, &rdn_ind, NULL);
	co_nmt_set_hb_ind(nmt, &hb_ind, NULL);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);

	REQUIRE(co_nmt_set_alternate_bus_id(nmt, CO_NMT_BUS_B_ID));
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;
	LOG("slave: received message on CAN A");
}

static void
TestMessageReceivedB(const struct can_msg *const msg)
{
	(void)msg;
	LOG("slave: received message on CAN B");
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
	++sentMsgCounter;

	if (sentMsgCounter > 1)
		FAIL_TEST("slave: unexpected sent message on CAN A");
}

static void
TestMessageSentB(const struct can_msg *const msg)
{
	(void)msg;
	FAIL_TEST("slave: unexpected sent message on CAN B");
}

static void
TestIgnoredMessageB(const struct can_msg *const msg)
{
	(void)msg;
	LOG("slave: ignored message on CAN B");
}

void
TestStep(void)
{
	switch (state) {
	case NMT_SLAVE_START:;
		// wait for master to initialize
		struct timespec currentTime = { 0, 0 };
		GetCurrentTestTime(&currentTime);

		if (timespec_diff_sec(&currentTime, &testStartTime)
				< NMT_WAIT_INTERVAL_SEC)
			break;

		LOG("slave: enter TEST state");
		state = NMT_SLAVE_BUS_SELECTION;

		LOG("slave: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_SLAVE_DONE: FINISH_TEST(); break;
	default: break;
	}
}
