/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-redundancy-startup-slave.h"
#include "nmt-redundancy.h"

static enum nmt_slave_redundancy state = NMT_SLAVE_START;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec testStartTime = { 0, 0 };

// NMT callbacks
static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_SLAVE_ID) {
		LOG_INT32_EXPECTED("slave id", NMT_SLAVE_ID,
				co_dev_get_id(device));
		FAIL_TEST("slave: invalid Node-Id in cs_ind()");
	} else {
		LOG_UINT32("slave: received command", cs);
	}
}

static void
hb_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_nmt_ec_state_t hb_state,
		co_nmt_ec_reason_t reason, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("slave: heartbeat event occured");
	LOG_UINT32("id", id);
	LOG_INT32("state", hb_state);
	LOG_INT32("reason", reason);

	if (state == NMT_SLAVE_BUS_SELECTION) {
		if ((hb_state == CO_NMT_EC_OCCURRED)
				&& (reason == CO_NMT_EC_STATE)) {
			LOG("slave: normal operation");
			state = NMT_SLAVE_NORMAL_OPERATION;
		} else {
			LOG_INT32_EXPECTED("hb_state", CO_NMT_EC_OCCURRED,
					hb_state);
			LOG_INT32_EXPECTED("reason", CO_NMT_EC_TIMEOUT, reason);
			FAIL_TEST("slave: incorrect state/reason in hb_ind() while"
				  " in BUS_SELECTION state");
		}
	} else {
		FAIL_TEST("slave: invalid state in hb_ind()");
	}
}

static void
rdn_ind(co_nmt_t *nmt_, co_unsigned8_t bus_id, co_nmt_ecss_rdn_reason_t reason,
		void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("slave: redundancy event occured");
	LOG_UINT32("bus_id", bus_id);
	LOG_INT32("reason", reason);

	FAIL_TEST("slave: unexpected redundancy event");
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id == NMT_MASTER_ID) {
		LOG_UINT32("slave: master state changed to", st);
	} else {
		LOG_UINT32("slave: state changed to", st);

		if (state == NMT_SLAVE_NORMAL_OPERATION) {
			if (st == CO_NMT_ST_START)
				state = NMT_SLAVE_TEST;
			else
				FAIL_TEST("slave: invalid state in NORMAL_OPERATION test state");
		}
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_redundancy_startup_slave_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("slave: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_ecss_rdn_ind(nmt, &rdn_ind, NULL);
	co_nmt_set_hb_ind(nmt, &hb_ind, NULL);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);

	REQUIRE(co_nmt_set_alternate_bus_id(nmt, CO_NMT_BUS_B_ID));
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
	if (state == NMT_SLAVE_TEST)
		state = NMT_SLAVE_DONE;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_SLAVE_START:;
		// wait for master to initialize
		struct timespec currentTime = { 0, 0 };
		GetCurrentTestTime(&currentTime);

		if (timespec_diff_sec(&currentTime, &testStartTime)
				< NMT_WAIT_INTERVAL_SEC)
			break;

		LOG("slave: enter BUS_SELECTION state");
		state = NMT_SLAVE_BUS_SELECTION;
		msgCounter = 0;

		LOG("slave: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_SLAVE_DONE:
		if (CheckMessages(NMT_RDN_STARTUP_SLAVE_RECV_MSG_NUMBER,
				    msgCounter,
				    NMT_RDN_STARTUP_SLAVE_SENT_MSG_NUMBER,
				    sentMsgCounter)
				&& (co_nmt_get_active_bus_id(nmt)
						== CO_NMT_BUS_A_ID)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
