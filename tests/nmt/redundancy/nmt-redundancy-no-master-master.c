/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-redundancy-no-master-master.h"
#include "nmt-redundancy.h"

static enum nmt_master_redundancy state = NMT_MASTER_START;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_MASTER_ID) {
		LOG_INT32_EXPECTED("master id", NMT_MASTER_ID,
				co_dev_get_id(device));
		FAIL_TEST("master: invalid Node-ID in cs_ind()");
	}

	LOG_UINT32("master: received command", cs);
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id == NMT_MASTER_ID) {
		LOG_UINT32("master: state changed to", st);
	} else {
		LOG_UINT32("master: slave changed state to", st);

		if (id != NMT_SLAVE_ID) {
			LOG_UINT32_EXPECTED("slave id", NMT_SLAVE_ID, id);
			FAIL_TEST("master: invalid slave ID");
		}
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_redundancy_no_master_master_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("master: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;
	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
	++sentMsgCounter;

	if (sentMsgCounter == NMT_RDN_NOMASTER_MASTER_SENT_MSG_NUMBER)
		state = NMT_MASTER_DONE;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_MASTER_START:
		LOG("master: enter TEST state");
		state = NMT_MASTER_TEST;

		LOG("master: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_MASTER_SWITCH_BUS:
		LOG("master: switching the active bus");
		if (co_nmt_set_active_bus(nmt, CO_NMT_BUS_B_ID) == 0)
			state = NMT_MASTER_TEST;
		else
			FAIL_TEST("master: failed to switch the active bus");
		break;
	case NMT_MASTER_DONE:
		if (CheckMessages(NMT_RDN_NOMASTER_SLAVE_SENT_MSG_NUMBER,
				    msgCounter,
				    NMT_RDN_NOMASTER_MASTER_SENT_MSG_NUMBER,
				    sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
