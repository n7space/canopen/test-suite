/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-service-slave.h"
#include "nmt-service.h"

#define NMT_SRV_SLAVE_SEQ_SIZE 6u
static const co_unsigned8_t nmt_slave_cs_seq[NMT_SRV_SLAVE_SEQ_SIZE] = {
	CO_NMT_CS_STOP,
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_START,
	CO_NMT_CS_STOP,
	CO_NMT_CS_START,
	CO_NMT_CS_ENTER_PREOP,
};

static const co_unsigned8_t nmt_slave_st_seq[NMT_SRV_SLAVE_SEQ_SIZE] = {
	CO_NMT_ST_STOP,
	CO_NMT_ST_PREOP,
	CO_NMT_ST_START,
	CO_NMT_ST_STOP,
	CO_NMT_ST_START,
	CO_NMT_ST_PREOP,
};

static const uint_least8_t nmt_slave_srv_seq[NMT_SRV_SLAVE_SEQ_SIZE] = {
	CO_NMT_STOP_SRV,
	CO_NMT_PREOP_SRV,
	CO_NMT_OPER_SRV,
	CO_NMT_STOP_SRV,
	CO_NMT_OPER_SRV,
	CO_NMT_PREOP_SRV,
};

static enum nmt_slave_service state = NMT_SLAVE_START;

static uint32_t csSeq = 0;
static uint32_t stSeq = 0;
static uint32_t preOpCounter = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec testWaitTime = { 0, 0 };

// NMT callbacks
static void
hb_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_nmt_ec_state_t state_,
		co_nmt_ec_reason_t reason, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("slave: heartbeat event occured");
	LOG_UINT32("id", id);
	LOG_INT32("state", state_);
	LOG_INT32("reason", reason);
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id != NMT_SLAVE_ID)
		FAIL_TEST("slave: invalid Node-ID in st_ind()");
	else {
		LOG_UINT32("slave: state changed to", st);

		if (state == NMT_SLAVE_TEST) {
			if (st == CO_NMT_ST_PREOP)
				++preOpCounter;
		} else if (state == NMT_SLAVE_TEST_SRV) {
			if (stSeq >= NMT_SRV_SLAVE_SEQ_SIZE) {
				FAIL_TEST("slave: invalid state sequence counter");
				return;
			}

			if (st != nmt_slave_st_seq[stSeq]) {
				LOG_UINT32_EXPECTED("st",
						nmt_slave_st_seq[stSeq], st);
				FAIL_TEST("slave: incorrect master state");
			}

			if (!CheckServices(nmt, nmt_slave_srv_seq[stSeq]))
				FAIL_TEST("slave: incorrect service state");

			++stSeq;
		}
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_service_slave_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("slave: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_hb_ind(nmt, &hb_ind, NULL);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_SLAVE_START:
		LOG("slave: enter TEST state");
		state = NMT_SLAVE_TEST;

		LOG("slave: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		if (!CheckServices(nmt, CO_NMT_PREOP_SRV))
			FAIL_TEST("slave: incorrect service state while in initial PREOP");

		GetCurrentTestTime(&testWaitTime);
		break;
	case NMT_SLAVE_TEST:;
		// wait for master to finish the test
		struct timespec currentTime = { 0, 0 };
		GetCurrentTestTime(&currentTime);

		if (timespec_diff_sec(&currentTime, &testWaitTime)
				> NMT_WAIT_INTERVAL_SEC) {
			LOG("master: enter TEST_SRV state");
			state = NMT_SLAVE_TEST_SRV;
		}
		break;
	case NMT_SLAVE_TEST_SRV:
		if (csSeq < NMT_SRV_SLAVE_SEQ_SIZE) {
			REQUIRE(co_nmt_cs_ind(nmt, nmt_slave_cs_seq[csSeq]));
			++csSeq;
		} else {
			state = NMT_SLAVE_DONE;
		}
		break;
	case NMT_SLAVE_DONE: FINISH_TEST(); break;
	default: break;
	}
}
