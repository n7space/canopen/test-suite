/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-reset-device-master.h"
#include "nmt-state.h"

static enum nmt_master_state state = NMT_MASTER_START;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec waitStartTime = { 0, 0 };
static enum nmt_master_state stateAfterWait = NMT_MASTER_DONE;

// NMT callbacks
static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_MASTER_ID) {
		LOG_INT32_EXPECTED("master id", NMT_MASTER_ID,
				co_dev_get_id(device));
		FAIL_TEST("master: invalid node id in cs_ind()");
	}

	LOG_UINT32("master: received command", cs);
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id != NMT_MASTER_ID)
		return;

	LOG_UINT32("master: state changed to", st);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_reset_device_master_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("master: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_MASTER_START:
		LOG("master: enter INIT state");
		state = NMT_MASTER_INIT;

		LOG("master: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_MASTER_INIT:
		state = NMT_MASTER_WAIT;
		stateAfterWait = NMT_MASTER_TEST;
		LOG("master: enter TEST state after WAIT");
		break;
	case NMT_MASTER_TEST:
		REQUIRE(co_nmt_cs_req(nmt, CO_NMT_CS_RESET_NODE, NMT_SLAVE_ID));
		state = NMT_MASTER_WAIT;
		stateAfterWait = NMT_MASTER_DONE;
		LOG("master: enter DONE state after WAIT");
		break;
	case NMT_MASTER_WAIT:;
		struct timespec currentTime = { 0, 0 };
		GetCurrentTestTime(&currentTime);

		if ((waitStartTime.tv_sec == 0)
				&& (waitStartTime.tv_nsec == 0)) {
			waitStartTime = currentTime;
			LOG("master: WAIT state started");
		}
		// wait for slave to finish the test phase to receive all NMT messages
		else if (timespec_diff_sec(&currentTime, &waitStartTime)
				> NMT_WAIT_INTERVAL_SEC) {
			state = stateAfterWait;
			waitStartTime.tv_sec = 0;
			waitStartTime.tv_nsec = 0;
			LOG("master: WAIT state finished");
		}
		break;
	case NMT_MASTER_DONE:
		if (CheckMessages(NMT_RESET_SLAVE_SENT_MSG_NUMBER, msgCounter,
				    NMT_RESET_MASTER_SENT_MSG_NUMBER,
				    sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
