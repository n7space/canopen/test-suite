# This file is part of the Test Suite build system.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Suite was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Import("env")

tests = []

nmtStateMaster = env.Dcf2Dev("nmt-state-master.dcf")
nmtStateSlave = env.Dcf2Dev("nmt-state-slave.dcf")
tests += env.MakeSymmetricalTestCase(
    "nmt-state-test-case",
    ["nmt-state-master.c"] + nmtStateMaster,
    ["nmt-state-slave.c"] + nmtStateSlave,
    trace={
        "title": "NMT local state transition",
        "traces": ["SRS-CANSW-FUN-NMT-040"],
        "doc": {
            "given": "Master/slave with NMT services set up.",
            "when": "Master/slave issues a local NMT request.",
            "then": "Master/slave transitions to a state defined by CiA 301 Figure 48 & 49.",
        },
    },
)

nmtRequestMaster = env.Dcf2Dev("nmt-request-master.dcf")
nmtRequestSlave = env.Dcf2Dev("nmt-request-slave.dcf")
tests += env.MakeSymmetricalTestCase(
    "nmt-request-test-case",
    ["nmt-request-master.c"] + nmtRequestMaster,
    ["nmt-request-slave.c"] + nmtRequestSlave,
    trace={
        "title": "NMT request",
        "traces": ["SRS-CANSW-FUN-NMT-010"],
        "doc": {
            "given": "Master and slave with NMT services set up.",
            "when": "Master sends a remote NMT request to slave.",
            "then": "Slave transitions to a state defined by CiA 301 Figure 48 & 49.",
        },
    },
)

nmtResetDevtMaster = env.Dcf2Dev("nmt-reset-device-master.dcf")
nmtResetDevtSlave = env.Dcf2Dev("nmt-reset-device-slave.dcf")
tests += env.MakeSymmetricalTestCase(
    "nmt-reset-device-test-case",
    ["nmt-reset-device-master.c"] + nmtResetDevtMaster,
    ["nmt-reset-device-slave.c"] + nmtResetDevtSlave,
    trace={
        "title": "NMT reset device",
        "traces": ["SRS-CANSW-FUN-NMT-011"],
        "doc": {
            "given": "Master and slave with NMT services set up.",
            "when": "Master sends a remote NMT 'reset node' request to slave.",
            "then": "Slave resets the CAN device.",
        },
    },
)


env.Alias("nmt-state-tests", tests)

Return("tests")
