/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-state-master.h"
#include "nmt-state.h"

// NMT state transitions numbered as in CiA 301 - Figure 48 & 49
static const co_unsigned8_t nmt_master_state_seq[NMT_STATE_MASTER_SEQ_SIZE] = {
	CO_NMT_ST_BOOTUP, // (15)
	CO_NMT_ST_BOOTUP, // (16)
	CO_NMT_ST_PREOP, //  (2)
	CO_NMT_ST_START, //  (3)
	CO_NMT_ST_PREOP, //  (4)
	CO_NMT_ST_STOP, //   (5)
	CO_NMT_ST_START, //  (6)
	CO_NMT_ST_STOP, //   (8)
	CO_NMT_ST_PREOP, //  (7)
	CO_NMT_ST_START,
	CO_NMT_ST_BOOTUP, // |(9)
	CO_NMT_ST_BOOTUP, // |
	CO_NMT_ST_PREOP, //  V
	CO_NMT_ST_STOP,
	CO_NMT_ST_BOOTUP, // |(10)
	CO_NMT_ST_BOOTUP, // |
	CO_NMT_ST_PREOP, //  V
	CO_NMT_ST_BOOTUP, // |(11)
	CO_NMT_ST_BOOTUP, // |
	CO_NMT_ST_PREOP, //  V
	CO_NMT_ST_START,
	CO_NMT_ST_BOOTUP, // |(12)
	CO_NMT_ST_PREOP, //  V
	CO_NMT_ST_STOP,
	CO_NMT_ST_BOOTUP, // |(13)
	CO_NMT_ST_PREOP, //  V
	CO_NMT_ST_BOOTUP, // |(14)
	CO_NMT_ST_PREOP, //  V
};

static const co_unsigned8_t nmt_master_cs_seq[NMT_STATE_MASTER_SEQ_SIZE] = {
	CO_NMT_CS_RESET_NODE, // (15)
	CO_NMT_CS_RESET_COMM, // (16)
	CO_NMT_CS_ENTER_PREOP, // (2)
	CO_NMT_CS_START, //       (3)
	CO_NMT_CS_ENTER_PREOP, // (4)
	CO_NMT_CS_STOP, //        (5)
	CO_NMT_CS_START, //       (6)
	CO_NMT_CS_STOP, //        (8)
	CO_NMT_CS_ENTER_PREOP, // (7)
	CO_NMT_CS_START,
	CO_NMT_CS_RESET_NODE, //  (9)
	CO_NMT_CS_RESET_COMM,
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_STOP,
	CO_NMT_CS_RESET_NODE, // (10)
	CO_NMT_CS_RESET_COMM,
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_RESET_NODE, // (11)
	CO_NMT_CS_RESET_COMM,
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_START,
	CO_NMT_CS_RESET_COMM, // (12)
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_STOP,
	CO_NMT_CS_RESET_COMM, // (13)
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_RESET_COMM, // (14)
	CO_NMT_CS_ENTER_PREOP,
};

static enum nmt_master_state state = NMT_MASTER_START;

static uint32_t stateStep = 0;
static uint32_t csStep = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec waitStartTime = { 0, 0 };

// NMT callbacks
static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_MASTER_ID) {
		LOG_INT32_EXPECTED("master id", NMT_MASTER_ID,
				co_dev_get_id(device));
		FAIL_TEST("master: invalid node id in cs_ind()");
	}

	LOG_UINT32("master: received command", cs);

	switch (state) {
	case NMT_MASTER_INIT:
		if (csStep >= NMT_MASTER_INIT_POS)
			FAIL_TEST("master: bad csStep range in INIT state");
		break;
	case NMT_MASTER_TEST:
		if (csStep < NMT_MASTER_INIT_POS)
			FAIL_TEST("master: bad csStep range in TEST state");
		break;
	default: FAIL_TEST("master: invalid test state");
	}

	if (cs != nmt_master_cs_seq[csStep]) {
		LOG_INT32_EXPECTED("cs", nmt_master_cs_seq[csStep], cs);
		FAIL_TEST("master: received invalid cs indication");
	}

	++csStep;

	if (csStep != stateStep)
		FAIL_TEST("master: csStep and stateStep desync");
	if (stateStep == NMT_STATE_MASTER_SEQ_SIZE) {
		state = NMT_MASTER_WAIT;
		LOG("master: entering WAIT state");
	}
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id != NMT_MASTER_ID)
		return;

	LOG_UINT32("master: state changed to", st);

	switch (state) {
	case NMT_MASTER_INIT:
		if (stateStep >= NMT_MASTER_INIT_POS)
			FAIL_TEST("master: bad stateStep range in INIT state");
		break;
	case NMT_MASTER_TEST:
		if (stateStep < NMT_MASTER_INIT_POS)
			FAIL_TEST("master: bad stateStep range in TEST state");
		break;
	default: FAIL_TEST("master: invalid test state");
	}

	if (st != nmt_master_state_seq[stateStep]) {
		LOG_INT32_EXPECTED("st", nmt_master_state_seq[stateStep], st);
		FAIL_TEST("master: received invalid state indication");
	}

	if (csStep != stateStep)
		FAIL_TEST("master: csStep and stateStep desync");

	++stateStep;
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_state_master_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("master: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
	LOG_UINT32("master: received msg with cs   ", msg->data[0]);
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_MASTER_START:
		LOG("master: enter INIT state");
		state = NMT_MASTER_INIT;

		LOG("master: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_MASTER_INIT:
		if ((csStep == NMT_MASTER_INIT_POS)
				&& (stateStep == NMT_MASTER_INIT_POS)
				&& (co_nmt_get_st(nmt) == CO_NMT_ST_PREOP)) {
			state = NMT_MASTER_TEST;
			LOG("master: enter TEST state");
		}
		break;
	case NMT_MASTER_TEST:
		REQUIRE(co_nmt_cs_ind(nmt, nmt_master_cs_seq[csStep]));
		break;
	case NMT_MASTER_WAIT:;
		struct timespec currentTime = { 0, 0 };
		GetCurrentTestTime(&currentTime);

		if ((waitStartTime.tv_sec == 0)
				&& (waitStartTime.tv_nsec == 0)) {
			waitStartTime = currentTime;
			LOG("master: WAIT state started");
		}
		// wait for the slave to finish the test to receive all NMT messages
		else if (timespec_diff_sec(&currentTime, &waitStartTime)
				> NMT_WAIT_INTERVAL_SEC) {
			state = NMT_MASTER_DONE;
			LOG("master: WAIT state finished");
		}
		break;
	case NMT_MASTER_DONE:
		if (CheckMessages(NMT_STATE_MSG_NUMBER, msgCounter,
				    NMT_STATE_MSG_NUMBER, sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
