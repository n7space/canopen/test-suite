/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-heartbeat-slave.h"
#include "nmt-heartbeat.h"

static const co_unsigned8_t nmt_slave_cs_seq[NMT_HB_SLAVE_CS_SEQ_SIZE] = {
	CO_NMT_CS_START,
	CO_NMT_CS_STOP,
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_START,
	CO_NMT_CS_RESET_COMM,
};

static enum nmt_slave_heartbeat state = NMT_SLAVE_START;
static uint32_t seqCounter = 0;
static uint32_t seqMsgCounter = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec testStartTime = { 0, 0 };

// NMT callbacks
static void
hb_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_nmt_ec_state_t state_,
		co_nmt_ec_reason_t reason, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("slave: heartbeat event occured");
	LOG_UINT32("id", id);
	LOG_INT32("state", state_);
	LOG_INT32("reason", reason);

	FAIL_TEST("slave: unexpected heartbeat event");
}

static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_SLAVE_ID)
		FAIL_TEST("slave: incorrect Node-ID in cs_ind()");

	LOG_UINT32("slave: received command", cs);
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id != NMT_SLAVE_ID)
		FAIL_TEST("slave: incorrect Node-ID in st_ind()");
	else
		LOG_UINT32("slave: state changed to", st);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_heartbeat_slave_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("slave: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_hb_ind(nmt, &hb_ind, NULL);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;

	++seqMsgCounter;

	if (sentMsgCounter == NMT_HB_SLAVE_MSG_PER_STATE)
		state = NMT_SLAVE_TEST_STATE;
	else if (sentMsgCounter == NMT_HB_SLAVE_SENT_MSG_NUMBER)
		state = NMT_SLAVE_DONE;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_SLAVE_START:;
		// wait for master to initialize
		struct timespec currentTime = { 0, 0 };
		GetCurrentTestTime(&currentTime);

		if (timespec_diff_sec(&currentTime, &testStartTime)
				< NMT_WAIT_INTERVAL_SEC)
			return;

		LOG("slave: enter TEST state");
		state = NMT_SLAVE_TEST;

		LOG("slave: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_SLAVE_TEST_STATE:
		if ((seqMsgCounter == NMT_HB_SLAVE_MSG_PER_STATE)
				&& (seqCounter < NMT_HB_SLAVE_CS_SEQ_SIZE)) {
			REQUIRE(co_nmt_cs_ind(
					nmt, nmt_slave_cs_seq[seqCounter]));
			++seqCounter;
			seqMsgCounter = 0;
		}
		break;
	case NMT_SLAVE_DONE:
		if (CheckMessages(NMT_HB_MASTER_SENT_MSG_NUMBER, msgCounter,
				    NMT_HB_SLAVE_SENT_MSG_NUMBER,
				    sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
