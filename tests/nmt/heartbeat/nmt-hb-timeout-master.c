/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>

#include "dcf/nmt-hb-timeout-master.h"
#include "nmt-heartbeat.h"

#define NMT_HB_SLAVE_EVENT_SEQ_SIZE 3u
static const co_nmt_ec_reason_t
		nmt_slave_hb_reason_seq[NMT_HB_SLAVE_EVENT_SEQ_SIZE] = {
			CO_NMT_EC_STATE,
			CO_NMT_EC_TIMEOUT,
			CO_NMT_EC_TIMEOUT,
		};

static const co_nmt_ec_state_t
		nmt_slave_hb_state_seq[NMT_HB_SLAVE_EVENT_SEQ_SIZE] = {
			CO_NMT_EC_OCCURRED,
			CO_NMT_EC_OCCURRED,
			CO_NMT_EC_RESOLVED,
		};

static enum nmt_master_heartbeat state = NMT_MASTER_START;
static uint32_t eventSeq = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

// NMT callbacks
static void
hb_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_nmt_ec_state_t state_,
		co_nmt_ec_reason_t reason, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("master: heartbeat event occured");

	if (eventSeq >= NMT_HB_SLAVE_EVENT_SEQ_SIZE) {
		FAIL_TEST("master: too many hearbeat events");
	} else if (id != NMT_SLAVE_ID) {
		LOG_UINT32_EXPECTED("id", NMT_SLAVE_ID, id);
		FAIL_TEST("master: incorrect slave ID in heartbeat event");
	} else if (state_ != nmt_slave_hb_state_seq[eventSeq]) {
		LOG_INT32_EXPECTED("state", nmt_slave_hb_state_seq[eventSeq],
				state_);
		FAIL_TEST("master: incorrect event state");
	} else if (reason != nmt_slave_hb_reason_seq[eventSeq]) {
		LOG_INT32_EXPECTED("reason", nmt_slave_hb_reason_seq[eventSeq],
				reason);
		FAIL_TEST("master: incorrect event reason");
	}

	++eventSeq;
}

static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_MASTER_ID)
		FAIL_TEST("master: incorrect Node-ID in cs_ind()");

	LOG_UINT32("master: received command", cs);
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id == NMT_MASTER_ID) {
		LOG_UINT32("master: state changed to", st);
	} else {
		LOG_UINT32("master: slave changed state to", st);
		if (id != NMT_SLAVE_ID) {
			LOG_UINT32_EXPECTED("slave ID", NMT_SLAVE_ID, id);
			FAIL_TEST("master: incorrect slave ID");
		}
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_hb_timeout_master_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("master: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_hb_ind(nmt, &hb_ind, NULL);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msg->id != (uint_least32_t)CO_NMT_EC_CANID(NMT_SLAVE_ID)) {
		LOG_UINT32_EXPECTED("CAN-ID",
				(uint_least32_t)CO_NMT_EC_CANID(NMT_SLAVE_ID),
				msg->id);
		FAIL_TEST("master: incorrect message received");
	}

	if (msgCounter == NMT_HB_TIMEOUT_SLAVE_SENT_MSG_NUMBER)
		state = NMT_MASTER_DONE;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_MASTER_START:
		LOG("master: enter TEST state");
		state = NMT_MASTER_TEST;

		LOG("master: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_MASTER_DONE:
		if (CheckMessages(NMT_HB_TIMEOUT_SLAVE_SENT_MSG_NUMBER,
				    msgCounter, NMT_HB_MASTER_SENT_MSG_NUMBER,
				    sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
