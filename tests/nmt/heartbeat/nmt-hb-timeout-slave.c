/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/co/ssdo.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-hb-timeout-slave.h"
#include "nmt-heartbeat.h"

static co_ssdo_t *ssdo = NULL;

static enum nmt_slave_heartbeat state = NMT_SLAVE_START;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static uint32_t stateMsgCounter = 0;

static struct timespec waitStartTime = { 0, 0 };

// NMT callbacks
static void
hb_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_nmt_ec_state_t state_,
		co_nmt_ec_reason_t reason, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("slave: heartbeat event occured");
	LOG_UINT32("id", id);
	LOG_INT32("state", state_);
	LOG_INT32("reason", reason);

	FAIL_TEST("slave: unexpected heartbeat event");
}

static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_SLAVE_ID)
		FAIL_TEST("slave: incorrect Node-ID in cs_ind()");

	LOG_UINT32("slave: received command", cs);
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id != NMT_SLAVE_ID)
		FAIL_TEST("slave: incorrect Node-ID in st_ind()");
	else
		LOG_UINT32("slave: state changed to", st);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_hb_timeout_slave_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("slave: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_hb_ind(nmt, &hb_ind, NULL);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);

	ssdo = co_ssdo_create(net, dev, 1u);
	co_ssdo_start(ssdo);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
	++stateMsgCounter;

	if ((state == NMT_SLAVE_TEST)
			&& (stateMsgCounter == NMT_HB_SLAVE_MSG_PER_STATE)) {
		Set1017ProducerHbTime(0);
		GetCurrentTestTime(&waitStartTime);

		LOG("slave: enter TEST_TIMEOUT state");
		state = NMT_SLAVE_TEST_TIMEOUT;
		stateMsgCounter = 0;
	} else if (sentMsgCounter == NMT_HB_TIMEOUT_SLAVE_SENT_MSG_NUMBER) {
		if (state != NMT_SLAVE_TEST_RESOLVE)
			FAIL_TEST("slave: incorrect state at the end of test");
		else if (stateMsgCounter != NMT_HB_SLAVE_MSG_PER_STATE)
			FAIL_TEST("slave: incorrect number of send messages in TEST_RESOLVE state");

		state = NMT_SLAVE_DONE;
	}
}

void
TestStep(void)
{
	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	switch (state) {
	case NMT_SLAVE_START:
		// wait for master to initialize
		if (timespec_diff_sec(&currentTime, &waitStartTime)
				< NMT_WAIT_INTERVAL_SEC)
			break;

		LOG("slave: enter TEST state");
		state = NMT_SLAVE_TEST;

		LOG("slave: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_SLAVE_TEST_TIMEOUT:
		if (timespec_diff_sec(&currentTime, &waitStartTime)
				>= NMT_HB_WAIT_INTERVAL_SEC) {
			Set1017ProducerHbTime(NMT_HB_TIME_MS);

			LOG("slave: enter TEST_RESOLVE state");
			state = NMT_SLAVE_TEST_RESOLVE;
		}
		break;
	case NMT_SLAVE_DONE:
		if (CheckMessages(NMT_HB_MASTER_SENT_MSG_NUMBER, msgCounter,
				    NMT_HB_TIMEOUT_SLAVE_SENT_MSG_NUMBER,
				    sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
