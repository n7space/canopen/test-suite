/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_NMT_STATE_H__
#define CTSSW_NMT_STATE_H__

#include "nmt.h"

enum nmt_master_state {
	NMT_MASTER_START,
	NMT_MASTER_INIT,
	NMT_MASTER_TEST,
	NMT_MASTER_WAIT,
	NMT_MASTER_DONE,
};

enum nmt_slave_state {
	NMT_SLAVE_START,
	NMT_SLAVE_INIT,
	NMT_SLAVE_RESET_BY_MASTER,
	NMT_SLAVE_TEST,
	NMT_SLAVE_DONE,
};

#define NMT_MASTER_INIT_POS 3u
#define NMT_SLAVE_INIT_POS 3u
#define NMT_SLAVE_RESET_BY_MASTER_POS 5u

/* NMT state tests*/
#define NMT_STATE_MASTER_SEQ_SIZE 28u
#define NMT_STATE_SLAVE_SEQ_SIZE 30u

static const uint32_t NMT_STATE_MSG_NUMBER = 16u;
static const uint32_t NMT_SLAVE_RESET_BY_MASTER_COUNT = 9u;

/* NMT request tests*/
#define NMT_REQ_MASTER_SEQ_SIZE 4u
#define NMT_REQ_MASTER_REQ_SEQ_SIZE 16u
#define NMT_REQ_SLAVE_SEQ_SIZE 30u

static const uint32_t NMT_REQ_MASTER_SENT_MSG_NUMBER =
		NMT_REQ_MASTER_REQ_SEQ_SIZE
		+ 2u; // boot-up and boot slave messages
static const uint32_t NMT_REQ_SLAVE_SENT_MSG_NUMBER = 8u;

/* NMT reset device tests*/
static const uint32_t NMT_RESET_MASTER_SENT_MSG_NUMBER = 3u;
static const uint32_t NMT_RESET_SLAVE_SENT_MSG_NUMBER = 3u;

#endif // CTSSW_NMT_STATE_H__
