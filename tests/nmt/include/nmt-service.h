/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_NMT_SERVICE_H__
#define CTSSW_NMT_SERVICE_H__

#include <stdbool.h>

#include "nmt.h"

#include <lely/co/csdo.h>
#include <lely/co/emcy.h>
#include <lely/co/nmt.h>
#include <lely/co/rpdo.h>
#include <lely/co/ssdo.h>
#include <lely/co/sync.h>
#include <lely/co/tpdo.h>

enum nmt_master_service {
	NMT_MASTER_START,
	NMT_MASTER_TEST,
	NMT_MASTER_TEST_SRV,
	NMT_MASTER_DONE,
};

enum nmt_slave_service {
	NMT_SLAVE_START,
	NMT_SLAVE_TEST,
	NMT_SLAVE_TEST_SRV,
	NMT_SLAVE_DONE,
};

/* NMT service manager tests*/

#define CO_NMT_SRV_RPDO 0x01
#define CO_NMT_SRV_TPDO 0x02
#define CO_NMT_SRV_CSDO 0x04
#define CO_NMT_SRV_SSDO 0x08
#define CO_NMT_SRV_SYNC 0x10
#define CO_NMT_SRV_EMCY 0x20

#define CO_NMT_PREOP_SRV \
	(CO_NMT_SRV_CSDO | CO_NMT_SRV_SSDO | CO_NMT_SRV_SYNC | CO_NMT_SRV_EMCY)

#define CO_NMT_OPER_SRV (CO_NMT_PREOP_SRV | CO_NMT_SRV_RPDO | CO_NMT_SRV_TPDO)

#define CO_NMT_STOP_SRV 0

static const co_unsigned16_t RPDO_NUM = 0x0001u;
static const co_unsigned16_t TPDO_NUM = 0x0001u;
static const co_unsigned8_t SSDO_NUM = 0x01u;
static const co_unsigned8_t CSDO_NUM = 0x01u;

static inline bool
CheckRpdoService(const co_nmt_t *const nmt_, const uint_least8_t services)
{
	const co_rpdo_t *rpdo = co_nmt_get_rpdo(nmt_, RPDO_NUM);

	if (rpdo == NULL) {
		LOG("CheckService(): RPDO is NULL");
		return false;
	}

	const bool expStopped = ((services & CO_NMT_SRV_RPDO) == 0);
	const bool retStopped = (co_rpdo_is_stopped(rpdo) != 0);

	if (expStopped != retStopped) {
		LOG_UINT32_EXPECTED("RPDO stopped", expStopped ? 1u : 0u,
				retStopped ? 1u : 0u);
		LOG("CheckService(): incorrect state of RPDO service");
		return false;
	}

	return true;
}

static inline bool
CheckTpdoService(const co_nmt_t *const nmt_, const uint_least8_t services)
{
	const co_tpdo_t *tpdo = co_nmt_get_tpdo(nmt_, TPDO_NUM);

	if (tpdo == NULL) {
		LOG("CheckService(): TPDO is NULL");
		return false;
	}

	const bool expStopped = ((services & CO_NMT_SRV_TPDO) == 0);
	const bool retStopped = (co_tpdo_is_stopped(tpdo) != 0);

	if (expStopped != retStopped) {
		LOG_UINT32_EXPECTED("TPDO stopped", expStopped ? 1u : 0u,
				retStopped ? 1u : 0u);
		LOG("CheckService(): incorrect state of TPDO service");
		return false;
	}

	return true;
}

static inline bool
CheckCsdoService(const co_nmt_t *const nmt_, const uint_least8_t services)
{
	const co_csdo_t *csdo = co_nmt_get_csdo(nmt_, CSDO_NUM);

	if (csdo == NULL) {
		LOG("CheckService(): CSDO is NULL");
		return false;
	}

	const bool expStopped = ((services & CO_NMT_SRV_CSDO) == 0);
	const bool retStopped = (co_csdo_is_stopped(csdo) != 0);

	if (expStopped != retStopped) {
		LOG_UINT32_EXPECTED("CSDO stopped", expStopped ? 1u : 0u,
				retStopped ? 1u : 0u);
		LOG("CheckService(): incorrect state of CSDO service");
		return false;
	}

	return true;
}

static inline bool
CheckSsdoService(const co_nmt_t *const nmt_, const uint_least8_t services)
{
	const co_ssdo_t *ssdo = co_nmt_get_ssdo(nmt_, SSDO_NUM);

	if (ssdo == NULL) {
		LOG("CheckService(): SSDO is NULL");
		return false;
	}

	const bool expStopped = ((services & CO_NMT_SRV_SSDO) == 0);
	const bool retStopped = (co_ssdo_is_stopped(ssdo) != 0);

	if (expStopped != retStopped) {
		LOG_UINT32_EXPECTED("SSDO stopped", expStopped ? 1u : 0u,
				retStopped ? 1u : 0u);
		LOG("CheckService(): incorrect state of SSDO service");
		return false;
	}

	return true;
}

static inline bool
CheckSyncService(const co_nmt_t *const nmt_, const uint_least8_t services)
{
	const co_sync_t *sync = co_nmt_get_sync(nmt_);

	if (sync == NULL) {
		LOG("CheckService(): SYNC is NULL");
		return false;
	}

	const bool expStopped = ((services & CO_NMT_SRV_SYNC) == 0);
	const bool retStopped = (co_sync_is_stopped(sync) != 0);

	if (expStopped != retStopped) {
		LOG_UINT32_EXPECTED("SYNC stopped", expStopped ? 1u : 0u,
				retStopped ? 1u : 0u);
		LOG("CheckService(): incorrect state of SYNC service");
		return false;
	}

	return true;
}

static inline bool
CheckEmcyService(const co_nmt_t *const nmt_, const uint_least8_t services)
{
	const co_emcy_t *emcy = co_nmt_get_emcy(nmt_);

	if (emcy == NULL) {
		LOG("CheckService(): EMCY is NULL");
		return false;
	}

	const bool expStopped = ((services & CO_NMT_SRV_EMCY) == 0);
	const bool retStopped = (co_emcy_is_stopped(emcy) != 0);

	if (expStopped != retStopped) {
		LOG_UINT32_EXPECTED("EMCY stopped", expStopped ? 1u : 0u,
				retStopped ? 1u : 0u);
		LOG("CheckService(): incorrect state of EMCY service");
		return false;
	}

	return true;
}

static inline bool
CheckServices(const co_nmt_t *const nmt_, const uint_least8_t services)
{
	return CheckRpdoService(nmt_, services)
			&& CheckTpdoService(nmt_, services)
			&& CheckCsdoService(nmt_, services)
			&& CheckSsdoService(nmt_, services)
			&& CheckSyncService(nmt_, services)
			&& CheckEmcyService(nmt_, services);
}

#endif // CTSSW_NMT_SERVICE_H__
