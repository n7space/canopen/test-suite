/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_NMT_BOOTUP_H__
#define CTSSW_NMT_BOOTUP_H__

#include "nmt.h"

enum nmt_master_bootup {
	NMT_MASTER_START,
	NMT_MASTER_TEST,
	NMT_MASTER_DONE,
};

enum nmt_slave_bootup {
	NMT_SLAVE_START,
	NMT_SLAVE_TEST,
	NMT_SLAVE_DONE,
};

/* NMT boot-up tests*/
static const uint32_t NMT_BOOTUP_MASTER_SENT_MSG_NUMBER = 2u;
static const uint32_t NMT_BOOTUP_SLAVE_SENT_MSG_NUMBER = 1u;

#endif // CTSSW_NMT_BOOTUP_H__
