/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_NMT_STARTUP_H__
#define CTSSW_NMT_STARTUP_H__

#include "nmt.h"

#include <lely/co/csdo.h>
#include <lely/co/dev.h>
#include <lely/co/sync.h>

enum nmt_master_startup {
	NMT_MASTER_START,
	NMT_MASTER_TEST,
	NMT_MASTER_SYNC_ENABLED,
	NMT_MASTER_DONE,
};

enum nmt_slave_startup {
	NMT_SLAVE_START,
	NMT_SLAVE_TEST,
	NMT_SLAVE_WAIT_FOR_SYNC,
	NMT_SLAVE_DONE,
};

#define CO_SYNC_CAN_ID 0x0080u

/* NMT autostart tests */
static const uint32_t NMT_AUTOSTART_MASTER_SENT_MSG_NUMBER = 2u;
static const uint32_t NMT_AUTOSTART_SLAVE_SENT_MSG_NUMBER = 1u;

/* NMT startup tests */
#define NMT_STARTUP_MASTER_SENT_MSG_NUMBER 4u
static const uint32_t NMT_STARTUP_SYNC_MSG_NUMBER = 1u;
static const uint32_t NMT_STARTUP_SLAVE_SENT_MSG_NUMBER = 1u;

static inline bool
CanMsgCmp(const struct can_msg *const expMsg, const struct can_msg *const msg)
{
	if (expMsg->id != msg->id) {
		LOG_UINT32_EXPECTED("msg->id", expMsg->id, msg->id);
		return false;
	} else if (expMsg->flags != msg->flags) {
		LOG_UINT32_EXPECTED("msg->flags", expMsg->flags, msg->flags);
		return false;
	} else if (expMsg->len != msg->len) {
		LOG_UINT32_EXPECTED("msg->len", expMsg->len, msg->len);
		return false;
	} else {
		for (size_t i = 0; i < expMsg->len; ++i) {
			if (expMsg->data[i] != msg->data[i]) {
				LOG_UINT32_EXPECTED("msg->data",
						expMsg->data[i], msg->data[i]);
				return false;
			}
		}
	}

	return true;
}

static inline void
co_csdo_dn_1005_con(co_csdo_t *sdo, co_unsigned16_t idx, co_unsigned8_t subidx,
		co_unsigned32_t ac, void *data)
{
	(void)sdo;
	(void)data;

	if ((idx != 0x1005) || (subidx != 0x00)) {
		FAIL_TEST("master: SYNC COB-ID download confirmation for incorrect object");
	} else if (ac != 0) {
		LOG_STR("ac", co_sdo_ac2str(ac));
		FAIL_TEST("master: failed to download a SYNC COB-ID value");
	}
}

static inline bool
CoSyncProducerSetEnable(bool enable)
{
	co_unsigned32_t syncCobId = co_dev_get_val_u32(dev, 0x1005, 0x00);

	if (enable)
		syncCobId |= CO_SYNC_COBID_PRODUCER;
	else
		syncCobId &= ~CO_SYNC_COBID_PRODUCER;

	if (co_dev_dn_val_req(dev, 0x1005, 0x00, CO_DEFTYPE_UNSIGNED32,
			    &syncCobId, NULL, &co_csdo_dn_1005_con, NULL)
			!= 0) {
		FAIL_TEST("master: failed to submit download request for SYNC COB-ID");
		return false;
	}

	return true;
}

#endif // CTSSW_NMT_STARTUP_H__
