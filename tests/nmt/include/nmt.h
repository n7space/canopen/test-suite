/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_NMT_H__
#define CTSSW_NMT_H__

#include <stdbool.h>

#include <lely/co/type.h>

#include <TestFramework/TestHarness.h>

static co_dev_t *dev = NULL;
static co_nmt_t *nmt = NULL;

static const co_unsigned8_t NMT_MASTER_ID = 0x01;
static const co_unsigned8_t NMT_SLAVE_ID = 0x02;

static const uint32_t NMT_WAIT_INTERVAL_SEC = 1u;

static inline bool
CheckMessages(const uint32_t expectedRecv, const uint32_t recvMessages,
		const uint32_t expectedSent, const uint32_t sentMessages)
{
	if (recvMessages != expectedRecv) {
		LOG_UINT32_EXPECTED("received messages", expectedRecv,
				recvMessages);
		FAIL_TEST("error: incorrect received messages count");
		return false;
	} else if (sentMessages != expectedSent) {
		LOG_UINT32_EXPECTED(
				"sent messages", expectedSent, sentMessages);
		FAIL_TEST("error: incorrect sent messages count");
		return false;
	}

	LOG_UINT32("received messages", recvMessages);
	LOG_UINT32("sent messages", sentMessages);
	return true;
}

#endif // CTSSW_NMT_H__
