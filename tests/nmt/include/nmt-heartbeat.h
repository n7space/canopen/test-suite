/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_NMT_HEARTBEAT_H__
#define CTSSW_NMT_HEARTBEAT_H__

#include "nmt.h"

#include <lely/co/csdo.h>

enum nmt_master_heartbeat {
	NMT_MASTER_START,
	NMT_MASTER_TEST,
	NMT_MASTER_TEST_STATE,
	NMT_MASTER_CS,
	NMT_MASTER_DONE,
};

enum nmt_slave_heartbeat {
	NMT_SLAVE_START,
	NMT_SLAVE_TEST,
	NMT_SLAVE_TEST_STATE,
	NMT_SLAVE_TEST_TIMEOUT,
	NMT_SLAVE_TEST_RESOLVE,
	NMT_SLAVE_DONE,
};

/* NMT heartbeat tests*/

#define NMT_HB_MASTER_CS_SEQ_SIZE 3u
#define NMT_HB_SLAVE_CS_SEQ_SIZE 5u

static const uint32_t NMT_HB_MASTER_SENT_MSG_NUMBER = 2u;
static const uint32_t NMT_HB_SLAVE_MSG_PER_STATE = 5u;

static const uint32_t NMT_HB_SLAVE_SENT_MSG_NUMBER =
		NMT_HB_SLAVE_MSG_PER_STATE // startup
		+ (NMT_HB_SLAVE_MSG_PER_STATE * NMT_HB_SLAVE_CS_SEQ_SIZE);

static const uint32_t NMT_HB_CONSUMER_SLAVE_SENT_MSG_NUMBER =
		NMT_HB_SLAVE_MSG_PER_STATE // startup
		+ ((1u + NMT_HB_MASTER_CS_SEQ_SIZE)
				* NMT_HB_SLAVE_MSG_PER_STATE);

static const uint32_t NMT_HB_TIMEOUT_SLAVE_SENT_MSG_NUMBER =
		2u * NMT_HB_SLAVE_MSG_PER_STATE;

static const co_unsigned16_t NMT_HB_TIME_MS = 500u;
static const uint32_t NMT_HB_WAIT_INTERVAL_SEC = 5u;

static inline void
Set1017ProducerHbTime(co_unsigned16_t hb_time)
{
	const int ret = co_dev_dn_req(dev, 0x1017u, 0x00u, &hb_time,
			sizeof(hb_time), NULL, NULL);
	if (ret != 0)
		FAIL_TEST("slave: failed to submit SDO request");
}

#endif // CTSSW_NMT_HEARTBEAT_H__
