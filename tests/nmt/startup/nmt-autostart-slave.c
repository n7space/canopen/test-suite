/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/dev.h>
#include <lely/co/nmt.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "dcf/nmt-autostart-slave.h"
#include "nmt-startup.h"

#define NMT_AUTOSTART_SLAVE_SEQ_SIZE 4u
static const co_unsigned8_t nmt_slave_st_seq[NMT_AUTOSTART_SLAVE_SEQ_SIZE] = {
	CO_NMT_ST_BOOTUP,
	CO_NMT_ST_BOOTUP,
	CO_NMT_ST_PREOP,
	CO_NMT_ST_START,
};

static const co_unsigned8_t nmt_slave_cs_seq[NMT_AUTOSTART_SLAVE_SEQ_SIZE] = {
	CO_NMT_CS_RESET_NODE,
	CO_NMT_CS_RESET_COMM,
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_START,
};

static enum nmt_slave_startup state = NMT_SLAVE_START;
static uint32_t seqCounter = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec testStartTime = { 0, 0 };

// NMT callbacks
static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const device = data;

	if (co_dev_get_id(device) != NMT_SLAVE_ID) {
		LOG_INT32_EXPECTED("slave id", NMT_SLAVE_ID,
				co_dev_get_id(device));
		FAIL_TEST("slave: invalid Node-Id in cs_ind()");
	} else {
		LOG_UINT32("slave: received command", cs);

		if (cs != nmt_slave_cs_seq[seqCounter]) {
			LOG_UINT32_EXPECTED(
					"cs", nmt_slave_cs_seq[seqCounter], cs);
			FAIL_TEST("slave: incorrect command specifier");
		}

		++seqCounter;

		if (seqCounter == NMT_AUTOSTART_SLAVE_SEQ_SIZE)
			state = NMT_SLAVE_DONE;
	}
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id != NMT_SLAVE_ID) {
		LOG_INT32_EXPECTED("slave id", NMT_SLAVE_ID, id);
		FAIL_TEST("slave: invalid node id in st_ind()");
	} else {
		LOG_UINT32("slave: state changed to", st);

		if (st != nmt_slave_st_seq[seqCounter]) {
			LOG_UINT32_EXPECTED(
					"st", nmt_slave_st_seq[seqCounter], st);
			FAIL_TEST("slave: incorrect command specifier");
		}
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_autostart_slave_init();

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("slave: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	switch (state) {
	case NMT_SLAVE_START:;
		// wait for master to finish test
		struct timespec currentTime = { 0, 0 };
		GetCurrentTestTime(&currentTime);

		if (timespec_diff_sec(&currentTime, &testStartTime)
				< NMT_WAIT_INTERVAL_SEC)
			break;

		LOG("slave: enter TEST state");
		state = NMT_SLAVE_TEST;

		LOG("slave: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		break;
	case NMT_SLAVE_DONE:
		if (CheckMessages(NMT_AUTOSTART_MASTER_SENT_MSG_NUMBER,
				    msgCounter,
				    NMT_AUTOSTART_SLAVE_SENT_MSG_NUMBER,
				    sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
