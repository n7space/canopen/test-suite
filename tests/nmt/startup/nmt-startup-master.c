/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/nmt.h>
#include <lely/util/error.h>

#include "dcf/nmt-startup-master.h"
#include "nmt-startup.h"

static co_sync_t *sync = NULL;

static const struct can_msg
		nmt_master_sent_msg_seq[NMT_STARTUP_MASTER_SENT_MSG_NUMBER] = {
			// boot-up/1st heartbeat
			{ CO_NMT_EC_CANID(NMT_MASTER_ID), 0u, 1u,
					{ CO_NMT_ST_BOOTUP, 0u } },
			// NMT 'reset communication' command to all slaves
			{ 0u, 0u, 2u, { CO_NMT_CS_RESET_COMM, 0u } },
			// 2nd heartbeat
			{ CO_NMT_EC_CANID(NMT_MASTER_ID), 0u, 1u,
					{ CO_NMT_ST_PREOP, 0u } },
			// 1st SYNC
			{ CO_SYNC_CAN_ID, 0u, 0u, { 0u } },
		};

#define NMT_STARTUP_MASTER_SEQ_SIZE 4u
static const co_unsigned8_t nmt_master_st_seq[NMT_STARTUP_MASTER_SEQ_SIZE] = {
	CO_NMT_ST_BOOTUP,
	CO_NMT_ST_BOOTUP,
	CO_NMT_ST_PREOP,
	CO_NMT_ST_START,
};

static const co_unsigned8_t nmt_master_cs_seq[NMT_STARTUP_MASTER_SEQ_SIZE] = {
	CO_NMT_CS_RESET_NODE,
	CO_NMT_CS_RESET_COMM,
	CO_NMT_CS_ENTER_PREOP,
	CO_NMT_CS_START,
};

static enum nmt_master_startup state = NMT_MASTER_START;
static uint32_t seqCounter = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static uint32_t syncCounter = 0;

// NMT callbacks
static void
hb_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_nmt_ec_state_t state_,
		co_nmt_ec_reason_t reason, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	LOG("master: heartbeat event occurred");
	LOG_UINT32("id", id);
	LOG_INT32("state", state_);
	LOG_INT32("reason", reason);

	FAIL_TEST("master: unexpected heartbeat event");
}

static void
cs_ind(co_nmt_t *nmt_, co_unsigned8_t cs, void *data)
{
	assert(nmt_ == nmt);

	const co_dev_t *const devices = data;

	if (co_dev_get_id(devices) != NMT_MASTER_ID) {
		LOG_INT32_EXPECTED("master id", NMT_MASTER_ID,
				co_dev_get_id(devices));
		FAIL_TEST("master: invalid node id in cs_ind()");
	} else {
		LOG_UINT32("master: received command", cs);

		if (seqCounter >= NMT_STARTUP_MASTER_SEQ_SIZE) {
			FAIL_TEST("master: too many commands received");
		} else if (cs != nmt_master_cs_seq[seqCounter]) {
			LOG_UINT32_EXPECTED("cs", nmt_master_cs_seq[seqCounter],
					cs);
			FAIL_TEST("master: incorrect command specifier");
		}

		++seqCounter;
	}
}

static void
st_ind(co_nmt_t *nmt_, co_unsigned8_t id, co_unsigned8_t st, void *data)
{
	assert(nmt_ == nmt);

	(void)data;

	if (id == NMT_MASTER_ID) {
		LOG_UINT32("master: state changed to", st);

		if (seqCounter >= NMT_STARTUP_MASTER_SEQ_SIZE) {
			FAIL_TEST("master: too many state changes");
		} else if (st != nmt_master_st_seq[seqCounter]) {
			LOG_UINT32_EXPECTED("st", nmt_master_st_seq[seqCounter],
					st);
			FAIL_TEST("master: incorrect command specifier");
		}
	} else {
		LOG_UINT32("master: slave changed state to", st);
	}
}

// SYNC callbacks
static void
sync_ind_func(co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync_ == sync);

	(void)cnt;
	(void)data;

	LOG("master: SYNC message sent");
	++syncCounter;
}

static void
sync_err_func(co_sync_t *sync_, co_unsigned16_t eec, co_unsigned8_t er,
		void *data)
{
	assert(sync == sync_);

	(void)data;

	LOG_UINT32("eec", eec);
	LOG_UINT32("er", er);
	FAIL_TEST("master: SYNC error occurred");
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_nmt_startup_master_init();

	if (!CoSyncProducerSetEnable(false))
		FAIL_TEST("master: failed to disable SYNC");

	nmt = co_nmt_create(net, dev);
	if (nmt == NULL) {
		FAIL_TEST("master: NMT service start failed");
		LOG_INT32("errc", get_errc());
	}

	co_nmt_set_cs_ind(nmt, &cs_ind, dev);
	co_nmt_set_hb_ind(nmt, &hb_ind, NULL);
	co_nmt_set_st_ind(nmt, &st_ind, NULL);

	sync = co_nmt_get_sync(nmt);
	if (sync == NULL)
		FAIL_TEST("master: NMT service manager failed to create SYNC service");

	co_sync_set_ind(sync, sync_ind_func, NULL);
	co_sync_set_err(sync, sync_err_func, NULL);
}

void
TestTeardown(void)
{
	co_nmt_destroy(nmt);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	if (sentMsgCounter >= NMT_STARTUP_MASTER_SENT_MSG_NUMBER) {
		FAIL_TEST("master: too many messages sent");
	} else if (!CanMsgCmp(&nmt_master_sent_msg_seq[sentMsgCounter], msg)) {
		LOG_UINT32("sentMsgCounter", sentMsgCounter);
		FAIL_TEST("master: incorrect message sent");
	}

	++sentMsgCounter;

	if ((seqCounter == NMT_STARTUP_MASTER_SEQ_SIZE)
			&& (sentMsgCounter
					== NMT_STARTUP_MASTER_SENT_MSG_NUMBER)) {
		state = NMT_MASTER_DONE;
	}
}

void
TestStep(void)
{
	switch (state) {
	case NMT_MASTER_START:
		LOG("master: enter TEST state");
		state = NMT_MASTER_TEST;

		LOG("master: initial resetting of the node");
		REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_RESET_NODE));
		/* 1. Start transmission of HeartBeat */
		break;
	case NMT_MASTER_TEST:
		/* 2. Start bus monitoring protocol, wait for boot of all devices */
		if (co_nmt_chk_bootup(nmt, 0) != 0) {
			/* 3. Configuration of all device parameters */
			LOG("master: all slaves booted, proceeding to START state");
			REQUIRE(co_nmt_cs_ind(nmt, CO_NMT_CS_START));

			/* 4. Start transmission of SYNC */
			LOG("master: start SYNC transmission");
			if (!CoSyncProducerSetEnable(true))
				FAIL_TEST("master: failed to enable SYNC");

			state = NMT_MASTER_SYNC_ENABLED;
		}
		break;
	case NMT_MASTER_DONE:
		if (syncCounter != NMT_STARTUP_SYNC_MSG_NUMBER) {
			LOG_UINT32_EXPECTED("syncCounter",
					NMT_STARTUP_SYNC_MSG_NUMBER,
					syncCounter);
			FAIL_TEST("master: incorrect number of SYNC messages sent");
		} else if (CheckMessages(NMT_STARTUP_SLAVE_SENT_MSG_NUMBER,
					   msgCounter,
					   NMT_STARTUP_MASTER_SENT_MSG_NUMBER,
					   sentMsgCounter)) {
			FINISH_TEST();
		}
		break;
	default: break;
	}
}
