/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/dev.h>
#include <lely/co/sync.h>
#include <lely/util/time.h>

#include "dcf/sync-counter-overflow-0-consumer.h"
#include "sync.h"

static uint32_t msgCounter = 0;

// SYNC callbacks
static void
sync_ind_func_counter_overflow_0_cons(
		co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync == sync_);

	sync_ind_func(sync, cnt, data);

	if (cnt != 0)
		FAIL_TEST("consumer: SYNC counter should be 0");
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sync_counter_overflow_0_consumer_init();

	sync = co_sync_create(net, dev);
	co_sync_set_ind(sync, sync_ind_func_counter_overflow_0_cons, NULL);
	co_sync_set_err(sync, sync_err_func, NULL);

	co_sync_start(sync);
}

void
TestTeardown(void)
{
	co_sync_stop(sync);
	co_sync_destroy(sync);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == SYNC_COUNTER_OVERFLOW_NUMBER) {
		if (syncCounter != msgCounter) {
			LOG_UINT32_EXPECTED("SYNC vs all received messages",
					syncCounter, msgCounter);
			FAIL_TEST("consumer: received non-SYNC messages");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("consumer: sent unexpected message");
}
