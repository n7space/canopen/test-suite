/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/dev.h>
#include <lely/co/sync.h>
#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/sync-cycle-period-producer.h"
#include "sync.h"

static uint32_t sentMsgCounter = 0;

static struct timespec lastSyncTime = { 0, 0 };

// SYNC callbacks
static void
sync_ind_func_period_cycle_prod(
		co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync == sync_);

	sync_ind_func(sync, cnt, data);

	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	if (syncCounter > 1u) {
		const int32_t delta = (int32_t)timespec_diff_msec(
				&currentTime, &lastSyncTime);

		if (!CheckTimeInterval(delta, SYNC_CYCLE_PERIOD)) {
			LOG_INT32_EXPECTED("cycle period", SYNC_CYCLE_PERIOD,
					delta);
			FAIL_TEST("producer: incorrect cycle period");
		}
	}

	if (syncCounter == SYNC_CYCLE_PERIOD_NUMBER) {
		if (sentMsgCounter != SYNC_CYCLE_PERIOD_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					SYNC_CYCLE_PERIOD_NUMBER,
					sentMsgCounter);
			FAIL_TEST("producer: incorrect sent messages count");
		} else {
			FINISH_TEST();
		}
	} else {
		lastSyncTime = currentTime;
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sync_cycle_period_producer_init();

	sync = co_sync_create(net, dev);
	co_sync_set_ind(sync, sync_ind_func_period_cycle_prod, NULL);
	co_sync_set_err(sync, sync_err_func, NULL);

	co_sync_start(sync);
}

void
TestTeardown(void)
{
	co_sync_stop(sync);
	co_sync_destroy(sync);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	FAIL_TEST("producer: received unexpected message");
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
