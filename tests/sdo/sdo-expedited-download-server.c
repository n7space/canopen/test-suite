/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/obj.h>

#include "dcf/sdo-expedited-download-server.h"
#include "sdo-server.h"

static bool indReceived = false;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static co_sub_t *sub2000_0 = NULL;

static co_unsigned32_t
co_sub2000_0_dn_ind_exp(co_sub_t *sub, struct co_sdo_req *req,
		co_unsigned32_t ac, void *data)
{
	if (sub != sub2000_0)
		FAIL_TEST("server: received download indication for other object");

	if (ac != 0) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub2000_0_dn_ind_exp received abort code");
		return ac;
	}

	indReceived = true;

	return co_sub_dn_ind_func(sub, req, data);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sdo_expedited_download_server_init();

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_ssdo_start(ssdo);

	REQUIRE_SIZE(co_dev_set_val_i16(dev, IDX, SUBIDX, 0),
			sizeof(co_integer16_t));

	sub2000_0 = co_dev_find_sub(dev, IDX, SUBIDX);
	co_sub_set_dn_ind(sub2000_0, &co_sub2000_0_dn_ind_exp, NULL);
}

void
TestTeardown(void)
{
	co_ssdo_stop(ssdo);
	co_ssdo_destroy(ssdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (indReceived) {
		const co_integer16_t readValueI16 =
				co_dev_get_val_i16(dev, IDX, SUBIDX);

		if (readValueI16 != SDO_EXP_VAL) {
			LOG_INT32_EXPECTED("value", SDO_EXP_VAL, readValueI16);
			FAIL_TEST("server: OD object 0x2000 does not contain expected value");
		} else if (msgCounter != 1) {
			FAIL_TEST("server: received more than one message");
		} else if (subDnInd != 1) {
			FAIL_TEST("server: more than one download indication");
		} else
			FINISH_TEST();
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;

	if (sentMsgCounter > 1u)
		FAIL_TEST("server: too many messages sent");
}
