/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>
#include <stdint.h>

#include <lely/co/obj.h>
#include <lely/co/val.h>

#include "dcf/0x0A-octet-string-download-server.h"
#include "sdo-datatypes.h"
#include "sdo-server.h"

enum sdo_dt_dn_test_state {
	SDO_DN_INIT,
	SDO_DN_SEG,
	SDO_DN_DONE,
};

static enum sdo_dt_dn_test_state testState = SDO_DN_INIT;

static const uint32_t SDO_VAL_SIZE = ARRAY_VAL_LEN * TEST_VAL_OS_ELEM_SIZE;
static const uint32_t SDO_SRV_IND_COUNT =
		((SDO_VAL_SIZE + SDO_SEGMENT_SIZE) - 1u) / SDO_SEGMENT_SIZE;
static const uint32_t SDO_CLI_MSG_COUNT =
		SDO_SRV_IND_COUNT + 1u; // initiate message
static const uint32_t SDO_SRV_MSG_COUNT = SDO_CLI_MSG_COUNT;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static uint32_t subDnIndArray = 0;

static co_sub_t *sub2000_0 = NULL;
static const uint8_t *buf = NULL;

inline static co_unsigned32_t
co_sub2000_0_dn_ind_dt(co_sub_t *sub, struct co_sdo_req *req,
		co_unsigned32_t ac, void *data)
{
	(void)data;

	++subDnIndArray;

	if (sub != sub2000_0)
		FAIL_TEST("server: received download request for other object");

	if (ac != 0) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub2000_0_dn_ind_dt received abort code");
		return ac;
	}

	switch (testState) {
	case SDO_DN_INIT:
		testState = SDO_DN_SEG;
		buf = (uint8_t *)TEST_VAL_OS;
		// falls through
	case SDO_DN_SEG:
		if (0 != memcmp(req->buf, buf, req->nbyte))
			FAIL_TEST("server: SDO sub-block segment data does not match");
		buf += req->nbyte;

		if ((req->offset + req->nbyte) == req->size) {
			// this should set downloaded value, but the test
			// already verified data is correct, so just set it from
			// TEST_VAL variable
			(void)co_dev_set_val(dev, IDX, SUBIDX, TEST_VAL_OS,
					req->size);
			testState = SDO_DN_DONE;
		}
		break;
	default: FAIL_TEST("server: invalid test state");
	}

	return 0;
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_0x0A_octet_string_download_server_init();

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_ssdo_start(ssdo);

	sub2000_0 = co_dev_find_sub(dev, IDX, SUBIDX);
	co_sub_set_dn_ind(sub2000_0, &co_sub2000_0_dn_ind_dt, NULL);
}

void
TestTeardown(void)
{
	co_ssdo_stop(ssdo);
	co_ssdo_destroy(ssdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == SDO_SRV_MSG_COUNT) {
		const co_octet_string_t *const readValueOS =
				co_dev_get_val(dev, IDX, SUBIDX);

		if (co_val_cmp(CO_DEFTYPE_OCTET_STRING, *readValueOS,
				    TEST_VAL_OS)
				!= 0) {
			LOG_UINT32("object", IDX);
			LOG_ARRAY("expected value", TEST_VAL_OS, SDO_VAL_SIZE);
			LOG_ARRAY("read value", *readValueOS, SDO_VAL_SIZE);
			FAIL_TEST("server: OD object does not contain expected value");
		} else if (subDnIndArray != SDO_SRV_IND_COUNT) {
			LOG_UINT32_EXPECTED("indications", SDO_SRV_IND_COUNT,
					subDnIndArray);
			FAIL_TEST("server: incorrect indications count");
		} else if (sentMsgCounter != SDO_CLI_MSG_COUNT) {
			LOG_UINT32_EXPECTED("sent messages", SDO_CLI_MSG_COUNT,
					sentMsgCounter);
			FAIL_TEST("client: incorrect sent messages count");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
