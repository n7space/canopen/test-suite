/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>
#include <stdint.h>

#include <lely/util/error.h>

#include "dcf/0x10-integer24-download-client.h"
#include "sdo-client.h"
#include "sdo-datatypes.h"

static uint32_t SDO_VAL_SIZE = 0;

static uint32_t SDO_CLI_MSG_COUNT = 0;
static uint32_t SDO_SRV_MSG_COUNT = 0;
static uint32_t SDO_CLI_IND_COUNT = 0;
static uint32_t SDO_CLI_IND_BYTES = 0;

static bool reqSent = false;

static const co_integer24_t val = TEST_VAL_I24;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static void
co_csdo_dn_con_func_dt(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, void *data)
{
	co_csdo_dn_con_func(sdo, idx, subidx, ac, data);

	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoCon("client: SDO download", idx, subidx);
}

static void
co_csdo_ind_dt_dn(const co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, size_t size, size_t nbyte, void *data)
{
	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoInd("client: SDO download", idx, subidx);

	co_csdo_ind_func(sdo, idx, subidx, size, nbyte, data);
}

void
TestSetup(can_net_t *const net)
{
	SDO_VAL_SIZE = CoValGetReadWriteSize(CO_DEFTYPE_INTEGER24);

	SDO_CLI_MSG_COUNT = CoValGetMsgCount(CO_DEFTYPE_INTEGER24);
	SDO_SRV_MSG_COUNT = CoValGetMsgCount(CO_DEFTYPE_INTEGER24);
	SDO_CLI_IND_COUNT = CoValGetClientIndCount(CO_DEFTYPE_INTEGER24);
	SDO_CLI_IND_BYTES = CoValGetClientIndBytes(CO_DEFTYPE_INTEGER24);

	dev = dcf_0x10_integer24_download_client_init();

	csdo = co_csdo_create(net, dev, SDO_NUM);

	co_csdo_start(csdo);

	co_csdo_set_dn_ind(csdo, &co_csdo_ind_dt_dn, NULL);
}

void
TestTeardown(void)
{
	co_csdo_stop(csdo);
	co_csdo_destroy(csdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (dnConCounter == 1u) {
		if (msgCounter != SDO_CLI_MSG_COUNT) {
			LOG_UINT32_EXPECTED("received messages",
					SDO_CLI_MSG_COUNT, msgCounter);
			FAIL_TEST("client: incorrect received messages count");
		} else if (sentMsgCounter != SDO_SRV_MSG_COUNT) {
			LOG_UINT32_EXPECTED("sent messages", SDO_SRV_MSG_COUNT,
					sentMsgCounter);
			FAIL_TEST("client: incorrect sent messages count");
		} else if (indCounter != SDO_CLI_IND_COUNT) {
			LOG_UINT32_EXPECTED("indications", SDO_CLI_IND_COUNT,
					indCounter);
			FAIL_TEST("client: incorrect indications count");
		} else if (indBytes != SDO_CLI_IND_BYTES) {
			if (SDO_CLI_IND_BYTES == 0) {
				LOG_UINT64_EXPECTED(
						"sent bytes", 0uL, indBytes);
				FAIL_TEST("client: sent bytes should be 0 (expedited transfer)");
			} else {
				LOG_UINT64_EXPECTED("sent bytes", SDO_VAL_SIZE,
						indBytes);
				FAIL_TEST("client: incorrect sent bytes count");
			}
		} else {
			FINISH_TEST();
		}
	}
}

void
TestStep(void)
{
	if (!reqSent) {
		const int ret = co_csdo_dn_val_req(csdo, IDX, SUBIDX,
				CO_DEFTYPE_INTEGER24, &val, NULL,
				&co_csdo_dn_con_func_dt, NULL);

		if (ret != 0) {
			LOG_INT32("ret", ret);
			LOG_INT32("errc", get_errc());
			FAIL_TEST("client: failed to submit the request");
		}

		reqSent = true;
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
