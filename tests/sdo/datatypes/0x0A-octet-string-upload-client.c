/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>
#include <stdint.h>

#include <lely/co/val.h>
#include <lely/util/error.h>
#include <lely/util/membuf.h>

#include "dcf/0x0A-octet-string-upload-client.h"
#include "sdo-client.h"
#include "sdo-datatypes.h"

#define ARRAY_LEN_BYTES (ARRAY_VAL_LEN * TEST_VAL_OS_ELEM_SIZE)

static const uint32_t SDO_VAL_SIZE = ARRAY_LEN_BYTES;

static const uint32_t SDO_CLI_MSG_COUNT =
		(((SDO_VAL_SIZE + SDO_SEGMENT_SIZE) - 1u) / SDO_SEGMENT_SIZE)
		+ 1u; // initiate message
static const uint32_t SDO_SRV_MSG_COUNT = SDO_CLI_MSG_COUNT;
static const uint32_t SDO_CLI_IND_COUNT = 2u;
static const uint32_t SDO_CLI_IND_BYTES = SDO_VAL_SIZE;

static bool reqSent = false;
static struct membuf buf = MEMBUF_INIT;
static uint8_t val_buf[ARRAY_LEN_BYTES] = { 0 };

static co_octet_string_t val = CO_ARRAY_C;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static void
co_csdo_up_con_func_dt(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	co_csdo_up_con_func(sdo, idx, subidx, ac, ptr, n, data);

	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoCon("client: SDO upload", idx, subidx);

	const uint_least8_t *const buf_ptr = ptr;
	const size_t bytes_read = co_val_read(
			CO_DEFTYPE_OCTET_STRING, &val, buf_ptr, buf_ptr + n);

	if (bytes_read != SDO_VAL_SIZE) {
		LOG_UINT64_EXPECTED("bytes read", SDO_VAL_SIZE, bytes_read);
		FAIL_TEST("client: invalid bytes read count");
	}
}

static void
co_csdo_ind_dt_up(const co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, size_t size, size_t nbyte, void *data)
{
	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoInd("client: SDO upload", idx, subidx);

	co_csdo_ind_func(sdo, idx, subidx, size, nbyte, data);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_0x0A_octet_string_upload_client_init();

	csdo = co_csdo_create(net, dev, SDO_NUM);

	co_csdo_start(csdo);

	membuf_init(&buf, val_buf, SDO_VAL_SIZE);
	co_csdo_set_up_ind(csdo, &co_csdo_ind_dt_up, NULL);
}

void
TestTeardown(void)
{
	co_csdo_stop(csdo);
	co_csdo_destroy(csdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (upConCounter == 1u) {
		if (msgCounter != SDO_CLI_MSG_COUNT) {
			LOG_UINT32_EXPECTED("received messages",
					SDO_CLI_MSG_COUNT, msgCounter);
			FAIL_TEST("client: incorrect received messages count");
		} else if (sentMsgCounter != SDO_SRV_MSG_COUNT) {
			LOG_UINT32_EXPECTED("sent messages", SDO_SRV_MSG_COUNT,
					sentMsgCounter);
			FAIL_TEST("client: incorrect sent messages count");
		} else if (indCounter != SDO_CLI_IND_COUNT) {
			LOG_UINT32_EXPECTED("indications", SDO_CLI_IND_COUNT,
					indCounter);
			FAIL_TEST("client: incorrect indications count");
		} else if (indBytes != SDO_CLI_IND_BYTES) {
			LOG_UINT64_EXPECTED("received bytes", SDO_CLI_IND_BYTES,
					indBytes);
			FAIL_TEST("client: incorrect received bytes count");
		} else if (co_val_cmp(CO_DEFTYPE_OCTET_STRING, &val,
					   &TEST_VAL_OS)
				!= 0) {
			LOG_UINT32("object", IDX);
			LOG_ARRAY("expected", TEST_VAL_OS, SDO_VAL_SIZE);
			LOG_ARRAY("read", val, SDO_VAL_SIZE);
			FAIL_TEST("client: received invalid value of the object");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestStep(void)
{
	if (!reqSent) {
		const int ret = co_csdo_up_req(csdo, IDX, SUBIDX, &buf,
				&co_csdo_up_con_func_dt, NULL);

		if (ret != 0) {
			LOG_INT32("ret", ret);
			LOG_INT32("errc", get_errc());
			FAIL_TEST("client: failed to submit the request");
		}

		reqSent = true;
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
