/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>
#include <stdint.h>

#include <lely/co/obj.h>

#include "dcf/0x12-integer40-download-server.h"
#include "sdo-datatypes.h"
#include "sdo-server.h"

static uint32_t SDO_VAL_SIZE = 0;
static uint32_t SDO_CLI_MSG_COUNT = 0;
static uint32_t SDO_SRV_MSG_COUNT = 0;
static uint32_t SDO_SRV_IND_COUNT = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static co_sub_t *sub2000_0 = NULL;

static co_unsigned32_t
co_sub2000_0_dn_ind_dt(co_sub_t *sub, struct co_sdo_req *req,
		co_unsigned32_t ac, void *data)
{
	if (sub != sub2000_0)
		FAIL_TEST("server: received upload request for other object");

	if (ac != 0) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub2000_0_dn_ind_dt received abort code");
		return ac;
	}

	return co_sub_dn_ind_func(sub, req, data);
}

void
TestSetup(can_net_t *const net)
{
	SDO_VAL_SIZE = CoValGetReadWriteSize(CO_DEFTYPE_INTEGER40);

	SDO_CLI_MSG_COUNT = CoValGetMsgCount(CO_DEFTYPE_INTEGER40);
	SDO_SRV_MSG_COUNT = CoValGetMsgCount(CO_DEFTYPE_INTEGER40);
	SDO_SRV_IND_COUNT = CoValGetServerDnIndCount(CO_DEFTYPE_INTEGER40);

	dev = dcf_0x12_integer40_download_server_init();

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_ssdo_start(ssdo);

	REQUIRE_SIZE(co_dev_set_val_i40(dev, IDX, SUBIDX, 0),
			sizeof(co_integer40_t));

	sub2000_0 = co_dev_find_sub(dev, IDX, SUBIDX);
	co_sub_set_dn_ind(sub2000_0, &co_sub2000_0_dn_ind_dt, NULL);
}

void
TestTeardown(void)
{
	co_ssdo_stop(ssdo);
	co_ssdo_destroy(ssdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == SDO_SRV_MSG_COUNT) {
		const co_integer40_t readValueI40 =
				co_dev_get_val_i40(dev, IDX, SUBIDX);

		if (readValueI40 != TEST_VAL_I40) {
			LOG_UINT32("OD object", IDX);
			LOG_UINT64_EXPECTED("object value",
					(uint64_t)TEST_VAL_I40,
					(uint64_t)readValueI40);
			FAIL_TEST("server: OD object does not contain expected value");
		} else if (sentMsgCounter != SDO_CLI_MSG_COUNT) {
			LOG_UINT32_EXPECTED("sent messages", SDO_CLI_MSG_COUNT,
					sentMsgCounter);
			FAIL_TEST("server: incorrect sent messages count");
		} else if (subDnInd != SDO_SRV_IND_COUNT) {
			LOG_UINT32_EXPECTED("indications", SDO_SRV_IND_COUNT,
					subDnInd);
			FAIL_TEST("server: incorrect indications count");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
