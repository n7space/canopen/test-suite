/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdint.h>

#include "dcf/0x09-visible-string-upload-server.h"
#include "sdo-datatypes.h"
#include "sdo-server.h"

enum sdo_dt_up_test_state {
	SDO_UP_INIT,
	SDO_UP_TRANSFER,
	SDO_UP_FINI,
	SDO_UP_DONE,
};

static enum sdo_dt_up_test_state testState = SDO_UP_INIT;

static const uint32_t SDO_VAL_SIZE = ARRAY_VAL_LEN * TEST_VAL_VS_ELEM_SIZE;
static const uint32_t SDO_SRV_IND_COUNT =
		(((SDO_VAL_SIZE + SDO_SEGMENT_SIZE) - 1u) / SDO_SEGMENT_SIZE)
		+ 1u; // initiate message
static const uint32_t SDO_CLI_MSG_COUNT = SDO_SRV_IND_COUNT;
static const uint32_t SDO_SRV_MSG_COUNT = SDO_SRV_IND_COUNT;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static uint32_t subUpIndDt = 0;

static co_sub_t *sub2000_0 = NULL;

static const char *sdo_up_buf = (char *)TEST_VAL_VS;
static size_t sdo_up_nbyte = 7u;
static size_t sdo_up_offset = 0;

static co_unsigned32_t
co_sub2000_0_up_ind_dt(const co_sub_t *sub, struct co_sdo_req *req,
		co_unsigned32_t ac, void *data)
{
	(void)data;

	++subUpIndDt;

	if (sub != sub2000_0)
		FAIL_TEST("server: received upload request for other object");

	if (ac != 0) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub2000_0_up_ind_dt received abort code");
		return ac;
	}

	switch (testState) {
	case SDO_UP_INIT:
		co_sdo_req_init(req, NULL);
		req->size = SDO_VAL_SIZE;
		testState = SDO_UP_TRANSFER;
		break;
	case SDO_UP_TRANSFER:
		req->buf = sdo_up_buf;
		req->offset = sdo_up_offset;
		req->nbyte = sdo_up_nbyte;

		sdo_up_buf += req->nbyte;
		sdo_up_offset += req->nbyte;

		if ((sdo_up_offset + sdo_up_nbyte) > req->size) {
			testState = SDO_UP_FINI;
			sdo_up_nbyte = req->size - sdo_up_offset;
		}
		break;
	case SDO_UP_FINI:
		req->buf = sdo_up_buf;
		req->offset = sdo_up_offset;
		req->nbyte = sdo_up_nbyte;
		testState = SDO_UP_DONE;
		break;
	default: FAIL_TEST("server: invalid test state");
	}

	return 0;
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_0x09_visible_string_upload_server_init();

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_ssdo_start(ssdo);

	sub2000_0 = co_dev_find_sub(dev, IDX, SUBIDX);
	co_sub_set_up_ind(sub2000_0, &co_sub2000_0_up_ind_dt, NULL);
}

void
TestTeardown(void)
{
	co_ssdo_stop(ssdo);
	co_ssdo_destroy(ssdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == SDO_SRV_MSG_COUNT) {
		if (subUpIndDt != SDO_SRV_IND_COUNT) {
			LOG_UINT32_EXPECTED("upload indications",
					SDO_SRV_IND_COUNT, subUpIndDt);
			FAIL_TEST("server: incorrect upload indications count");
		} else if (sentMsgCounter != SDO_CLI_MSG_COUNT) {
			LOG_UINT32_EXPECTED("sent messages", SDO_CLI_MSG_COUNT,
					sentMsgCounter);
			FAIL_TEST("server: incorrect sent messages count");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
