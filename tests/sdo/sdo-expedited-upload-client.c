/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/val.h>
#include <lely/util/error.h>

#include "dcf/sdo-expedited-upload-client.h"
#include "sdo-client.h"

static bool reqSent = false;

static co_integer16_t val = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static void
co_csdo_up_con_func_exp(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	co_csdo_up_con_func(sdo, idx, subidx, ac, ptr, n, data);

	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX)) {
		FailTestInvalidSdoCon(
				"client: SDO expedited upload", idx, subidx);
	}

	const uint_least8_t *const buf_ptr = ptr;
	const size_t bytes_read = co_val_read(
			CO_DEFTYPE_INTEGER16, &val, buf_ptr, buf_ptr + n);

	if (bytes_read != co_type_sizeof(CO_DEFTYPE_INTEGER16)) {
		LOG_UINT64_EXPECTED("bytes read",
				co_type_sizeof(CO_DEFTYPE_INTEGER16),
				bytes_read);
		FAIL_TEST("client: invalid bytes read count");
	} else if (val != SDO_EXP_VAL) {
		LOG_INT32_EXPECTED("value", SDO_EXP_VAL, val);
		FAIL_TEST("client: received invalid value of 0x2000 object");
	} else {
		FINISH_TEST();
	}
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sdo_expedited_upload_client_init();

	csdo = co_csdo_create(net, dev, SDO_NUM);

	co_csdo_start(csdo);
}

void
TestTeardown(void)
{
	co_csdo_stop(csdo);
	co_csdo_destroy(csdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter > 1u)
		FAIL_TEST("client: too many messages received");
}

void
TestStep(void)
{
	if (!reqSent) {
		const int ret = co_csdo_up_req(csdo, IDX, SUBIDX, NULL,
				&co_csdo_up_con_func_exp, NULL);

		if (ret != 0) {
			LOG_INT32("ret", ret);
			LOG_INT32("errc", get_errc());
			FAIL_TEST("client: failed to submit the request");
		}

		reqSent = true;
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;

	if (sentMsgCounter > 1u)
		FAIL_TEST("client: too many messages sent");
}
