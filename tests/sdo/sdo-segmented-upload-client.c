/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/val.h>
#include <lely/util/error.h>

#include "dcf/sdo-segmented-upload-client.h"
#include "sdo-client.h"

static bool reqSent = false;

static co_integer64_t val = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static void
co_csdo_up_con_func_seg(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	co_csdo_up_con_func(sdo, idx, subidx, ac, ptr, n, data);

	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoCon(
				"client: SDO segmented upload", idx, subidx);

	const uint_least8_t *const buf_ptr = ptr;
	const size_t bytes_read = co_val_read(
			CO_DEFTYPE_INTEGER64, &val, buf_ptr, buf_ptr + n);

	if (bytes_read != co_type_sizeof(CO_DEFTYPE_INTEGER64)) {
		LOG_UINT64_EXPECTED("bytes read",
				co_type_sizeof(CO_DEFTYPE_INTEGER64),
				bytes_read);
		FAIL_TEST("client: invalid bytes read count");
	}
}

static void
co_csdo_ind_seg_up(const co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, size_t size, size_t nbyte, void *data)
{
	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoCon(
				"client: SDO segmented upload", idx, subidx);

	co_csdo_ind_func(sdo, idx, subidx, size, nbyte, data);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sdo_segmented_upload_client_init();

	csdo = co_csdo_create(net, dev, SDO_NUM);

	co_csdo_start(csdo);

	co_csdo_set_up_ind(csdo, &co_csdo_ind_seg_up, NULL);
}

void
TestTeardown(void)
{
	co_csdo_stop(csdo);
	co_csdo_destroy(csdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (upConCounter == 1u) {
		if (msgCounter != SDO_SEG_MSG_COUNT) {
			LOG_UINT32_EXPECTED("received messages",
					SDO_SEG_MSG_COUNT, msgCounter);
			FAIL_TEST("client: incorrect received messages count");
		} else if (sentMsgCounter != SDO_SEG_MSG_COUNT) {
			LOG_UINT32_EXPECTED("sent messages", SDO_SEG_MSG_COUNT,
					sentMsgCounter);
			FAIL_TEST("client: incorrect sent messages count");
		} else if (indCounter != SDO_SEG_IND_COUNT) {
			LOG_UINT32_EXPECTED("indications", SDO_SEG_IND_COUNT,
					indCounter);
			FAIL_TEST("client: incorrect indications count");
		} else if (indBytes != sizeof(val)) {
			LOG_UINT64_EXPECTED("received bytes", sizeof(val),
					indBytes);
			FAIL_TEST("client: incorrect received bytes count");
		} else if (val != SDO_SEG_VAL) {
			LOG_INT64_EXPECTED("value", SDO_SEG_VAL, val);
			FAIL_TEST("client: received invalid value of 0x2000 object");
		} else
			FINISH_TEST();
	}
}

void
TestStep(void)
{
	if (!reqSent) {
		const int ret = co_csdo_up_req(csdo, IDX, SUBIDX, NULL,
				&co_csdo_up_con_func_seg, NULL);

		if (ret != 0) {
			LOG_INT32("ret", ret);
			LOG_INT32("errc", get_errc());
			FAIL_TEST("client: failed to submit the request");
		}

		reqSent = true;
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
