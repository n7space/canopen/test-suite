/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>
#include <string.h>

#include <lely/util/error.h>
#include <lely/util/membuf.h>

#include "dcf/sdo-block-upload-domain-client.h"
#include "sdo-client.h"

static uint32_t SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT = 0;

static struct membuf buf = MEMBUF_INIT;
static char val[SDO_BLK_VALUE_SIZE] = { 0 };

static bool reqSent = false;
static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static void
co_csdo_up_con_func_blk(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	co_csdo_up_con_func(sdo, idx, subidx, ac, ptr, n, data);

	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoCon("client: SDO block upload", idx, subidx);
}

static void
co_csdo_ind_blk_up(const co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, size_t size, size_t nbyte, void *data)
{
	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
		FailTestInvalidSdoInd("client: SDO block upload", idx, subidx);

	co_csdo_ind_func(sdo, idx, subidx, size, nbyte, data);
}

void
TestSetup(can_net_t *const net)
{
	SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT = ((uint32_t)strlen(SDO_BLK_VALUE) / 7u)
			+ 1u + 2u; // initiate and end message

	dev = dcf_sdo_block_upload_domain_client_init();

	csdo = co_csdo_create(net, dev, SDO_NUM);

	co_csdo_start(csdo);

	membuf_init(&buf, val, SDO_BLK_VALUE_SIZE);
	co_csdo_set_up_ind(csdo, &co_csdo_ind_blk_up, NULL);
}

void
TestTeardown(void)
{
	co_csdo_stop(csdo);
	co_csdo_destroy(csdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (upConCounter == 1u) {
		if (msgCounter != SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT) {
			LOG_UINT32_EXPECTED("received messages",
					SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT,
					msgCounter);
			FAIL_TEST("client: incorrect received messages count");
		} else if (sentMsgCounter != SDO_BLK_MSG_UP_DOMAIN_SRV_COUNT) {
			LOG_UINT32_EXPECTED("sent messages",
					SDO_BLK_MSG_UP_DOMAIN_SRV_COUNT,
					msgCounter);
			FAIL_TEST("client: incorrect sent messages count");
		} else if (indCounter != SDO_BLK_IND_UP_DOMAIN_CLI_COUNT) {
			LOG_UINT32_EXPECTED("indications",
					SDO_BLK_IND_UP_DOMAIN_CLI_COUNT,
					indCounter);
			FAIL_TEST("client: incorrect indications count");
		} else if (indBytes != SDO_BLK_VALUE_SIZE) {
			LOG_UINT64_EXPECTED("received bytes",
					SDO_BLK_VALUE_SIZE, indBytes);
			FAIL_TEST("client: incorrect received bytes count");
		} else if (memcmp(val, SDO_BLK_VALUE, SDO_BLK_VALUE_SIZE)
				!= 0) {
			FAIL_TEST("client: received invalid value of 0x2000 object");
		} else
			FINISH_TEST();
	}
}

void
TestStep(void)
{
	if (!reqSent) {
		const int ret = co_csdo_blk_up_req(csdo, IDX, SUBIDX, 0, &buf,
				&co_csdo_up_con_func_blk, NULL);

		if (ret != 0) {
			LOG_INT32("ret", ret);
			LOG_INT32("errc", get_errc());
			FAIL_TEST("client: failed to submit the request");
		}

		reqSent = true;
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
