/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include "dcf/sdo-abort-transfer-client.h"
#include "sdo-client.h"

enum sdo_abort_test_step {
	SDO_ABORT_INIT,
	SDO_ABORT_OUT_OF_MEMORY,
	SDO_ABORT_NO_READ_ACCESS,
	SDO_ABORT_NO_WRITE_ACCESS,
	SDO_ABORT_NO_OBJ,
	SDO_ABORT_NO_SUB,
	SDO_ABORT_TYPE_LEN,
	SDO_ABORT_NO_SDO,
	SDO_ABORT_TIMEOUT,
	SDO_ABORT_DONE,
};

static enum sdo_abort_test_step testStep = SDO_ABORT_INIT;
static bool inTest = false;

static const uint32_t SDO_ABORT_DN_CON_COUNT = 5u;
static const uint32_t SDO_ABORT_UP_CON_COUNT = 3u;

static co_integer16_t val = SDO_EXP_VAL;
#define ARRAY_SIZE 10
static const uint_least8_t array_val[ARRAY_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8, 9,
	0 };

static uint32_t msgCounter = 0;

static void
co_csdo_dn_con_func_abrt(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, void *data)
{
	(void)data;

	++dnConCounter;

	switch (testStep) {
	case SDO_ABORT_OUT_OF_MEMORY:
		if ((sdo != csdo) || (idx != 0x2004) || (subidx != SUBIDX))
			FailTestInvalidSdoCon("OUT_OF_MEMORY", idx, subidx);
		else if (ac != CO_SDO_AC_NO_MEM)
			FailTestInvalidAc("OUT_OF_MEMORY", ac);
		testStep = SDO_ABORT_NO_READ_ACCESS;
		break;
	case SDO_ABORT_NO_WRITE_ACCESS:
		if ((sdo != csdo) || (idx != 0x2002) || (subidx != SUBIDX))
			FailTestInvalidSdoCon("NO_WRITE_ACCESS", idx, subidx);
		else if (ac != CO_SDO_AC_NO_WRITE)
			FailTestInvalidAc("NO_WRITE_ACCESS", ac);
		testStep = SDO_ABORT_NO_OBJ;
		break;
	case SDO_ABORT_NO_SUB:
		if ((sdo != csdo) || (idx != IDX) || (subidx != 0x01))
			FailTestInvalidSdoCon("NO_SUB", idx, subidx);
		else if (ac != CO_SDO_AC_NO_SUB)
			FailTestInvalidAc("NO_SUB", ac);
		testStep = SDO_ABORT_TYPE_LEN;
		break;
	case SDO_ABORT_TYPE_LEN:
		if ((sdo != csdo) || (idx != 0x2001) || (subidx != SUBIDX))
			FailTestInvalidSdoCon("TYPE_LEN", idx, subidx);
		else if (ac != CO_SDO_AC_TYPE_LEN_LO)
			FailTestInvalidAc("TYPE_LEN", ac);
		testStep = SDO_ABORT_NO_SDO;
		break;
	case SDO_ABORT_NO_SDO:
		if ((sdo != csdo) || (idx != 0x2004) || (subidx != SUBIDX))
			FailTestInvalidSdoCon("NO_SDO", idx, subidx);
		else if (ac != CO_SDO_AC_NO_SDO)
			FailTestInvalidAc("NO_SDO", ac);
		testStep = SDO_ABORT_TIMEOUT;
		break;
	default: FAIL_TEST("Invalid test state."); return;
	}

	inTest = false;
}

static void
co_csdo_up_con_func_abrt(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	(void)ptr;
	(void)n;
	(void)data;

	++upConCounter;

	if (testStep == SDO_ABORT_NO_READ_ACCESS) {
		if ((sdo != csdo) || (idx != 0x2003) || (subidx != SUBIDX))
			FailTestInvalidSdoCon("NO_READ_ACCESS", idx, subidx);
		else if (ac != CO_SDO_AC_NO_READ)
			FailTestInvalidAc("NO_READ_ACCESS", ac);
		testStep = SDO_ABORT_NO_WRITE_ACCESS;
	} else if (testStep == SDO_ABORT_NO_OBJ) {
		if ((sdo != csdo) || (idx != 0x2fff) || (subidx != SUBIDX))
			FailTestInvalidSdoCon("NO_OBJ", idx, subidx);
		else if (ac != CO_SDO_AC_NO_OBJ)
			FailTestInvalidAc("NO_OBJ", ac);
		testStep = SDO_ABORT_NO_SUB;
	} else if (testStep == SDO_ABORT_TIMEOUT) {
		if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX))
			FailTestInvalidSdoCon("TIMEOUT", idx, subidx);
		else if (ac != CO_SDO_AC_TIMEOUT)
			FailTestInvalidAc("TIMEOUT", ac);
		co_csdo_set_timeout(csdo, 0);
		testStep = SDO_ABORT_DONE;
	} else {
		FAIL_TEST("Invalid test state.");
		return;
	}

	inTest = false;
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sdo_abort_transfer_client_init();

	csdo = co_csdo_create(net, dev, SDO_NUM);

	co_csdo_start(csdo);

	testStep = SDO_ABORT_OUT_OF_MEMORY;
}

void
TestTeardown(void)
{
	co_csdo_stop(csdo);
	co_csdo_destroy(csdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;
}

void
TestStep(void)
{
	if (inTest)
		return;

	switch (testStep) {
	case SDO_ABORT_INIT: return;
	case SDO_ABORT_OUT_OF_MEMORY:
		REQUIRE(co_csdo_dn_req(csdo, 0x2004, SUBIDX, array_val,
				ARRAY_SIZE, &co_csdo_dn_con_func_abrt, NULL));
		break;
	case SDO_ABORT_NO_READ_ACCESS:
		REQUIRE(co_csdo_up_req(csdo, 0x2003, SUBIDX, NULL,
				&co_csdo_up_con_func_abrt, NULL));
		break;
	case SDO_ABORT_NO_WRITE_ACCESS:
		REQUIRE(co_csdo_dn_req(csdo, 0x2002, SUBIDX, &val, sizeof(val),
				&co_csdo_dn_con_func_abrt, NULL));
		break;
	case SDO_ABORT_NO_OBJ:
		REQUIRE(co_csdo_up_req(csdo, 0x2fff, SUBIDX, NULL,
				&co_csdo_up_con_func_abrt, NULL));
		break;
	case SDO_ABORT_NO_SUB:
		REQUIRE(co_csdo_dn_req(csdo, IDX, 0x01, &val, sizeof(val),
				&co_csdo_dn_con_func_abrt, NULL));
		break;
	case SDO_ABORT_TYPE_LEN:
		REQUIRE(co_csdo_dn_req(csdo, 0x2001, SUBIDX, &val, sizeof(val),
				&co_csdo_dn_con_func_abrt, NULL));
		break;
	case SDO_ABORT_NO_SDO:
		REQUIRE(co_csdo_dn_req(csdo, 0x2004, SUBIDX, &array_val,
				CO_SDO_REQ_MEMBUF_SIZE,
				&co_csdo_dn_con_func_abrt, NULL));
		break;
	case SDO_ABORT_TIMEOUT:
		co_csdo_set_timeout(csdo, 1);
		REQUIRE(co_csdo_up_req(csdo, IDX, SUBIDX, NULL,
				&co_csdo_up_con_func_abrt, NULL));
		break;
	case SDO_ABORT_DONE:
		if ((dnConCounter == SDO_ABORT_DN_CON_COUNT)
				&& (upConCounter == SDO_ABORT_UP_CON_COUNT)
				&& (msgCounter == SDO_ABORT_MSG_CLI_COUNT)) {
			FINISH_TEST();
		} else
			FAIL_TEST("Invalid number of upload/download confirmations or messages.");
		return;
	}
	inTest = true;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
}
