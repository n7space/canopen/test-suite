/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/co/obj.h>

#include "dcf/sdo-block-download-domain-server.h"
#include "sdo-server.h"

enum sdo_blk_dn_state {
	SDO_DN_INIT,
	SDO_DN_SEG,
	SDO_DN_LAST,
	SDO_DN_DONE,
};

static enum sdo_blk_dn_state testState = SDO_DN_INIT;

static uint32_t SDO_BLK_MSG_DN_DOMAIN_SRV_COUNT = 0;
static uint32_t SDO_BLK_IND_DN_DOMAIN_SRV_COUNT = 0;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static uint32_t subBlkDnInd = 0;

static co_sub_t *sub2000_0 = NULL;
static const char *buf = NULL;

inline static co_unsigned32_t
co_sub2000_0_dn_ind_seg(co_sub_t *sub, struct co_sdo_req *req,
		co_unsigned32_t ac, void *data)
{
	(void)data;

	++subBlkDnInd;

	if (sub != sub2000_0)
		FAIL_TEST("server: received upload request for other object");

	if (ac != 0) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub2000_0_dn_ind_seg received abort code");
		return ac;
	}

	if ((testState == SDO_DN_SEG)
			&& ((req->offset + req->nbyte) == req->size)) {
		testState = SDO_DN_LAST;
	}

	switch (testState) {
	case SDO_DN_INIT:
		if (req->nbyte != 0)
			FAIL_TEST("server: invalid SDO block download initiate message");
		testState = SDO_DN_SEG;
		buf = SDO_BLK_VALUE;
		break;
	case SDO_DN_SEG:
		if (0 != strncmp(req->buf, buf, req->nbyte))
			FAIL_TEST("server: SDO sub-block segment data do not match");
		buf += req->nbyte;
		break;
	case SDO_DN_LAST:
		if (0 != strncmp(req->buf, "last", req->nbyte))
			FAIL_TEST("server: SDO sub-block last segment data do not match");
		testState = SDO_DN_DONE;
		break;
	default: FAIL_TEST("server: invalid test state");
	}

	return 0;
}

void
TestSetup(can_net_t *const net)
{
	SDO_BLK_MSG_DN_DOMAIN_SRV_COUNT = ((uint32_t)strlen(SDO_BLK_VALUE) / 7u)
			+ 1u + 2u; // initiate and end message
	SDO_BLK_IND_DN_DOMAIN_SRV_COUNT = SDO_BLK_MSG_DN_DOMAIN_SRV_COUNT - 1u;

	dev = dcf_sdo_block_download_domain_server_init();

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_ssdo_start(ssdo);

	sub2000_0 = co_dev_find_sub(dev, IDX, SUBIDX);
	co_sub_set_dn_ind(sub2000_0, &co_sub2000_0_dn_ind_seg, NULL);
}

void
TestTeardown(void)
{
	co_ssdo_stop(ssdo);
	co_ssdo_destroy(ssdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == SDO_BLK_MSG_DN_DOMAIN_SRV_COUNT) {
		if (subBlkDnInd != SDO_BLK_IND_DN_DOMAIN_SRV_COUNT) {
			LOG_UINT32_EXPECTED("indications",
					SDO_BLK_IND_DN_DOMAIN_SRV_COUNT,
					subBlkDnInd);
			FAIL_TEST("server: incorrect indications count");
		} else if (sentMsgCounter != SDO_BLK_MSG_DN_DOMAIN_CLI_COUNT) {
			LOG_UINT32_EXPECTED("sent messages",
					SDO_BLK_MSG_DN_DOMAIN_CLI_COUNT,
					sentMsgCounter);
			FAIL_TEST("server: incorrect sent messages count");
		} else
			FINISH_TEST();
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
