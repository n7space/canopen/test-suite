/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/obj.h>

#include "dcf/sdo-abort-transfer-server.h"
#include "sdo-server.h"

static uint32_t msgCounter = 0;

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sdo_abort_transfer_server_init();

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_ssdo_start(ssdo);
}

void
TestTeardown(void)
{
	co_ssdo_destroy(ssdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == SDO_ABORT_MSG_SRV_STOP)
		co_ssdo_stop(ssdo);
	else if (msgCounter == SDO_ABORT_MSG_SRV_COUNT)
		FINISH_TEST();
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
}
