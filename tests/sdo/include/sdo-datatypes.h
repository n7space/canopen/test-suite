/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_SDO_DATATYPES_H__
#define CTSSW_SDO_DATATYPES_H__

#include <lely/co/type.h>
#include <lely/co/val.h>
#include <lely/compat/uchar.h>

#include <TestFramework/TestHarness.h>

static const uint32_t SDO_EXPEDITED_MAX_SIZE = 4u;
static const uint32_t SDO_SEGMENT_SIZE = 7u;

/* basic types */
static const co_boolean_t TEST_VAL_B = 0x1u;
static const co_integer8_t TEST_VAL_I8 = 0x1A;
static const co_integer16_t TEST_VAL_I16 = 0x1234;
static const co_integer32_t TEST_VAL_I32 = 0x12345678;
static const co_unsigned8_t TEST_VAL_U8 = 0xABu;
static const co_unsigned16_t TEST_VAL_U16 = 0xABCDu;
static const co_unsigned32_t TEST_VAL_U32 = 0xABCDEF12u;
static const co_real32_t TEST_VAL_R32 = 1234.5678f;
static const co_integer24_t TEST_VAL_I24 = 0x123456;
static const co_real64_t TEST_VAL_R64 = 1234.5678f;
static const co_integer40_t TEST_VAL_I40 = 0x1234567890;
static const co_integer48_t TEST_VAL_I48 = 0x1234567890AB;
static const co_integer56_t TEST_VAL_I56 = 0x1234567890ABCD;
static const co_integer64_t TEST_VAL_I64 = 0x1234567890ABCDEF;
static const co_unsigned24_t TEST_VAL_U24 = 0xABCDEFu;
static const co_unsigned40_t TEST_VAL_U40 = 0xABCDEF1234u;
static const co_unsigned48_t TEST_VAL_U48 = 0xABCDEF123456u;
static const co_unsigned56_t TEST_VAL_U56 = 0xABCDEF12345678u;
static const co_unsigned64_t TEST_VAL_U64 = 0xABCDEF1234567890u;

/* time types */
static const co_time_of_day_t TEST_VAL_T = { 0x02345678u, 0xABCDu };
static const co_time_diff_t TEST_VAL_TD = { 0x02345678u, 0xABCDu };

static const co_time_scet_t TEST_VAL_SCET = { 0x12345678u, 0xABCDEFu };
static const co_time_sutc_t TEST_VAL_SUTC = { 0x9EF0u, 0x12345678u, 0xABCDu };

/* array types */
#define ARRAY_VAL_LEN 26

static const co_visible_string_t TEST_VAL_VS = CO_VISIBLE_STRING_NC(
		ARRAY_VAL_LEN, "abcdefghijklmnopqrstuvwxyz");

static const co_unicode_string_t TEST_VAL_US = CO_UNICODE_STRING_NC(
		ARRAY_VAL_LEN, { u"abcdefghijklmnopqrstuvwxyz" });

static const co_octet_string_t TEST_VAL_OS = CO_OCTET_STRING_NC(ARRAY_VAL_LEN,
		"\xF1\xE2\xD3\xC4\xB5\xA6\x97\x88\x79\x6A\x5B\x4C\x3D"
		"\x2E\x1F\x10\x21\x32\x43\x54\x65\x76\x87\x98\xA9\x1A");

static const co_domain_t TEST_VAL_DOM =
		CO_DOMAIN_NC(uint_least8_t, ARRAY_VAL_LEN,
				{
						0xF1u,
						0xE2u,
						0xD3u,
						0xC4u,
						0xB5u,
						0xA6u,
						0x97u,
						0x88u,
						0x79u,
						0x6Au,
						0x5Bu,
						0x4Cu,
						0x3Du,
						0x2Eu,
						0x1Fu,
						0x10u,
						0x21u,
						0x32u,
						0x43u,
						0x54u,
						0x65u,
						0x76u,
						0x87u,
						0x98u,
						0xA9u,
						0x1Au,
				});

#define TEST_VAL_VS_ELEM_SIZE sizeof(TEST_VAL_VS[0])
#define TEST_VAL_US_ELEM_SIZE sizeof(TEST_VAL_US[0])
#define TEST_VAL_OS_ELEM_SIZE sizeof(TEST_VAL_OS[0])
#define TEST_VAL_DOM_ELEM_SIZE sizeof(uint_least8_t)

/**
 * Returns the read/write size (in bytes) of the value of the specified type.
 * In most cases function returns the same value as co_type_sizeof(), but for
 * a few integer/unsigned types size is different then the size in memory.
 *
 * @see co_type_sizeof()
 */
static inline uint32_t
CoValGetReadWriteSize(const co_unsigned16_t type)
{
	switch (type) {
	case CO_DEFTYPE_INTEGER24: return 3u;
	case CO_DEFTYPE_INTEGER40: return 5u;
	case CO_DEFTYPE_INTEGER48: return 6u;
	case CO_DEFTYPE_INTEGER56: return 7u;
	case CO_DEFTYPE_UNSIGNED24: return 3u;
	case CO_DEFTYPE_UNSIGNED40: return 5u;
	case CO_DEFTYPE_UNSIGNED48: return 6u;
	case CO_DEFTYPE_UNSIGNED56: return 7u;
	case CO_DEFTYPE_TIME_OF_DAY:
	case CO_DEFTYPE_TIME_DIFF: return 6u;
	case CO_DEFTYPE_TIME_SCET: return 7u;
	case CO_DEFTYPE_TIME_SUTC: return 8u;
	default: return (uint32_t)co_type_sizeof(type);
	}
}

/**
 * Returns the message count server/client receives in SDO download/upload
 * transfer. For expedited transfers, there is only one initiate/confirm message
 * for both sides. In all other cases there is one initiate/confirm message plus
 * one message and confirmation for each 7-byte segment of the value.
 */
static inline uint32_t
CoValGetMsgCount(const co_unsigned16_t type)
{
	const uint32_t val_size = CoValGetReadWriteSize(type);
	return (val_size <= SDO_EXPEDITED_MAX_SIZE)
			? 1u
			: (((val_size - 1u) / SDO_SEGMENT_SIZE) + 2u);
}

/**
 * Returns the number of indications client gets in SDO download/upload
 * transfer. There are no indications for expedited transfer and two otherwise.
 */
static inline uint32_t
CoValGetClientIndCount(const co_unsigned16_t type)
{
	const uint32_t val_size = CoValGetReadWriteSize(type);
	return val_size <= SDO_EXPEDITED_MAX_SIZE ? 0 : 2u;
}

/**
 * Returns indicated number of sent/received bytes for client in SDO transfer.
 * For expedited transfers number equals zero, because there is no need to use
 * a receive/send buffer as the entire value is available right away.
 */
static inline uint32_t
CoValGetClientIndBytes(const co_unsigned16_t type)
{
	const uint32_t val_size = CoValGetReadWriteSize(type);
	return val_size <= SDO_EXPEDITED_MAX_SIZE ? 0 : val_size;
}

/**
 * Returns the number of download indications server gets in SDO download
 * transfer. There is one indication for each 7-byte segment of the value.
 */
static inline uint32_t
CoValGetServerDnIndCount(const co_unsigned16_t type)
{
	const uint32_t val_size = CoValGetReadWriteSize(type);
	return ((val_size - 1u) / SDO_SEGMENT_SIZE) + 1u;
}

#endif // CTSSW_SDO_DATATYPES_H__
