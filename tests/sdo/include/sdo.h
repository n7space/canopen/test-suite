/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_SDO_H__
#define CTSSW_SDO_H__

#include <lely/co/dev.h>
#include <lely/co/sdo.h>

#include <TestFramework/TestHarness.h>

static co_dev_t *dev = NULL;
static const co_unsigned8_t SDO_NUM = 0x01;

static const co_unsigned16_t IDX = 0x2000u;
static const co_unsigned8_t SUBIDX = 0x00u;

// SDO expedited download/upload
static const co_integer16_t SDO_EXP_VAL = 0x1234;

// SDO segmented download/upload
static const co_integer64_t SDO_SEG_VAL = 0x1234567890C0DE01;
static const uint32_t SDO_SEG_IND_COUNT = 2u;
static const uint32_t SDO_SEG_MSG_COUNT = 3u;

// SDO block download/upload
static const uint32_t SDO_BLK_MSG_DN_SRV_COUNT = 4u;
static const uint32_t SDO_BLK_MSG_DN_CLI_COUNT = 3u;
static const uint32_t SDO_BLK_MSG_UP_COUNT = 4u;

static const uint32_t SDO_BLK_IND_SRV_COUNT = 3u;
static const uint32_t SDO_BLK_IND_CLI_COUNT = 2u;

static const uint32_t SDO_BLK_MSG_DN_DOMAIN_CLI_COUNT = 4u;
static const uint32_t SDO_BLK_IND_DN_DOMAIN_CLI_COUNT = 3u;
static const uint32_t SDO_BLK_MSG_UP_DOMAIN_SRV_COUNT = 5u;
static const uint32_t SDO_BLK_IND_UP_DOMAIN_CLI_COUNT = 3u;

#define SDO_BLK_VALUE_SIZE 1124u
#define SDO_BLK_VALUE \
	"abc_001def_002ghi_003jkl_004mno_005pqr_006stu_007vwx_008yzZ_009ABC_010" \
	"abc_011def_012ghi_013jkl_014mno_015pqr_016stu_017vwx_018yzZ_019ABC_020" \
	"abc_021def_022ghi_023jkl_024mno_025pqr_026stu_027vwx_028yzZ_029ABC_030" \
	"abc_031def_032ghi_033jkl_034mno_035pqr_036stu_037vwx_038yzZ_039ABC_040" \
	"abc_041def_042ghi_043jkl_044mno_045pqr_046stu_047vwx_048yzZ_049ABC_050" \
	"abc_051def_052ghi_053jkl_054mno_055pqr_056stu_057vwx_058yzZ_059ABC_060" \
	"abc_061def_062ghi_063jkl_064mno_065pqr_066stu_067vwx_068yzZ_069ABC_070" \
	"abc_071def_072ghi_073jkl_074mno_075pqr_076stu_077vwx_078yzZ_079ABC_080" \
	"abc_081def_082ghi_083jkl_084mno_085pqr_086stu_087vwx_088yzZ_089ABC_090" \
	"abc_091def_092ghi_093jkl_094mno_095pqr_096stu_097vwx_098yzZ_099ABC_100" \
	"abc_101def_102ghi_103jkl_104mno_105pqr_106stu_107vwx_108yzZ_109ABC_110" \
	"abc_111def_112ghi_113jkl_114mno_115pqr_116stu_117vwx_118yzZ_119ABC_120" \
	"abc_121def_122ghi_123jkl_124mno_125pqr_126stu_127vwx_128yzZ_129ABC_130" \
	"abc_131def_132ghi_133jkl_134mno_135pqr_136stu_137vwx_138yzZ_139ABC_140" \
	"abc_141def_142ghi_143jkl_144mno_145pqr_146stu_147vwx_148yzZ_149ABC_150" \
	"abc_151def_152ghi_153jkl_154mno_155pqr_156stu_157vwx_158yzZ_159ABC_160" \
	"last"

// SDO abort transfer
static const uint32_t SDO_ABORT_MSG_SRV_COUNT = 12u;
static const uint32_t SDO_ABORT_MSG_SRV_STOP = 9u;
static const uint32_t SDO_ABORT_MSG_CLI_COUNT = 10u;

// SDO error reporting functions
static inline void
FailTestInvalidSdoCon(const char errTag[], const co_unsigned16_t idx,
		const co_unsigned8_t subidx)
{
	LOG_STR("invalid SDO confirmation", errTag);
	LOG_UINT32("idx", idx);
	LOG_UINT32("subidx", subidx);
}

static inline void
FailTestInvalidSdoInd(const char errTag[], const co_unsigned16_t idx,
		const co_unsigned8_t subidx)
{
	LOG_STR("invalid SDO indication", errTag);
	LOG_UINT32("idx", idx);
	LOG_UINT32("subidx", subidx);
}

static inline void
FailTestInvalidAc(const char errTag[], const co_unsigned32_t ac)
{
	LOG_STR("invalid AC error", errTag);
	LOG_STR("error", co_sdo_ac2str(ac));
}

#endif // SDO_H__
