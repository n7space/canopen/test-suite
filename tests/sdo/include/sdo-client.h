/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_SDO_CLIENT_H__
#define CTSSW_SDO_CLIENT_H__

#include "sdo.h"

#include <lely/co/csdo.h>

static co_csdo_t *csdo = NULL;

static uint32_t dnConCounter = 0;
static uint32_t upConCounter = 0;
static uint32_t indCounter = 0;
static size_t indBytes = 0;

inline static void
co_csdo_dn_con_func(co_csdo_t *sdo, co_unsigned16_t idx, co_unsigned8_t subidx,
		co_unsigned32_t ac, void *data)
{
	(void)sdo;
	(void)idx;
	(void)subidx;
	(void)data;

	++dnConCounter;

	if (ac != 0) {
		LOG_STR("AC error", co_sdo_ac2str(ac));
		FAIL_TEST("SDO download confirmation error");
	}
}

inline static void
co_csdo_up_con_func(co_csdo_t *sdo, co_unsigned16_t idx, co_unsigned8_t subidx,
		co_unsigned32_t ac, const void *ptr, size_t n, void *data)
{
	(void)sdo;
	(void)idx;
	(void)subidx;
	(void)ptr;
	(void)n;
	(void)data;

	++upConCounter;

	if (ac != 0) {
		LOG_STR("AC error", co_sdo_ac2str(ac));
		FAIL_TEST("SDO upload confirmation error");
	}
}

inline static void
co_csdo_ind_func(const co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, size_t size, size_t nbyte, void *data)
{
	(void)sdo;
	(void)idx;
	(void)subidx;
	(void)size;
	(void)data;

	++indCounter;

	if ((indBytes != 0) && (indBytes >= nbyte)) {
		LOG_UINT64("received/sent byte", nbyte);
		LOG_UINT64("received/sent bytes", indBytes);
		FAIL_TEST("New received/sent bytes number is lower than already received/sent bytes count");
	} else
		indBytes = nbyte;
}

#endif // SDO_CLIENT_H__
