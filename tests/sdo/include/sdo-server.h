/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_SDO_SERVER_H__
#define CTSSW_SDO_SERVER_H__

#include "sdo.h"

#include <lely/co/obj.h>
#include <lely/co/ssdo.h>

static co_ssdo_t *ssdo = NULL;

static uint32_t subDnInd = 0;
static uint32_t subUpInd = 0;

inline static co_unsigned32_t
co_sub_dn_ind_func(co_sub_t *sub, struct co_sdo_req *req, void *data)
{
	(void)data;

	++subDnInd;

	co_unsigned32_t ac = 0;
	const int result = co_sub_on_dn(sub, req, &ac);
	if ((result == -1) && (ac != 0)) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub_on_dn failed");
	}
	return ac;
}

inline static co_unsigned32_t
co_sub_up_ind_func(const co_sub_t *sub, struct co_sdo_req *req, void *data)
{
	(void)data;

	++subUpInd;

	co_unsigned32_t ac = 0;
	const int result = co_sub_on_up(sub, req, &ac);
	if ((result == -1) && (ac != 0)) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub_on_up failed");
	}
	return ac;
}

#endif // SDO_SERVER_H__
