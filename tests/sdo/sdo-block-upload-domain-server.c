/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "dcf/sdo-block-upload-domain-server.h"
#include "sdo-server.h"

static uint32_t SDO_BLK_IND_UP_DOMAIN_IND_COUNT = 0;
static uint32_t SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT = 0;

enum sdo_blk_up_test {
	SDO_UP_INIT,
	SDO_UP_TRANSFER,
	SDO_UP_FINI,
	SDO_UP_DONE,
};

static enum sdo_blk_up_test testState = SDO_UP_INIT;

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static uint32_t subUpIndDomain = 0;

static co_sub_t *sub2000_0 = NULL;
static const char *sdo_up_buf = SDO_BLK_VALUE;
static size_t sdo_up_nbyte = 7u;
static size_t sdo_up_offset = 0;

static co_unsigned32_t
co_sub2000_0_up_ind_dom(const co_sub_t *sub, struct co_sdo_req *req,
		co_unsigned32_t ac, void *data)
{
	(void)data;

	++subUpIndDomain;

	if (sub != sub2000_0)
		FAIL_TEST("server: received upload request for other object");

	if (ac != 0) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub2000_0_up_ind_dom received abort code");
		return ac;
	}

	switch (testState) {
	case SDO_UP_INIT:
		co_sdo_req_init(req, NULL);
		req->size = strlen(SDO_BLK_VALUE);
		testState = SDO_UP_TRANSFER;
		break;
	case SDO_UP_TRANSFER:
		req->buf = sdo_up_buf;
		req->offset = sdo_up_offset;
		req->nbyte = sdo_up_nbyte;

		sdo_up_buf += req->nbyte;
		sdo_up_offset += req->nbyte;

		if ((sdo_up_offset + sdo_up_nbyte) > req->size) {
			testState = SDO_UP_FINI;
			sdo_up_nbyte = req->size - sdo_up_offset;
		}
		break;
	case SDO_UP_FINI:
		req->buf = sdo_up_buf;
		req->offset = sdo_up_offset;
		req->nbyte = sdo_up_nbyte;
		testState = SDO_UP_DONE;
		break;
	default: FAIL_TEST("server: invalid test state");
	}

	return 0;
}

void
TestSetup(can_net_t *const net)
{
	SDO_BLK_IND_UP_DOMAIN_IND_COUNT = ((uint32_t)strlen(SDO_BLK_VALUE) / 7u)
			+ 2u; // upload initiate + last segment
	SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT =
			(((uint32_t)strlen(SDO_BLK_VALUE)) / 7) + 1u
			+ 2u; // initiate and end message

	dev = dcf_sdo_block_upload_domain_server_init();

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_ssdo_start(ssdo);

	sub2000_0 = co_dev_find_sub(dev, IDX, SUBIDX);
	co_sub_set_up_ind(sub2000_0, &co_sub2000_0_up_ind_dom, NULL);
}

void
TestTeardown(void)
{
	co_ssdo_stop(ssdo);
	co_ssdo_destroy(ssdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == SDO_BLK_MSG_UP_DOMAIN_SRV_COUNT) {
		if (subUpIndDomain != SDO_BLK_IND_UP_DOMAIN_IND_COUNT) {
			LOG_UINT32_EXPECTED("received indications",
					SDO_BLK_IND_UP_DOMAIN_IND_COUNT,
					subUpIndDomain);
			FAIL_TEST("server: incorrect received upload indication count");
		} else if (sentMsgCounter != SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT) {
			LOG_UINT32_EXPECTED("sent messages",
					SDO_BLK_MSG_UP_DOMAIN_CLI_COUNT,
					sentMsgCounter);
			FAIL_TEST("client: incorrect sent messages count");
		} else
			FINISH_TEST();
	}
}

void
TestStep(void)
{
	// noop
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
