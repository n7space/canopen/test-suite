/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/co/val.h>
#include <lely/util/error.h>

#include "dcf/sdo-block-download-client.h"
#include "sdo-client.h"

static bool reqSent = false;

static const co_integer64_t val = SDO_SEG_VAL;
static uint_least8_t buf[sizeof(co_integer64_t)] = { 0 };

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static void
co_csdo_dn_con_func_blk(co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, co_unsigned32_t ac, void *data)
{
	co_csdo_dn_con_func(sdo, idx, subidx, ac, data);

	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX)) {
		FailTestInvalidSdoCon(
				"client: SDO block download", idx, subidx);
	}
}

static void
co_csdo_ind_blk_dn(const co_csdo_t *sdo, co_unsigned16_t idx,
		co_unsigned8_t subidx, size_t size, size_t nbyte, void *data)
{
	if ((sdo != csdo) || (idx != IDX) || (subidx != SUBIDX)) {
		FailTestInvalidSdoInd(
				"client: SDO block download", idx, subidx);
	}

	co_csdo_ind_func(sdo, idx, subidx, size, nbyte, data);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sdo_block_download_client_init();

	csdo = co_csdo_create(net, dev, SDO_NUM);

	co_csdo_start(csdo);

	co_csdo_set_dn_ind(csdo, &co_csdo_ind_blk_dn, NULL);
}

void
TestTeardown(void)
{
	co_csdo_stop(csdo);
	co_csdo_destroy(csdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (dnConCounter == 1u) {
		if (msgCounter != SDO_BLK_MSG_DN_CLI_COUNT) {
			LOG_UINT32_EXPECTED("received messages",
					SDO_BLK_MSG_DN_CLI_COUNT, msgCounter);
			FAIL_TEST("client: incorrect received messages count");
		} else if (sentMsgCounter != SDO_BLK_MSG_DN_SRV_COUNT) {
			LOG_UINT32_EXPECTED("sent messages",
					SDO_BLK_MSG_DN_SRV_COUNT,
					sentMsgCounter);
			FAIL_TEST("client: incorrect sent messages count");
		} else if (indCounter != SDO_BLK_IND_CLI_COUNT) {
			LOG_UINT32_EXPECTED("indications",
					SDO_BLK_IND_CLI_COUNT, indCounter);
			FAIL_TEST("client: incorrect indications count");
		} else if (indBytes != sizeof(val)) {
			LOG_UINT64_EXPECTED(
					"sent bytes", sizeof(val), indBytes);
			FAIL_TEST("client: incorrect sent bytes count");
		} else
			FINISH_TEST();
	}
}

void
TestStep(void)
{
	if (!reqSent) {
		const size_t written_bytes = co_val_write(
				CO_DEFTYPE_INTEGER64, &val, buf, NULL);
		if (written_bytes != co_type_sizeof(CO_DEFTYPE_INTEGER64)) {
			LOG_UINT64_EXPECTED("written bytes",
					co_type_sizeof(CO_DEFTYPE_INTEGER64),
					written_bytes);
			FAIL_TEST("client: invalid written bytes count");
		}

		const int ret = co_csdo_blk_dn_req(csdo, IDX, SUBIDX, &val,
				sizeof(val), &co_csdo_dn_con_func_blk, NULL);

		if (ret != 0) {
			LOG_INT32("ret", ret);
			LOG_INT32("errc", get_errc());
			FAIL_TEST("client: failed to submit the request");
		}

		reqSent = true;
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}
