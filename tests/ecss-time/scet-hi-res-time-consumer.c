/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/scet-hi-res-time-consumer.h"
#include "ecss-time-rpdo.h"
#include "ecss-time-scet.h"
#include "ecss-time-sync.h"
#include "ecss-time-tpdo.h"

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec lastTime = { 0, 0 };
static struct timespec lastSyncTime = { 0, 0 };

static void
UpdateSpacecraftTime(const co_time_scet_t *const currentSCETTime)
{
	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	// set the local time to value received in Spacecraft Time PDO
	localSCETTime = *currentSCETTime;

	// adjust the local time by adding the time interval since the last
	// preceding SYNC object was successfully transmitted
	UpdateLocalSCETTime(timespec_diff_msec(&currentTime, &lastSyncTime));
}

// RPDO callbacks
static void
rpdo_ind_func_scet_rpdo(co_rpdo_t *pdo, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	rpdo_ind_func(pdo, ac, ptr, n, data);

	const co_time_scet_t *readValueSCET = co_dev_get_val(dev, IDX_SET, 0);

	UpdateSpacecraftTime(readValueSCET);

	if (syncCounter == ECSS_TIME_SYNC_NUMBER) {
		if (sentMsgCounter != ECSS_TIME_CONSUMER_SENT_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					ECSS_TIME_CONSUMER_SENT_NUMBER,
					sentMsgCounter);
			FAIL_TEST("consumer: incorrect sent messages count");
		}
		// values from the last PDO won't be actuated as it
		// would happen on the next received SYNC message
		else if (rpdoCounter != (ECSS_TIME_PDO_NUMBER - 1u)) {
			LOG_UINT32_EXPECTED("received PDO messages",
					ECSS_TIME_PDO_NUMBER - 1u, rpdoCounter);
			FAIL_TEST("consumer: incorrect received PDO messages count");
		} else if (rpdoCounter != msgCounter) {
			LOG_UINT32_EXPECTED("received PDO vs all messages",
					rpdoCounter, msgCounter);
			FAIL_TEST("consumer: received non-PDO messages");
		} else if (tpdoCounter != ECSS_TIME_LOCAL_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("sent PDO messages",
					ECSS_TIME_LOCAL_PDO_NUMBER,
					tpdoCounter);
			FAIL_TEST("consumer: incorrect sent PDO messages count");

		} else {
			FINISH_TEST();
		}
	}
}

// SYNC callbacks
static void
sync_ind_func_scet_prod(co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	assert(sync == sync_);

	sync_ind_func(sync, cnt, data);

	if (cnt != 0)
		FAIL_TEST("consumer: SYNC counter should be 0");

	// save last preceding SYNC object time
	GetCurrentTestTime(&lastSyncTime);

	// first call will not trigger RPDO callback (no data to actuate)
	REQUIRE(co_rpdo_sync(rpdo, 0));

	// set the Local SCET Get object as it shall correspond to the local
	// time when the last preceding SYNC object was successfully received.
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &localSCETTime,
				     sizeof(localSCETTime)),
			sizeof(localSCETTime));
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_scet_hi_res_time_consumer_init();

	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func, NULL);

	rpdo = co_rpdo_create(net, dev, PDO_NUM);
	co_rpdo_set_ind(rpdo, rpdo_ind_func_scet_rpdo, NULL);
	co_rpdo_set_err(rpdo, rpdo_err_func, NULL);

	sync = co_sync_create(net, dev);
	co_sync_set_ind(sync, sync_ind_func_scet_prod, NULL);
	co_sync_set_err(sync, sync_err_func, NULL);

	co_tpdo_start(tpdo);
	co_rpdo_start(rpdo);
	co_sync_start(sync);

	const co_time_scet_t scet = CO_TIME_SCET_INIT;
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_SET, 0, &scet, sizeof(scet)),
			sizeof(scet));
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &scet, sizeof(scet)),
			sizeof(scet));
}

void
TestTeardown(void)
{
	co_tpdo_stop(tpdo);
	co_rpdo_stop(rpdo);
	co_sync_stop(sync);
	co_tpdo_destroy(tpdo);
	co_rpdo_destroy(rpdo);
	co_sync_destroy(sync);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter > ECSS_TIME_PDO_NUMBER)
		FAIL_TEST("consumer: too many PDO messages received");
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	const int64_t deltaMs = timespec_diff_msec(&currentTime, &lastTime);

	if (deltaMs == 0)
		return;

	UpdateLocalSCETTime(deltaMs);

	lastTime = currentTime;
}
