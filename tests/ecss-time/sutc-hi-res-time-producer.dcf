# This file is part of the Test Suite test cases.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Suite was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

[DeviceInfo]
VendorName=N7 Space Sp. z o.o.
VendorNumber=0x004E3753
BaudRate_10=1
BaudRate_20=1
BaudRate_50=1
BaudRate_125=1
BaudRate_250=1
BaudRate_500=1
BaudRate_800=1
BaudRate_1000=1

[DeviceComissioning]
NodeID=0x01

[MandatoryObjects]
SupportedObjects=3
1=0x1000
2=0x1001
3=0x1018

[OptionalObjects]
SupportedObjects=4
1=0x1005
2=0x1019
3=0x1800
4=0x1A00

[ManufacturerObjects]
SupportedObjects=2
1=0x2000
2=0x2001

[1000]
ParameterName=Device type
DataType=0x0007
AccessType=ro

[1001]
ParameterName=Error register
DataType=0x0005
AccessType=ro

[1005]
ParameterName=COB-ID SYNC
DataType=0x0007
AccessType=rw
DefaultValue=0x00000080

[1018]
SubNumber=5
ParameterName=Identity object
ObjectType=0x09

[1018sub0]
ParameterName=Highest sub-index supported
DataType=0x0005
AccessType=const
DefaultValue=0x04

[1018sub1]
ParameterName=Vendor-ID
DataType=0x0007
AccessType=ro
DefaultValue=0x004E3753

[1018sub2]
ParameterName=Product code
DataType=0x0007
AccessType=ro

[1018sub3]
ParameterName=Revision number
DataType=0x0007
AccessType=ro

[1018sub4]
ParameterName=Serial number
DataType=0x0007
AccessType=ro

[1019]
ParameterName=Synchronous counter overflow value
DataType=0x0005
AccessType=rw
DefaultValue=0

[1800]
SubNumber=3
ParameterName=TPDO communication parameter (Spacecraft Time)
ObjectType=0x09

[1800sub0]
ParameterName=Highest sub-index supported
DataType=0x0005
AccessType=const
DefaultValue=0x02

[1800sub1]
ParameterName=COB-ID used by TPDO
DataType=0x0007
AccessType=rw
DefaultValue=$NODEID+0x180

[1800sub2]
ParameterName=Transmission type
DataType=0x0005
AccessType=rw
DefaultValue=0x01

[1A00]
ParameterName=TPDO mapping parameter (Spacecraft Time)
ObjectType=0x09
DataType=0x0007
AccessType=rw
CompactSubObj=1

[1A00Value]
NrOfEntries=1
1=0x20000040

[2000]
ParameterName=Local UTC Get
DataType=0x0061
AccessType=ro
PDOMapping=1

[2001]
ParameterName=Local UTC Set
DataType=0x0061
AccessType=wo
