/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/sutc-time-producer.h"
#include "ecss-time-sdo.h"
#include "ecss-time-sutc.h"
#include "ecss-time-tpdo.h"

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static struct timespec lastTime = { 0, 0 };

static co_sub_t *subLocalSUTCSet = NULL;

// TPDO callbacks
static void
tpdo_ind_func_sutc_tpdo(co_tpdo_t *pdo, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	tpdo_ind_func(pdo, ac, ptr, n, data);

	if (tpdoCounter == ECSS_TIME_PDO_NUMBER) {
		if (sentMsgCounter != ECSS_TIME_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					ECSS_TIME_PDO_NUMBER, sentMsgCounter);
			FAIL_TEST("producer: incorrect sent messages count");
		} else if (msgCounter != ECSS_TIME_LOCAL_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("total received messages",
					msgCounter, ECSS_TIME_LOCAL_PDO_NUMBER);
			FAIL_TEST("producer: incorrect total received messages count");
		} else {
			FINISH_TEST();
		}
	}
}

// SDO callbacks
static inline co_unsigned32_t
co_sub_local_sutc_set_dn_ind(co_sub_t *sub, struct co_sdo_req *req,
		co_unsigned32_t ac, void *data)
{
	(void)data;

	if (sub != subLocalSUTCSet)
		FAIL_TEST("producer: received upload request for other object");

	if (ac != 0) {
		LOG_UINT32("ac", ac);
		FAIL_TEST("co_sub_local_sutc_set_dn_ind received abort code");
		return ac;
	}

	REQUIRE(co_sub_on_dn(sub, req, &ac));

	// set the Spacecraft Time value
	if ((ac == 0) && (co_sdo_req_last(req) != 0)) {
		localSUTCTime = *((const co_time_sutc_t *)co_dev_get_val(
				dev, IDX_SET, 0));
	}

	return ac;
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sutc_time_producer_init();

	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func_sutc_tpdo, NULL);

	ssdo = co_ssdo_create(net, dev, SDO_NUM);

	co_tpdo_start(tpdo);
	co_ssdo_start(ssdo);

	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &localSUTCTime,
				     sizeof(localSUTCTime)),
			sizeof(localSUTCTime));

	subLocalSUTCSet = co_dev_find_sub(dev, IDX_SET, 0);
	co_sub_set_dn_ind(subLocalSUTCSet, &co_sub_local_sutc_set_dn_ind, NULL);
}

void
TestTeardown(void)
{
	co_tpdo_stop(tpdo);
	co_tpdo_destroy(tpdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter > ECSS_TIME_LOCAL_PDO_NUMBER)
		FAIL_TEST("producer: too many messages received.");
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	const int64_t deltaMs = timespec_diff_msec(&currentTime, &lastTime);

	if (deltaMs == 0)
		return;

	UpdateLocalSUTCTime(deltaMs);

	// set the Local SUTC Get object
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &localSUTCTime,
				     sizeof(localSUTCTime)),
			sizeof(localSUTCTime));

	lastTime = currentTime;
}
