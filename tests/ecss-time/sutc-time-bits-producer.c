/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/sutc-time-bits-producer.h"
#include "ecss-time-rpdo.h"
#include "ecss-time-sutc.h"
#include "ecss-time-tpdo.h"

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;
static struct timespec lastTime = { 0, 0 };

// TPDO callbacks
static void
tpdo_ind_func_sutc_tpdo(co_tpdo_t *pdo, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	tpdo_ind_func(pdo, ac, ptr, n, data);

	if (tpdoCounter == ECSS_TIME_PDO_NUMBER) {
		if (sentMsgCounter != ECSS_TIME_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					ECSS_TIME_PDO_NUMBER, sentMsgCounter);
			FAIL_TEST("producer: incorrect sent messages count");
		} else if (msgCounter != ECSS_TIME_LOCAL_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("total received messages",
					msgCounter, ECSS_TIME_LOCAL_PDO_NUMBER);
			FAIL_TEST("producer: incorrect total received messages count");
		} else if (rpdoCounter != ECSS_TIME_LOCAL_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("received RPDO indications",
					msgCounter, ECSS_TIME_LOCAL_PDO_NUMBER);
			FAIL_TEST("producer: incorrect RPDO indications count");
		} else {
			FINISH_TEST();
		}
	}
}

// RPDO callbacks
static void
rpdo_ind_func_sutc_rpdo(co_rpdo_t *pdo, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	rpdo_ind_func(pdo, ac, ptr, n, data);

	const co_time_sutc_t *readValueSUTC = co_dev_get_val(dev, IDX_LOCAL, 0);

	if (readValueSUTC->usec != 0)
		FAIL_TEST("producer: unused Submiliseconds bits are not zeroed");
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sutc_time_bits_producer_init();

	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func_sutc_tpdo, NULL);

	rpdo = co_rpdo_create(net, dev, PDO_NUM);
	co_rpdo_set_ind(rpdo, rpdo_ind_func_sutc_rpdo, NULL);
	co_rpdo_set_err(rpdo, rpdo_err_func, NULL);

	co_tpdo_start(tpdo);
	co_rpdo_start(rpdo);

	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &localSUTCTime,
				     sizeof(localSUTCTime)),
			sizeof(localSUTCTime));
}

void
TestTeardown(void)
{
	co_rpdo_stop(rpdo);
	co_tpdo_stop(tpdo);
	co_rpdo_destroy(rpdo);
	co_tpdo_destroy(tpdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter > ECSS_TIME_LOCAL_PDO_NUMBER)
		FAIL_TEST("producer: too many messages received.");
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	const int64_t deltaUSec = timespec_diff_usec(&currentTime, &lastTime);

	if (deltaUSec == 0)
		return;

	UpdateLocalSUTCTimeUSec(deltaUSec);

	// set the Local SUTC Get object
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &localSUTCTime,
				     sizeof(localSUTCTime)),
			sizeof(localSUTCTime));

	lastTime = currentTime;
}
