/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/TestTime.h>

#include "dcf/sutc-time-bits-consumer.h"
#include "ecss-time-rpdo.h"
#include "ecss-time-sutc.h"
#include "ecss-time-tpdo.h"

static uint32_t msgCounter = 0;
static uint32_t sentMsgCounter = 0;

static struct timespec lastTime = { 0, 0 };

static void
UpdateSpacecraftTime(const co_time_sutc_t *const currentSUTCTime)
{
	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	// set the local time to value received in Spacecraft Time PDO - simple,
	// but inaccurate time synchronization method - just as an example.
	localSUTCTime = *currentSUTCTime;
	localSUTCTime.usec = 0; // unused
}

// RPDO callbacks
static void
rpdo_ind_func_sutc_rpdo(co_rpdo_t *pdo, co_unsigned32_t ac, const void *ptr,
		size_t n, void *data)
{
	rpdo_ind_func(pdo, ac, ptr, n, data);

	const co_time_sutc_t *readValueSUTC = co_dev_get_val(dev, IDX_SET, 0);
	UpdateSpacecraftTime(readValueSUTC);
}

void
TestSetup(can_net_t *const net)
{
	dev = dcf_sutc_time_bits_consumer_init();

	tpdo = co_tpdo_create(net, dev, PDO_NUM);
	co_tpdo_set_ind(tpdo, tpdo_ind_func, NULL);

	rpdo = co_rpdo_create(net, dev, PDO_NUM);
	co_rpdo_set_ind(rpdo, rpdo_ind_func_sutc_rpdo, NULL);
	co_rpdo_set_err(rpdo, rpdo_err_func, NULL);

	co_tpdo_start(tpdo);
	co_rpdo_start(rpdo);

	const co_time_sutc_t sutc = CO_TIME_SUTC_INIT;
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_SET, 0, &sutc, sizeof(sutc)),
			sizeof(sutc));
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &sutc, sizeof(sutc)),
			sizeof(sutc));
}

void
TestTeardown(void)
{
	co_tpdo_stop(tpdo);
	co_rpdo_stop(rpdo);
	co_tpdo_destroy(tpdo);
	co_rpdo_destroy(rpdo);
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;

	++msgCounter;

	if (msgCounter == ECSS_TIME_PDO_NUMBER) {
		if (sentMsgCounter != ECSS_TIME_LOCAL_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("sent messages",
					ECSS_TIME_LOCAL_PDO_NUMBER,
					sentMsgCounter);
			FAIL_TEST("consumer: incorrect sent messages count");
		} else if (rpdoCounter != ECSS_TIME_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("received PDO vs all messages",
					rpdoCounter, msgCounter);
			FAIL_TEST("consumer: received non-PDO messages");
		} else if (tpdoCounter != ECSS_TIME_LOCAL_PDO_NUMBER) {
			LOG_UINT32_EXPECTED("sent PDO messages",
					ECSS_TIME_LOCAL_PDO_NUMBER,
					tpdoCounter);
			FAIL_TEST("consumer: incorrect sent PDO messages count");
		} else {
			FINISH_TEST();
		}
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;

	++sentMsgCounter;
}

void
TestStep(void)
{
	struct timespec currentTime = { 0, 0 };
	GetCurrentTestTime(&currentTime);

	const int64_t deltaMs = timespec_diff_msec(&currentTime, &lastTime);

	if (deltaMs == 0)
		return;

	UpdateLocalSUTCTime(deltaMs);

	// set the unused least significant bits of 'usec' to zero
	const co_time_sutc_t localTimePDO = {
		.days = localSUTCTime.days,
		.ms = localSUTCTime.ms,
		.usec = 0 // unused
	};

	// set the Local UTC Get object
	REQUIRE_SIZE(co_dev_set_val(dev, IDX_GET, 0, &localTimePDO,
				     sizeof(localTimePDO)),
			sizeof(localTimePDO));

	lastTime = currentTime;
}
