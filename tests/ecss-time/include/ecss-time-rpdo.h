/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_ECSS_TIME_RPDO_H__
#define CTSSW_ECSS_TIME_RPDO_H__

#include "ecss-time.h"

#include <lely/co/rpdo.h>
#include <lely/co/sdo.h>

static co_rpdo_t *rpdo = NULL;

static uint32_t rpdoCounter = 0;

// RPDO callbacks
static void
rpdo_ind_func(co_rpdo_t *pdo, co_unsigned32_t ac, const void *ptr, size_t n,
		void *data)
{
	(void)pdo;
	(void)ptr;
	(void)n;
	(void)data;

	if (ac == 0) {
		++rpdoCounter;
	} else {
		LOG_STR("rpdo_ind_func() error", co_sdo_ac2str(ac));
		FAIL_TEST("rpdo_ind_func() called with an error");
	}
}

static void
rpdo_err_func(co_rpdo_t *pdo, co_unsigned16_t eec, co_unsigned8_t er,
		void *data)
{
	(void)pdo;
	(void)data;

	LOG_UINT32("eec", eec);
	LOG_UINT32("er", er);
	FAIL_TEST("rpdo_err_func() called");
}

#endif // CTSSW_ECSS_TIME_RPDO_H__
