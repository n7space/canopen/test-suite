/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_ECSS_TIME_H__
#define CTSSW_ECSS_TIME_H__

#include <lely/co/dev.h>
#include <lely/co/type.h>

#include <TestFramework/TestHarness.h>

static co_dev_t *dev = NULL;

static const co_unsigned16_t PDO_NUM = 0x0001u;

static const co_unsigned16_t IDX_GET = 0x2000u;
static const co_unsigned16_t IDX_SET = 0x2001u;
static const co_unsigned16_t IDX_LOCAL = 0x2002u;

static const uint32_t ECSS_TIME_PDO_NUMBER = 10u;
static const uint32_t ECSS_TIME_LOCAL_PDO_NUMBER = 2u;
static const uint32_t ECSS_TIME_SYNC_NUMBER = ECSS_TIME_PDO_NUMBER;

static const uint32_t ECSS_TIME_CONSUMER_SENT_NUMBER =
		ECSS_TIME_SYNC_NUMBER + ECSS_TIME_LOCAL_PDO_NUMBER;
static const uint32_t ECSS_TIME_PRODUCER_RECV_NUMBER =
		ECSS_TIME_CONSUMER_SENT_NUMBER;

#endif // CTSSW_ECSS_TIME_H__
