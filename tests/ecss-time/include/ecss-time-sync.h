/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_ECSS_TIME_SYNC_H__
#define CTSSW_ECSS_TIME_SYNC_H__

#include "ecss-time.h"

#include <lely/co/sync.h>

static co_sync_t *sync = NULL;

// SYNC default callbacks
static uint32_t syncCounter = 0;

static void
sync_ind_func(co_sync_t *sync_, co_unsigned8_t cnt, void *data)
{
	(void)data;

	++syncCounter;

	assert(sync == sync_);

	if (cnt != 0)
		FAIL_TEST("consumer: SYNC counter should be 0");
}

static void
sync_err_func(co_sync_t *sync_, co_unsigned16_t eec, co_unsigned8_t er,
		void *data)
{
	(void)data;

	assert(sync == sync_);

	LOG_UINT32("eec", eec);
	LOG_UINT32("er", er);
	FAIL_TEST("sync_err_func called");
}

#endif // CTSSW_ECSS_TIME_SYNC_H__
