/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_ECSS_TIME_SUTC_H__
#define CTSSW_ECSS_TIME_SUTC_H__

#include "ecss-time.h"

#include <lely/co/val.h>

static co_time_sutc_t localSUTCTime = CO_TIME_SUTC_INIT;

#define MS_PER_DAY 86400000u
#define SUBMILISECONDS_PER_MS 1000u

static inline void
UpdateLocalSUTCTime(int_least64_t deltaMs)
{
	co_unsigned16_t deltaDays = 0;

	if (deltaMs < 0) {
		FAIL_TEST("UpdateLocalSUTCTime(): time delta must be a positive number");
	} else if (deltaMs != 0) {
		if ((deltaMs + localSUTCTime.ms) > MS_PER_DAY) {
			deltaDays = (co_unsigned16_t)((deltaMs + localSUTCTime.ms)
					/ MS_PER_DAY);
			deltaMs = (co_unsigned32_t)((deltaMs + localSUTCTime.ms)
					% MS_PER_DAY);
		}

		localSUTCTime.ms =
				(co_unsigned32_t)(localSUTCTime.ms + deltaMs);

		localSUTCTime.days = (co_unsigned16_t)(localSUTCTime.days
				+ deltaDays);
	}
}

static inline void
UpdateLocalSUTCTimeUSec(int_least64_t deltaUSec)
{
	co_unsigned32_t deltaMs = 0;

	if (deltaUSec < 0) {
		FAIL_TEST("UpdateLocalSUTCTimeUSec(): time delta must be a positive number");
	} else if (deltaUSec != 0) {
		if ((deltaUSec + localSUTCTime.usec) > SUBMILISECONDS_PER_MS) {
			deltaMs = (co_unsigned32_t)((deltaUSec + localSUTCTime.usec)
					/ SUBMILISECONDS_PER_MS);
			deltaUSec = (co_unsigned16_t)((deltaUSec + localSUTCTime.usec)
					% SUBMILISECONDS_PER_MS);
		}

		localSUTCTime.usec = (co_unsigned16_t)(localSUTCTime.usec
				+ deltaUSec);

		UpdateLocalSUTCTime(deltaMs);
	}
}

#endif // CTSSW_ECSS_TIME_SUTC_H__
