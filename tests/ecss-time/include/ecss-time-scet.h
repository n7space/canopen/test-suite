/**@file
 * This file is part of the Test Suite test cases.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTSSW_ECSS_TIME_SCET_H__
#define CTSSW_ECSS_TIME_SCET_H__

#include "ecss-time.h"

#include <lely/co/val.h>

static co_time_scet_t localSCETTime = CO_TIME_SCET_INIT;

#define SUBSECONDS_PER_SECOND 1000u

static inline void
UpdateLocalSCETTime(const int_least64_t deltaMs)
{
	if (deltaMs < 0) {
		FAIL_TEST("updateLocalSCETTime(): time delta must be a positive number");
	} else if (deltaMs != 0) {
		if ((deltaMs + localSCETTime.subseconds)
				> SUBSECONDS_PER_SECOND) {
			localSCETTime.seconds +=
					(co_unsigned32_t)((deltaMs + localSCETTime.subseconds)
							/ SUBSECONDS_PER_SECOND);
			localSCETTime.subseconds =
					(co_unsigned24_t)((deltaMs + localSCETTime.subseconds)
							% SUBSECONDS_PER_SECOND);
		} else {
			localSCETTime.subseconds += (co_unsigned24_t)deltaMs;
		}
	}
}

#endif // CTSSW_ECSS_TIME_SCET_H__
