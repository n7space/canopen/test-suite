/**@file
 * This file is part of the Test Suite CI configuration.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Suite was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@Library("n7s-jenkins-lib@2.5.2") _

pipeline {
    agent {
        label 'docker'
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: "10", daysToKeepStr: "14"))

        timeout(time: 3, unit: "HOURS")

        timestamps()
    }

    parameters {
        booleanParam (
            name: "cleanBuild",
            defaultValue: false,
            description: "Ensures clean rebuild",
        )
        booleanParam (
            name: "resetSvf",
            defaultValue: false,
            description: "Forces SVF reset/reboot before executing tests",
        )
        choice (
            name: "selectedTarget",
            choices: ["All", "V71", "V71_XC32", "RH71", "RH71_XC32", "RH707", "RH707_XC32"],
            description: "Selects target(s) for the pipeline",
        )
        string (
            name: "sconsOptions",
            defaultValue: "",
            description: "Additional custom SCons options",
        )
    }

    triggers {
        cron(
            env.BRANCH_NAME.equals('master')
            ? 'H 22 * * *'
            : '') // hack for rebuilding only master
    }

    environment {
        SCRIPTORIUM_IMAGE_URL = "gitlab-registry.n7space.com/docs-gens/scriptorium:v1.16.1"
        JOB_IMAGE_REGISTRY = "${env.JOB_NAME.toLowerCase()}"
        SHAREPOINT_LOGIN = credentials('jenkins-n7s-sharepoint')

        CORE_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:core"
        CM7_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:cortex-m7"
        XC32_BASE_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32"
        XC32_V71_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32-v71"
        XC32_RH71_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32-rh71"
        XC32_RH707_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32-rh707"
    }

    stages {
        stage("Clean") {
            when {
                anyOf {
                    buildingTag()
                    triggeredBy "TimerTrigger"
                    equals expected: true, actual: params.cleanBuild
                }
            }

            steps {
                cleanRepository()
            }
        }

        stage("Docker - Core") {
            agent {
                dockerfile {
                    dir "environment/docker/core"
                    additionalBuildArgs "-t ${CORE_IMAGE_NAME}"
                    reuseNode true
                }
            }

            steps {
                echo "Core image built - to be used by next steps"
            }
        }

        stage("Docker - Cortex-M7") {
            agent {
                dockerfile {
                    dir "environment/docker/cortex-m7"
                    additionalBuildArgs "-t ${CM7_IMAGE_NAME} --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                    reuseNode true
                }
            }

            steps {
                echo "Cortex-M7 image built - to be used by next steps"
            }
        }

        stage("Docker - XC32 base") {
            when {
                anyOf {
                    expression { params.selectedTarget == "All" }
                    expression { params.selectedTarget.endsWith("XC32") }
                }
            }

            agent {
                dockerfile {
                    dir "environment/docker/xc32/base"
                    additionalBuildArgs "-t ${XC32_BASE_IMAGE_NAME} --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                    reuseNode true
                }
            }

            steps {
                echo "XC32 image built - to be used by next steps"
            }
        }

        stage("Docker - XC32") {
            when {
                anyOf {
                    expression { params.selectedTarget == "All" }
                    expression { params.selectedTarget.endsWith("XC32") }
                }
            }

            parallel {
                stage("V71") {
                    when {
                        anyOf {
                            expression { params.selectedTarget == "All" }
                            expression { params.selectedTarget == "V71_XC32" }
                        }
                    }

                    agent {
                        dockerfile {
                            dir "environment/docker/xc32"
                            additionalBuildArgs "-t ${XC32_V71_IMAGE_NAME} --build-arg PROCESSOR=ATSAMV71Q21RT --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                            reuseNode true
                        }
                    }

                    steps {
                        echo "XC32 SAMV71 image built - to be used by next steps"
                    }
                }

                stage("RH71") {
                    when {
                        anyOf {
                            expression { params.selectedTarget == "All" }
                            expression { params.selectedTarget == "RH71_XC32" }
                        }
                    }

                    agent {
                        dockerfile {
                            dir "environment/docker/xc32"
                            additionalBuildArgs "-t ${XC32_RH71_IMAGE_NAME} --build-arg PROCESSOR=ATSAMRH71F20C --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                            reuseNode true
                        }
                    }

                    steps {
                        echo "XC32 SAMRH71 image built - to be used by next steps"
                    }
                }

                stage("RH707") {
                    when {
                        anyOf {
                            expression { params.selectedTarget == "All" }
                            expression { params.selectedTarget == "RH707_XC32" }
                        }
                    }

                    agent {
                        dockerfile {
                            dir "environment/docker/xc32"
                            additionalBuildArgs "-t ${XC32_RH707_IMAGE_NAME} --build-arg PROCESSOR=ATSAMRH707F18A --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                            reuseNode true
                        }
                    }

                    steps {
                        echo "XC32 SAMRH707 image built - to be used by next steps"
                    }
                }
            }
        }

        stage("Prerequisites") {
            parallel {
                stage("clang-format check") {
                    agent {
                        docker {
                            image env.CORE_IMAGE_NAME
                            reuseNode true
                        }
                    }

                    steps {
                        sh '''
                           clang-format -n --Werror \
                                 $(find tests -name *.c -o -name *.h) \
                                 $(find tests -name *.cpp -o -name *.hpp)
                           '''
                    }
                }
                stage("Python code checks") {
                    agent {
                        docker {
                            image env.CORE_IMAGE_NAME
                            reuseNode true
                        }
                    }

                    steps {
                        sh 'black --diff --check $(find . -name SCons\\*)'
                        sh 'flake8 $(find . -name SCons\\*)'
                    }
                }

                stage("Sonar setup") {
                    agent {
                        docker {
                            image env.CORE_IMAGE_NAME
                            reuseNode true
                        }
                    }

                    steps {
                        withSonarQubeEnv("SonarQube N7S") {
                            sonar action: "downloadScanner",
                                force: params.cleanBuild,
                                sonarUrl: env.SONAR_HOST_URL
                        }
                    }
                }
            }
        }

        stage("CANDP SRS") {
            agent {
                docker {
                    image env.SCRIPTORIUM_IMAGE_URL
                    args "--entrypoint="
                    reuseNode true
                }
            }

            steps {
                sh "mkdir -p artifacts"

                scriptoriumArtifact(
                    "CANDP-SRS.xlsx",
                    "download-from-sharepoint",
                    "--sharepoint ${N7S_SHAREPOINT}" as String,
                    '--user $SHAREPOINT_LOGIN_USR',
                    '--password $SHAREPOINT_LOGIN_PSW',
                    "\"CANopen9/Shared Documents/02 Deliverables/CANDP/SRS/CAN-N7S-CANDP-SRS-A Requirements.xlsx\"",
                )

                scriptorium(
                    "check-srs",
                    "--input artifacts/CANDP-SRS.xlsx", // new one
                )

                 scriptorium(
                    "check-srs",
                    "--input candp-srs.json", // old one
                )

                scriptoriumArtifact(
                    "srs-current.json",
                    "extract-srs",
                    "--input artifacts/CANDP-SRS.xlsx",
                )

                scriptorium(
                    "srs-changelog",
                    "candp-srs.json",
                    "artifacts/srs-current.json",
                    "> artifacts/srs-changelog.txt",
                )
                archiveArtifacts "artifacts/srs-changelog.txt"

                scriptoriumArtifact(
                    "CANDP-SRS-RB-to-TS-trace.html",
                    "generate-reqs-trace-matrix",
                    "candp-sss.json",
                    "artifacts/srs-current.json",
                )

                scriptoriumArtifact(
                    "CANDP-SDD-Forward-Trace.html",
                    "generate-sdd-forward",
                    "artifacts/srs-current.json",
                    "candp-sdd.json",
                )

                scriptoriumArtifact(
                    "CANDP-SDD-Backward-Trace.html",
                    "generate-sdd-backward",
                    "candp-sdd.json",
                    "artifacts/srs-current.json",
                )
            }
        }

        stage("Building") {
            matrix {
                axes {
                    axis {
                        name "N7S_CI_TARGET"
                        values "V71", "V71_XC32", "RH71", "RH71_XC32", "RH707", "RH707_XC32"
                    }
                }

                when {
                    anyOf {
                        expression { params.selectedTarget == "All" }
                        expression { params.selectedTarget == env.N7S_CI_TARGET }
                    }
                }

                agent {
                    docker {
                        image getDockerImageNameForTarget(env.N7S_CI_TARGET)
                        reuseNode true
                    }
                }

                environment {
                    N7S_CI_PLATFORM = getCurrentPlatformName()
                }

                stages {
                    stage("Build dependencies") {
                        steps {
                            debugBuild("LibCANopen libs", parallelBuildFlag())
                            releaseBuild("LibCANopen libs", parallelBuildFlag())
                        }
                    }

                    stage("Build test binaries") {
                        steps {
                            debugBuild("test-cases-apps unit-tests-apps", parallelBuildFlag())
                            releaseBuild("test-cases-apps unit-tests-apps", parallelBuildFlag())

                            copyCompilationDatabaseForSonar()
                        }

                        post {
                            always {
                                archiveDirAsZip("install_root_${N7S_CI_PLATFORM}.zip", "${getCurrentPlatformBuildDir()}/release/${N7S_CI_PLATFORM}/install_root")
                            }
                        }
                    }

                    stage("Tests specifications") {
                        steps {
                            releaseBuild("traces")
                            sh "cp '${getCurrentPlatformBuildDir()}/release/integration-tests-specs.json' 'artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json'"
                            archiveArtifacts artifacts: "artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json"
                        }
                    }

                    stage("Doxygen") {
                        steps {
                            releaseBuild("LibCANopen-hwtb-doc")
                            publishHtmlReport("${getCurrentPlatformBuildDir()}/release/${N7S_CI_PLATFORM}/environment/resources/LibCANopen/lely-core/doc/html",
                                              "CANSW Doxygen")

                            archiveDirAsZip("doxygen-xml-${N7S_CI_PLATFORM}.zip",
                                            "${getCurrentPlatformBuildDir()}/release/${N7S_CI_PLATFORM}/resources/LibCANopen/lely-core/doc/xml")
                        }
                    }
                }
            }
        }

        stage("Sonar") {
            agent {
                docker {
                    image env.CORE_IMAGE_NAME
                    reuseNode true
                }
            }

            environment {
                SONAR_ORGANIZATION = "n7space"
                SONAR_CTSSW_KEY = "canopen_test-suite"
                SONAR_CANSW_KEY = "n7space_lely-core"
            }

            steps {
                withSonarQubeEnv("SonarQube N7S") {
                    sonar action: "scan",
                        args: ("-Dsonar.projectKey=${SONAR_CTSSW_KEY}"
                               + " -Dsonar.cfamily.variants.names=" + getSonarVariants()
                               + " -Dsonar.projectVersion=\$(git describe --exact-match --tags || echo '')"
                               + " --debug"
                    )

                    gatherCtsswSonarLogs()
                }

                withSonarQubeEnv("SonarCloud") {
                    gatherCanswSonarLogs()
                }
            }
        }

        stage("Testing") {
            matrix {
                axes {
                    axis {
                        name "N7S_CI_TARGET"
                        values "V71", "V71_XC32", "RH71", "RH71_XC32", "RH707", "RH707_XC32"
                    }
                }

                when {
                    anyOf {
                        expression { params.selectedTarget == "All" }
                        expression { params.selectedTarget == env.N7S_CI_TARGET }
                    }
                }

                environment {
                    N7S_CI_PLATFORM = getCurrentPlatformName()
                }

                stages {
                    stage("SVF") {
                        agent {
                            docker {
                                image getDockerImageNameForTarget(env.N7S_CI_TARGET)
                                reuseNode true
                            }
                        }

                        stages {
                            stage("Reset SVF") {
                                when {
                                    anyOf {
                                        buildingTag()
                                        triggeredBy "TimerTrigger"
                                        equals expected: true, actual: params.resetSvf
                                    }
                                }

                                steps {
                                    lock(resource: null,
                                         label: getSvfLabelForTarget(env.N7S_CI_TARGET),
                                         variable: "SELECTED_SVF",
                                         quantity: 1) {
                                        sh "PYTHONPATH=./site_scons/site_tools python3 ./resources/n7-core/environment/ci/reset-svf.py " + getCurrentPlatformSvf()
                                        sleep(time: 40, unit: "SECONDS")
                                    }
                                }
                            }

                            stage("Test Suite") {
                                steps {
                                    lock(resource: null,
                                         label: getSvfLabelForTarget(env.N7S_CI_TARGET),
                                         variable: "SELECTED_SVF",
                                         quantity: 1) {
                                        releaseBuild("tests")
                                    }
                                }
                                post {
                                    always {
                                        sh "cp '${getCurrentPlatformBuildDir()}/release/tests/report.json' 'artifacts/integration-tests-results-${N7S_CI_PLATFORM}.json'"
                                        archiveArtifacts artifacts: "artifacts/integration-tests-results-${N7S_CI_PLATFORM}.json"
                                        zip zipFile: "artifacts/integration-tests-logs-release-${N7S_CI_PLATFORM}.zip",
                                            archive: true, dir: getCurrentPlatformBuildDir() + "/release/tests",
                                            glob: "**/*.log",
                                            overwrite: true
                                    }
                                }
                            }

                            stage("dcf2dev Coverage") {
                                when {
                                    // XC32 doesn't support coverage (old GCC version)
                                    expression { !env.N7S_CI_TARGET.endsWith("XC32") }
                                }

                                steps {
                                    coverageBuild("dcf2dev-tests")
                                    publishCoverageReport()
                                }
                            }
                        }
                    }

                    stage("MPLAB") {
                        agent {
                            dockerfile {
                                dir 'environment/docker/core'
                                label 'docker'
                                reuseNode true
                            }
                        }

                        steps {
                            generateMplabProjectsForPlatform(getCurrentPlatformName(), getCurrentPlatformBuildDir())
                        }
                    }

                    stage("Scriptorium") {
                        agent {
                            docker {
                                image env.SCRIPTORIUM_IMAGE_URL
                                args "--entrypoint="
                                reuseNode true
                            }
                        }

                        environment {
                            SCRIPTORIUM_PROJECT_NAME = "CANopen Library Toolset - CANSW (${env.N7S_CI_PLATFORM.toUpperCase()})"
                        }

                        stages {
                            stage("Specifications") {
                                steps {
                                    scriptoriumArtifact(
                                        "unit-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "extract-cpputests-specs",
                                        "--input-filter test-*.xml",
                                        "${getCurrentPlatformBuildDir()}/release/${N7S_CI_PLATFORM}/resources/LibCANopen/lely-core/doc/xml" as String,
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SUITP-unit-tests-${N7S_CI_PLATFORM}.html",
                                        "generate-suitp",
                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Unit Tests'" as String,
                                        "artifacts/unit-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/srs-current.json",
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SUITP-integration-tests-${N7S_CI_PLATFORM}.html",
                                        "generate-suitp",
                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Integration Tests'" as String,
                                        "artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/srs-current.json",
                                    )

/* TODO                                    scriptoriumArtifact(
                                        "validation-spec-${N7S_CI_PLATFORM}.json",
                                        "combine-validation-spec",
                                        "--name '${SCRIPTORIUM_PROJECT_NAME}'" as String,
                                        "--tests",
                                        "artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/unit-tests-specs-${N7S_CI_PLATFORM}.json",
                                        // TODO ? "validation/specifications/tests-extra.json",
                                        // TODO ? "validation/specifications/tests-extra-${N7S_CI_PLATFORM}.json",
                                        "--reviews",
                                        "validation/specifications/design-reviews.json",
                                        "validation/specifications/design-reviews-${N7S_CI_PLATFORM}.json",
                                        "--inspections",
                                        "validation/specifications/inspections.json",
                                        "validation/specifications/inspections-${N7S_CI_PLATFORM}.json",
                                        "--overrides",
                                        "validation/overrides.json",
                                        "validation/overrides-${N7S_CI_PLATFORM}.json",
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SVS-Backward-Trace-${N7S_CI_PLATFORM}.html",
                                        "generate-svs-backward",
                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json" as String,
                                        "artifacts/srs-current.json",
                                    )
                                    scriptoriumArtifact(
                                        "CTSDP-SVS-Forward-Trace-${N7S_CI_PLATFORM}.html",
                                        "generate-svs-forward",
                                        "artifacts/srs-current.json",
                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json" as String,
                                    )
*/
                                }
                            }

                            stage("Reports") {
                                steps {
//                                    scriptoriumArtifact(
//                                        "unit-tests-results-${N7S_CI_PLATFORM}.json",
//                                        "extract-cpputests-results",
//                                        unitTestsLogDir('release') as String,
//                                    )
//                                    scriptoriumArtifact(
//                                        "integration-tests-results-${N7S_CI_PLATFORM}.json",
//                                        "extract-xunit-results",
//                                        "--input-filter *-report.xml",
//                                        "--cram",
//                                        "${getCurrentPlatformBuildDir()}/release/tests" as String,
//                                    )
//
//                                    scriptoriumArtifact(
//                                        "CTSDP-SUITR-unit-tests-${N7S_CI_PLATFORM}.html",
//                                        "generate-suitr",
//                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Unit Tests Results'" as String,
//                                        "artifacts/unit-tests-specs-${N7S_CI_PLATFORM}.json",
//                                        "artifacts/unit-tests-results-${N7S_CI_PLATFORM}.json",
//                                    )
                                    scriptoriumArtifact(
                                        "CANDP-SUITR-integration-tests-${N7S_CI_PLATFORM}.html",
                                        "generate-suitr",
                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Integration Tests Results'" as String,
                                        "artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/integration-tests-results-${N7S_CI_PLATFORM}.json",
                                    )
//
//                                    scriptoriumArtifact(
//                                        "validation-report-${N7S_CI_PLATFORM}.json",
//                                        "combine-validation-report",
//                                        "--name '${SCRIPTORIUM_PROJECT_NAME}'" as String,
//                                        "--tests",
//                                        "artifacts/unit-tests-results-${N7S_CI_PLATFORM}.json",
//                                        "artifacts/integration-tests-results-${N7S_CI_PLATFORM}.json",
//                                        // TODO "validation/results/tests-extra.json",
//                                        // TODO "validation/results/tests-extra-${N7S_CI_PLATFORM}.json",
//                                        "--reviews",
//                                        "validation/results/design-reviews.json",
//                                        "validation/results/design-reviews-${N7S_CI_PLATFORM}.json",
//                                        "--inspections",
//                                        "validation/results/inspections-results.json",
//                                        "validation/results/inspections-results-${N7S_CI_PLATFORM}.json",
//                                    )
//
//                                    scriptoriumArtifact(
//                                        "CTSDP-SValR-item-results-${N7S_CI_PLATFORM}.html",
//                                        "generate-svalr-items-results",
//                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json" as String,
//                                        "artifacts/validation-report-${N7S_CI_PLATFORM}.json",
//                                    )
//
//                                    scriptoriumArtifact(
//                                        "CTSDP-SValR-${N7S_CI_PLATFORM}.html",
//                                        "generate-svalr",
//                                        "artifacts/srs-current.json",
//                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json",
//                                        "artifacts/validation-report-${N7S_CI_PLATFORM}.json",
//                                        "validation/close-outs/ts-${N7S_CI_PLATFORM}.json",
//                                    )
//
//                                    publishHtmlSummary("Validation Reports", [
//                                        "SValR",
//                                        "SUITR-unit-tests",
//                                        "SUITR-integration-tests",
//                                    ])
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

def getPlatformList() {
    return [
        "samv71q21",
        "samv71q21_xc32",
        "samrh71f20",
        "samrh71f20_xc32",
        "samrh707f18",
        "samrh707f18_xc32",
    ]
}

def getPlatformName(String target) {
    return [
        "V71": "samv71q21",
        "V71_XC32": "samv71q21_xc32",
        "RH71": "samrh71f20",
        "RH71_XC32": "samrh71f20_xc32",
        "RH707": "samrh707f18",
        "RH707_XC32": "samrh707f18_xc32",
    ].get(target)
}

def getPlatformBaseName(String target) {
    return [
        "V71": "samv71q21",
        "V71_XC32": "samv71q21",
        "RH71": "samrh71f20",
        "RH71_XC32": "samrh71f20",
        "RH707": "samrh707f18",
        "RH707_XC32": "samrh707f18",
    ].get(target)
}

def getDockerImageNameForTarget(String target) {
    return [
        "V71": env.CM7_IMAGE_NAME,
        "V71_XC32": env.XC32_V71_IMAGE_NAME,
        "RH71": env.CM7_IMAGE_NAME,
        "RH71_XC32": env.XC32_RH71_IMAGE_NAME,
        "RH707": env.CM7_IMAGE_NAME,
        "RH707_XC32": env.XC32_RH707_IMAGE_NAME,
    ].get(target)
}

def getSvfLabelForTarget(String target) {
    return [
        "V71": "CAN2-V71",
        "V71_XC32": "CAN2-V71",
        "RH71": "CAN2-RH71",
        "RH71_XC32": "CAN2-RH71",
        "RH707": "CAN2-RH707",
        "RH707_XC32": "CAN2-RH707",
    ].get(target)
}

def getCurrentPlatformName() {
    return getPlatformName(env.N7S_CI_TARGET)
}

def getCurrentPlatformBaseName() {
    return getPlatformBaseName(env.N7S_CI_TARGET)
}

def getCurrentPlatformBuildDir() {
    return "build-${env.N7S_CI_TARGET}"
}

def getSelectedSvfName() {
    return env.SELECTED_SVF.split("-")[2].toLowerCase()
}

def getCurrentPlatformSvf() {
    if (env.SELECTED_SVF) {
        return "environment/svf-configs/${getCurrentPlatformBaseName()}/${getSelectedSvfName()}.conf"
    }
    if (env.N7S_CI_PLATFORM) {
        return "environment/svf-configs/${getCurrentPlatformBaseName()}/build-only.conf"
    }
    return "environment/svf-configs/localhost.conf"
}

def parallelBuildFlag() {
    return "-j3 "
}

def baseSconsOpts() {
    "buildDirName=${getCurrentPlatformBuildDir()} hwtbPlatformName=${getCurrentPlatformName()} svf=${getCurrentPlatformSvf()}"
}

def coverageSconsOpts() {
    return "build=coverage ${baseSconsOpts()}"
}

def releaseSconsOpts() {
    return "build=release ${baseSconsOpts()}"
}

def debugSconsOpts() {
    return "build=debug optimization=g ${baseSconsOpts()}"
}

def releaseBuild(String target, String additionalOpts = "") {
    scons targets: target, opts: releaseSconsOpts() + " " + additionalOpts
}

def debugBuild(String target, String additionalOpts = "") {
    // FIXME: Re-enable debug builds after fixing size issues on RH707 w/ XC32
    // scons targets: target, opts: debugSconsOpts() + " " + additionalOpts
}

def coverageBuild(String target, String additionalOpts = "") {
    scons targets: target, opts: coverageSconsOpts() + " " + additionalOpts
}

def publishCoverageReport() {
    publishHtmlReport("${getCurrentPlatformBuildDir()}/coverage/dcf2dev-coverage/html",
                      "dec2dev Tool Coverage Report")
}

def publishHtmlReport(String dir,
                      String title,
                      String reportFiles = "index.html") {
    title = title + " (" + env.N7S_CI_PLATFORM.toUpperCase() + ")"
    publishHTML target: [
        allowMissing: false,
        alwaysLinkToLastBuild: false,
        keepAll: true,
        reportDir: dir,
        reportFiles: reportFiles,
        reportName: title,
    ]
    // separate zip for copying artifacts
    archiveDirAsZip(title.replaceAll(" ", "-").replaceAll("/", "-") + ".zip", dir)
}

def archiveDirAsZip(String file, String dir) {
    zip zipFile: "artifacts/" + file, archive: true, dir: dir, overwrite: true
}

def gatherCtsswSonarLogs() {
    sh "./environment/sonar/rules_report.py '' ${SONAR_CTSSW_KEY} artifacts/ctssw-sonar-rules.html"
    archiveArtifacts artifacts: 'artifacts/ctssw-sonar-rules.html'

    sh "./environment/sonar/issues_report.py '' ${SONAR_CTSSW_KEY} artifacts/ctssw-sonar-issues.html"
    archiveArtifacts artifacts: 'artifacts/ctssw-sonar-issues.html'
}

def gatherCanswSonarLogs() {
    sh "./environment/sonar/rules_report.py ${SONAR_ORGANIZATION} ${SONAR_CANSW_KEY} artifacts/cansw-sonar-rules.html"
    archiveArtifacts artifacts: 'artifacts/cansw-sonar-rules.html'

    sh "./environment/sonar/issues_report.py ${SONAR_ORGANIZATION} ${SONAR_CANSW_KEY} artifacts/cansw-sonar-issues.html"
    archiveArtifacts artifacts: 'artifacts/cansw-sonar-issues.html'
}

def sonarVariantsForPlatform(String platform) {
    return "${platform},gcc_host_for_${platform}"
}

def getSonarVariants() {
    if (params.selectedTarget == "All")
        return getPlatformList().collect {
            sonarVariantsForPlatform(it)
        }.join(",")
    return sonarVariantsForPlatform(getCurrentPlatformName())
}

def copyCompilationDatabaseForSonar() {
    final host_dir = "sonar-variants/gcc_host_for_${getCurrentPlatformName()}"
    final dir = "sonar-variants/${getCurrentPlatformName()}"
    sh "mkdir -p ${host_dir}"
    sh "mkdir -p ${dir}"
    sh "cp ${getCurrentPlatformBuildDir()}/release/${getCurrentPlatformName()}/compile_commands.json ${dir}"
    sh "cp ${getCurrentPlatformBuildDir()}/release/gcc_host/compile_commands.json ${host_dir}"
}

def getMplabPlatforms(String platform) {
    if (platform == "samv71q21")
        return ["atsamv71q21b", "atsamv71q21rt"]
    if (platform == "samrh71f20")
        return ["atsamrh71f20c"]
    if (platform == "samrh707f18")
        return ["atsamrh707f18a"]
}

def generateMplabProjectsForPlatform(String platform, String build_dir) {
    def lely_source_dir = "environment/resources/LibCANopen/lely-core"
    def lely_build_dir = "${build_dir}/release/${platform}/resources/LibCANopen/lely-core"
    def mplab_projects_dir = "${build_dir}/release/${platform}/mplab_projects"

    getMplabPlatforms(platform).each { mplab_platform ->
        def generated_project_dir = "${mplab_projects_dir}/${mplab_platform}"
        sh "python3 resources/mplab/generate_mplab_project.py --lely-core-dir ${lely_source_dir} --makefile-path resources/mplab/Makefile ${lely_build_dir}/compile_commands.json ${generated_project_dir} ${mplab_platform}"
        archiveDirAsZip("CANOpen_MPLAB_${mplab_platform}.zip", generated_project_dir)
    }
}
