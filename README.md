Test Suite (CTSSW)
==================

Test validation suite for CANopen software library.

Designed to test [ECSS-compliant](https://ecss.nl/standard/ecss-e-st-50-15c-space-engineering-canbus-extension-protocol-1-may-2015/)
version of [lely-core](https://gitlab.com/n7space/canopen/lely-core) (forked from [Lely Industries](https://opensource.lely.com/canopen/))

Test cases are defined using [Test Environment (CTESW)](https://gitlab.com/n7space/canopen/test-environment).
Each consist of two applications, one running on x86 host and second running on ARM dedicated hardware.
Applications will exchange data using CAN bus and CANopen protocol stack.

Test Suite was developed under a programme of, and funded by, the [European Space Agency](https://www.esa.int/).

Licensed under the ESA Public License (ESA-PL) Permissive (Type 3), Version 2.4 (see [LICENSE](./LICENSE) for details).

## Requirements

 * GCC x86 compiler (10.2)
 * GCC ARM compiler (9-2020-q2-update)
 * GDB debugger (multiarch, 9.2)
 * Python 3.8
   with packages:
    - paramiko (2.7.2)
    - pygdbmi (0.9.0.3)
    - pyserial (3.5)
    - scons (4.1.0.post1)
    - scp (0.13.3)
    - timeout_decorator (0.5.0)

Easiest way to reproduce required software environment is to use Docker (>= 19.03.12) with provided image.
Dockerfile (distributed by [Test Environment](https://gitlab.com/n7space/canopen/test-environment))
can also be used as a reference for detailed list of required packages etc.

## Running tests

Tests are built and executed using [SCons](https://scons.org/) tool, using provided configuration.

### Configuration

Configuration of the test hardware setup can be provided in the form of INI files.
Example configuration is available at `configs/` subdirectory (provided by Test Environment).
By default `default_sdram.conf` is used, but it can be overwritten by passing option `config=<filepath>` to the SCons call.

### Execution

Tests can be executed by simply calling the [SCons](https://scons.org/) tool: `scons`.
If no test name is provided, all tests will be executed.

Single test can be executed by passing name of the test to the tool `scons <test-name>`.
Supported options can be obtained from the tool by calling `scons -h`.

Easiest way to execute tests in predefined environment is to use Docker. Suggested way of executing:
`docker run -v $PWD:"/path/to/ctssw" --workdir "/the/same/path" --user $(id -u):$(id -g) --rm "CTESW image name" scons`.
This way the container will run with user permissions and will have access to all necessary CTSSW files.
Docker configuration can be found in `docker/` directory.
